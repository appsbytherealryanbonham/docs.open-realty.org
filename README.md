# docs.open-realty.org

This repository is the home to docs.open-realty.org. These are the official docs for Open-Realty. 


## Usage

[Read The Docorg(https://docs.open-realty.rg)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[Creative Commons Attribution-ShareAlike 4.0 International](LICENSE.md)
