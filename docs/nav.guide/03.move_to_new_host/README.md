# Moving an existing site

1. If you only need to change your domain name, or the URL of your Open-Realty site and have not physically moved its program files to a new host, server, or folder, you can skip to step #7 below. Make sure to note the version number of Open-Realty you are running (e.g: v3.2.7) before continuing. 

2. BACKUP EVERYTHING!  Always backup your existing website and database prior to attempting to move an Open-Realty installation. Backing up your database can be done using your favorite database management tool which is often available through your hosting provider's control panel (Usually phpMyAdmin). Backing up your files can be done by downloading ALL of the Open-Realty® files from your current host to your local computer using your favorite FTP client (For example: Filezilla).  

::: tip
Some hosts provide a site backup option through the control panel that will let you download a single, archive file that includes your database backup file and all of the files from your site. Also, check with your new host as they may provide an option or method to automatically transfer your existing site and database to the new host. If you have this option, use it and skip to step X. 
:::

3. Using your favorite FTP client (e.g.: Filezilla), or whatever tool is applicable for your hosting, upload ALL of your Open-Realty files from the backup you made to the new host/location. If you wish to move Open-Realty into a sub-folder instead of the root (or vice versa) you would do that at this time. 

4. Create a new empty database, a database user, and password, and then give that user full permissions to the database you created. WRITE DOWN OR SAVE this information, you will need it later.  

5. Use phpMyAdmin, or another database management tool to restore your database backup to the empty database you just created in step #4. 

6. EDIT: 'include/common.php' which should already be uploaded to your new host from step #3. Replace the old database information with your new database information that you created in step 4. Modify the following lines in your common.php file as needed to match your new information: 

``` php
$db_user = "mydbuser";
$db_password = "mydbpassword";
$db_database = "mydatabase";
$db_server = "localhost";
```
7. UPLOAD: Upload the /install folder for your current version of Open-Realty (from step #1). If you no longer have the /install folder for your version, or are using an old version you can download the current version of Open-Realty and then follow the upgrade instructions in the [Upgrading Open-Realty](../02.upgrading/README.md) section to upgrade your Open-Realty installation at the same time you are moving it.  

8. Using your web browser, access the OR installer:  http://www.yourdomain.com/INSTALLPATH/install/
::: tip
Replace 'yourdomain.com' with your actual domain name and INSTALLPATH with the sub-folder name (if applicable) where OR was installed. 

For example, if you installed OR to a hosting account that uses the domain name: 'myrealestatecompany.com' and placed the files in a sub-folder of the document root named "open-realty" you would use "open-realty" as the INSTALLPATH and navigate to: 

http://www.myrealestatecompany.com/open-realty/install/index.php. 

If you installed OR to the document root of your hosting account, and not a sub-folder, you would omit the INSTALLPATH entirely.

http://www.myrealestatecompany.com/install/ 
:::

### Running the installer

1. Read the license agreement and click the "Agree" button to continue.
   
2. Select the default language for your site and click submit, this MUST be the same language you were using on your old host. Click Submit. 
   
3. The next screen verifies the file permissions. If it locates any permission problems you will need to correct them and refresh the page. Once all file permissions are correct; Click "Click to Continue Installation". 
   
4. You now need to select the type of installation you are doing. Select "Update Path and URL information only" 
   
5. Verify that the installer is using the correct settings for your database and paths. Click next. 
   
6. This step updates the common.php file, which stores the database configuration information Open-Realty® uses. 

Congratulations, your Open-Realty® installation is complete! You must delete the /install folder before your site will be fully accessible. 
  