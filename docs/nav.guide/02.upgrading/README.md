# Overview

The following two documentation sections provide information for upgrading from the venerable Open-Realty v2.x (circa 2005-2009) and previous versions of Open-Realty v3 released since 2010. 
