# Upgrading from version 3.x

::: danger
BACKUP! BACKUP! BACKUP!!!

(No, really, make a full backup of everything)
:::

Always backup your existing website files and database before attempting an upgrade. Backing up your database can be done using your web host's database management tool which is often available through their control panel,  'phpMyAdmin' is typically available. Some hosts offer more advanced or alternative backup methods via their control panels, speak with your host if you have any questions about performing backups of your site. 

One click automatic upgrade:

If you are currently running Open-Realty v3.1.0 or newer, and your hosting account is running PHP as [suexec](http://httpd.apache.org/docs/2.2/suexec.html) (suPHP, etc.) you can click on the "Upgrade Available" link present in OR's main administration area. If you do not have "Automatic Update Check" turned on in Site Config, this link will display as: "Unable to check for update. Click here to manually check".

You will be presented with a dialog box requesting confirmation, click "yes" if you wish to continue and perform the upgrade. 

Note:The automatic upgrade process can require several minutes, especially if you have a lot of listings. It will report the version(s) that have been successfully updated and applied when finished.

* * *

Manual upgrade:

If you are in doubt whether or not you can perform the one-click upgrade above, perform a manual upgrade. It only takes a few minutes.

Obtaining and uploading the upgrade

1. Download the latest official [Open-Realty release](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases). 
   
::: tip
Download the Package Zip file as shown in this screenshot, not the raw src zips..

<img src="/assets/img/release_download_screenshot.png" />
:::

2. Extract the downloaded .ZIP file to your local computer and upload the extracted files to your website, either to the root folder on your site, usually named 'www' or 'public html', or to a sub-folder of your web site root if you had previously installed OR in a sub-folder.
    * NOTE REGARDING TEMPLATES: If you modified one of the example Open-Realty templates such as Lazuli or HTML5, but did not change or copy the template folder name to something unique, e.g. /template/mytemplate/, updating Open-Realty  will overwrite your changes. Either rename your customized template folder prior to uploading the new OR files or do not upload the example template folder of the same name.
  
3. Using your web browser, navigate to: 

http://www.yourdomain.com/INSTALLPATH/install/index.php. 

Replace yourdomain.com with your actual domain name and INSTALLPATH with the sub-folder name (if applicable) where you uploaded the OR program files. 

For example, if you installed OR on a domain named myrealestatecompany.com in a sub-folder named "open-realty" you would navigate to:

http://www.myrealestatecompany.com/open-realty/install/index.php.

If you uploaded the program files to the document root of your hosting account, and not a sub-folder, you would omit the INSTALLPATH entirely. 

http://www.myrealestatecompany.com/install/ 

Running the installer

1. Read the license agreement and click the "Agree" button to continue. 
2. Select the default language for your site and click submit. NOTE: English is the only language file that is regularly maintained by the Development team. If you have difficulty installing due to blank pages in another language it is likely due to an incomplete or missing lang file for your selected language. To fix that you can open the /install/lang/en/ folder and replace the lang.inc.php file from your configured lang folder with the one from the 'en' folder. 
3. The next page attempts to verify the necessary file permissions. If it finds any permission problems you will need to correct them, and then refresh the page. Once all permissions are correct; Click "Click to Continue Installation". 
4. You must select the type of installation you are performing. You should select "Upgrade from Open-Realty 2.x (Beta 1 or higher) " 
5. Verify that all of the database and configuration information presented is correct. This information is obtained from your existing 'common.php' file. Click next. 
6. This step updates the common.php file, which stores the database configuration information for Open-Realty to use. Click "Continue to setup the database." 
7. This step will create and populate the Open-Realty database tables. 
8. Click "Click here to configure your installation". Your account login information will be unchanged from the prior version you had installed. 
9. Make sure you have any necessary CRON jobs setup as described in [Automating Tasks](../04.automating_tasks/README.md)

Congratulations, your Open-Realty installation is complete! You must delete the install directory before your site will be accessible.  

TEMPLATE UPGRADE NOTE:  There may always be the possibility of changes or updates to the template system and example templates, including the addition of new template files to the default template, and new or changed template tags. You may need to edit and update your custom template files if any were affected by the upgrade. You can review potential template changes before upgrading by visiting the [Open-Realty Release Notes.](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)
