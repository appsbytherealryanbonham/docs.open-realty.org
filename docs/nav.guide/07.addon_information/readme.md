# Overview

Why should you utilize the add-on system? What can you do with the
add-on system? In the past, Open-Realty v1.x and v2.x users made
numerous inquiries to us regarding problems they experienced upgrading
from one version of Open-Realty to a newer version most commonly because
they or their web developer had made customizations to the core
programming files that had to be re-done from scratch with each new OR
upgrade. These problems could have been easily prevented by the use of
the add-on system as it is external and separate and typically
unaffected by OR upgrades. Now that editing OR's core files is no longer
possible, this makes the proper use of add-ons even more important for
anyone who needs to create or customize functionality in OR

  

The add-on system was well thought out during the development of
Open-Realty v2, and was improved in OR v3. It provides great power and
flexibility to site developers to add functionality they desire to
Open-Realty without the need to alter or edit the core programming code.

  

Add-ons don't need to be some large, complex, or spectacular piece of
software. Add-ons can be as simple as rewriting an existing Open-Realty
function to better suit your taste, design or needs, or substituting
your own PHP content or code via the creation of custom template tags.


