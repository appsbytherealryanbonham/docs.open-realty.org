# Installing an add-on

Open-Realty add-ons can be found  at the Open-Realty Extras site located
at:

<http://www.open-realty.org/open-realty%C2%AE-extras.html>

Add-ons may also be available from other publishers elsewhere on the
internet, but the OR Extras site is a good place to look first.

Once you have located an add-on that you want to use in Open-Realty
check to see if documentation for that add-on is provided. This should
include instructions for installing the add-on.

If the add-on you want is located on the OR add-ons web site, then you
can locate it, and install it directly through the OR Add-On Manager in
the OR administration Control Panel.

Please see the
[Add-On Manager](../05.administration/10.addon_manager/addonmanager.md) section
in the Administration section of this documentation for full details on
using the Add-On Manager to install/uninstall  add-ons.

If the add-on is distributed as a compressed ZIP file then it can very
likely be installed through the Add-On Manager assuming it and the
folder contained therein is properly named.
