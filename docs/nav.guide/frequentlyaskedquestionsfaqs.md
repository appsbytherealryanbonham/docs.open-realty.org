# Frequently Asked Questions

I'd like to remove the "powered by" Open-Realty® logo from my web site,
how do I do this?

See [Removing Powered By](06.template_documentation/01.Installing%20Templates/removingbranding.md)

---

### How do I backup my Open-Realty site?

All of OR's listings, blog pages, users, form data, and configuration
values are all stored in a MySQL database. So in addition to backing up
your Open-Realty program files and your template, you will also want to
backup your database as well.

1. Back up all of your files. You should have a local copy on your
   computer from when you uploaded them to your server so it's best to use
   that as the base for your backup instead of downloading all your files
   again. The next step would be to login to your server via FTP and
   download the following content to your base Open-Realty files.

- /include/common.php (Replace the default copy included with
  Open-Realty with the one from your server)
- The entire contents of your /images/ folder and all sub-folders
  (this contains all your uploaded images and virtual tours)
- The entire contents of your /files/ folder and all sub-folders.
- Any custom templates that you've created or installed beneath the
  /template/ folder, or the /admin/template/ folder
- Any other files that you have modified or added, only on the server,
  and not on your local backup copy.

2. Backup your database. The easiest way is to use phpMyAdmin or a
   similar database management software. These are usually installed for
   most hosting plans and can be accessed through the control panel for
   your hosting plan (not the Open-Realty administration area). To backup
   using one of these programs you will want to export your entire database
   that you have setup for Open-Realty. If you are an advanced user, you
   can backup just those tables that have the table prefix for your
   Open-Realty installation. Save the exported database to a file on your
   computer with the rest of your Open-Realty files and you now have a
   complete backup.

3. Keep in mind that any time you add or remove listings your database
   is being changed. So if you restore an old backup of your database you
   will have lost any new listings or changes stored in the DB since the
   time that backup was made.

Many web hosting providers will provide for downloading a full site
backup from your hosting control panel. These full site backups will
include all of the files and the database for your site in a compressed,
archived file. This is an excellent way to backup your Open-Realty site.
Contact your host for information and instructions on how to do this
using  their system if available.

---

### I lost my password, how do I recover my password?

Go to the Open-Realty administration login page. Below the login area is
a field to enter your email address and have both your username and
password emailed to you. Enter the email address that you used for your
account and click "Lookup". Your username and password will be emailed
to the email address on your account.

If you do not have access to your email address or don't remember what
email address you used for your account, contact the site admin and
request that they reset your password or change your email address for
you.

---

### I forgot my admin password, and don't remember or have access to the

email address for the admin account. How do I reset the password?

If emailing the username and password do not work then you can reset the
password to the default password if you have access to your host's
database administration tools. These instructions assume your host uses
cPanel and phpMyAdmin.

The steps to reset the Admin's password for Open-Realty®:

1. Login at "http://www.yourdomain.com/cpanel"

2. Go "mySQL databases"

3. At the bottom of that page click "phpMyAdmin"

4. In the drop-down selection on the top left side of the page select
   your Open-Realty® database.

5. On the main section of the screen find the "defualt_en_userdb"
   table (assumes English install) and click the "Browse" action

6. Find the user you want to reset the password for. Click the "Edit"
   link next to them

7. In the userdb_user_password field enter
   "5f4dcc3b5aa765d61d8327deb882cf99" (without quotes)

8. Press the go button. You user password is now reset to "password"

---

### What is the difference between Agents and Members?

Agents can post listings to the site and can also be given more advanced
privileges such as editing site pages, blogs and etc.

Members can not post listings. Members are the ordinary site visitors
who visit your Open-Realty site who sign-up to be able to save searches
and favorite listings and request information using the Contact Forms.
Members can also receive notification by email when new listings that
match any saved searches when listings that match are added to the site.
You can enable or disable new listing notification from the Open-Realty
site configuration.

---

### I need to change a word or phrase that is used in Open-Realty, how do I

change it?

Open-Realty was designed using language variables so that almost all
wording on your Open-Realty site can easily be changed in the language
files. The language files contain a large list of all the language
variables used on the site and their current values. For details on
creating custom language files, editing current language files or
changing individual language variables see the LanguageGuide section of
this documentation.

---

### The statistics tabs located on the main administration page do not show

any values

The OR_small administration template that displays the statistics
contains template tags that use the default example field names from a
fresh Open-Realty install. If you are running a site that imports from a
MLS, you are likely using the MLS's field names instead. These template
tags simply need to be updated to use your field names.

This fix assumes you are presently set to use the 'OR_small'
administration template.

1\.  Copy the default template file:

/admin/template/default/or_index.html

to

/admin/template/OR_small/or_index.html

2\. Edit the or_index.html file and locate the following code block:
This will be on or around line \#31

```php
<div class="mini-panes"&gt;

<divid="listing\_stats\_data\_all">

       <span class="stat_caption">Average price:</span>
{listing_stat_avg_field_price_value}<br/>

       <span class="stat_caption">Median price:</span>
{listing_stat_median_field_price_value}<br/>

       <spanclass="stat_caption">Highest price:</span>
{listing_stat_max_field_price_value}<br/>

       <span class="stat_caption">Lowest price:</span>
{listing_stat_min_field_price_value}<br/>

       <span class="stat_caption">Average Square
Feet:</span> {listing_stat_avg_field_sq_feet_value}

</div>



  {foreach_pclass_block}

<divid="listing_stats_data_{pclass_id}">

       <span class="stat_caption">Total {pclass_name}
Listings:</span>
{active_listing_count_pclass_{pclass_id}}<br/>

       <span class="stat_caption">Average price:</span>
{listing_stat_avg_field_price_value_pclass_{pclass_id}}<br/>

       <span class="stat_caption">Median price:</span>
{listing_stat_median_field_price_value_pclass_{pclass_id}}<br/>

       <spanclass="stat_caption">Highest price:</span>
{listing_stat_max_field_price_value_pclass_{pclass_id}}<br/>

       <span class="stat_caption">Lowest price:</span>
{listing_stat_min_field_price_value_pclass_{pclass_id}}<br/>

<span class="stat_caption">Average Square Feet:</span>
{listing_stat_avg_field_sq_feet_value_pclass_{pclass_id}}

</div>

  {/foreach_pclass_block}

</div>
```

The statistics are retrieved using the
{listing_stat_FUNCTION_field_FIELDNAME_value} template tags which
can be reviewed on the Main template Tags page of the PUBLIC SITE
TEMPLATES section of the documentation. . In the codeblock above in bold
you will notice the 'price' and 'sq_feet' default OR field names are
being used. Change the field names in bold to your specific price and
square footage fields and save the template file. If the
'or_index.html' file is updated in a future version of OR, you will
want to take another copy as in step\#1 above again and re-apply your
changes, or simply create your own custom administration template folder
containing these changes, and switch to use that.
