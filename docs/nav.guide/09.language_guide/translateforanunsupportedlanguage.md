# Translate for an unsupported language

Translate Open-Realty® into a different language

Translating Open-Realty is not a difficult task, but it can be time
consuming. Open-Realty already includes several community-contributed
language files, but If you find that your language is not already
supported or is not regularly updated by the community, you can
translate the English language files into your desired language. The
files you should use to make your own full translation are the English
versions as they are the only language files that are 100% guaranteed to
be up to date with each new release.

First copy all of the English language files and sub folders into a new
language folder for your language. The English language files are
located within the following folders:

include/language/en/

include/language/en/custom

include/language/en/versions

If you wanted to translate OR to use the Klingon language (kn) for
instance, your new language folders could be named:

include/language/kn/

include/language/kn/custom

include/language/kn/versions

Within the /versions folder

The Open-Realty language files for the installer are maintained in a
separate location in the following path and also need to be included for
a complete translation:

install/lang/en/lang.inc.php

You would copy the above file to your new Klingon language folder for
the installer.

install/lang/kn/lang.inc.php

If you open the language files located within the /versions folder or
the installer language file in your favorite text editor, you will see a
list of array keys and values for the \$lang array.

example:

\$lang\['access_denied'\] = "Access Denied";

Change the array text values only, in the above example that would be:
"Access Denied", do not modify the array key, which is:
\$lang\['access_denied'\] the array key name must remain unchanged. You
will need to do this for each file located in your /custom lang folder.

Lang variable updated for Klingon language

\$lang\['access_denied'\] = "Naw' tem";

Once all language files have been translated to your liking, edit the
main language list in your new language folder and the English version
located at:

include/language/en/versions/languages.inc.php

include/language/kn/versions/languages.inc.php

Add your new Klingon language info to the \$lang array for both files.

\$lang\['kn'\] = '(kn) Klingon';

Once you have translated the language files please consider submitting
them for inclusion with a future release.
