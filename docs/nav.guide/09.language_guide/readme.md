# Overview

Open-Realty® was designed with one-at-a-time language support in mind.
Full multi-language support, such as having one site use several
languages simultaneously is not currently supported. However, we have
spent a lot of time in ensuring that OR uses language variables defined
via a $lang PHP array for almost all text used for the site pages. These
language variables are defined in "language files" that are located at
the following path within your OR installation:

  

include/language/??/versions/

  

The ?? in the path is the two letter language code that you have
configured in Open-Realty. For example: "en" for English. The
'original.inc.php' file located within the /versions folder above
contains most of the language variables for Open-Realty. In addition to
the 'original.inc.php' file, you will also see a few additional files
that are named using the Open-Realty version number denoting which
version they were added for and the extension ".inc.php". For Example:

  

2\_5\_6.inc.php

2\_5\_8.inc.php

3\_0\_0.inc.php

  

Each of these files contains additional language variables that were
introduced with those versions of Open-Realty.

  

Making customizations to existing language variables

If you find that you want to change the wording of certain Open-Realty
language variables then you may use the custom language folder. Custom
language files may be placed in the following path:

  

include/language/??/custom/

  

The ?? in this path is the two letter language code that you have
configured in Open-Realty. For example "en" for English. By default
there is a file in this folder named "example.inc.php". This file is an
example of how you can use a custom language file to create your own
language vars, or modify existing vars that you copy from the other lang
files.

  

Create a custom file in this folder and you may then copy language
variables you wish to change from the language files contained in the
versions folder. You can make any changes you want to your customized
language variables in the /custom folder and they will not be
overwritten by upgrades like the files in the

/versions folder. The language variables in any custom language file are
loaded into OR last, and any entries in it will override the default
language variables.

  

Creating new language variables

You can create your own new language variables if you wish for embedding
in your custom templates. Any new variables you add to the $lang PHP
array must be prefaced with the text: lang\_  when using a language
template tag to display your text contents.

  

Example:

  

Custom Lang variable: $lang\['my\_lang\_text'\] = 'My language var text
to display';

Corresponding OR language tag: {lang\_my\_lang\_text}

  

Note: Be careful what you choose to use for any new array key names, if
you duplicate an existing $lang array key your new version will override
any others.

If you need to use the apostrophe character ' in any language text, make
sure to escape it with a slash \\  e.g.: 'My language var\\'s text to
display';

  

