# log\_api

The log API contains all API methods for inserting activity log data.

  

Methods: log\_\_

  

log\_create\_entry($data) - Used to create an entry in the OR activity
log

create(array $data) : array

  

Returns: (array)

\['error'\] = TRUE/FALSE

\['status\_msg'\] = Result, Either success message or error

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['log\_api\_command'\] - TEXT - API/Command you are running when
the error occurred or the message is generate from.

$data\['log\_message'\] -  TEXT -  Message to insert in the log

  

&lt;?php

       require('/api/api.inc.php');

   $api = new api();

  

       $api-&gt;load\_local\_api('log\_\_log\_create\_entry',array(

               'log\_api\_command'=&gt;'my function name',

               'log\_message'=&gt;'This specific thing happened'

       ));

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;


