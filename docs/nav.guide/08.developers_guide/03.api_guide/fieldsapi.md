# fields\_api

The listings Field API contains all API methods for creating and
retrieving field metadata.

  

Methods: fields\_\_

  

assign\_class($data) - Used to add an existing listing field in OR to a
property class:

assign\_class(array $data) : \\multitype:string

  

Returns: (multitype:string)

\['error'\] - TRUE/FALSE - Error status of the assign  request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['field\_id'\] - INT -  Numeric listing field ID\# of the field
(listingsformelements\_id).

\['class\_id'\] - INT - Numeric class ID  ID\# the field was assigned
to.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['class'\] - INT - Class ID that we are assigning this field to

$data\['field\_id'\] - INT - Field ID to assign

  

------------------------------------------------------------------------

  

create($data) - Used to insert a listing field into a class.

create(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the create request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['field\_id'\] - INT - Numeric listing field ID\# of the new field
(listingsformelements\_id).

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['resource'\] - TEXT - What type of resource are you creating a
field for. Allowed Options are "listing".

$data\['class'\] - ARRAY - Array of property class ID that you want to
insert an listing field into.

$data\['field\_type'\] - TEXT - Type of field to create. Valid options
are
'text','textarea','select','select-multiple','option','checkbox','divider','price','url','email','number','decimal','date','lat','long'

$data\['field\_name'\] - TEXT - Name of field to create

$data\['field\_caption'\] - TEXT - Caption to display for field.

$data\['field\_elements'\] - ARRAY - Array of options if this is a
select type of field.

$data\['rank'\] - INT  - Order of Field.

$data\['search\_rank'\] - INT - Order of Field on Search pages.

$data\['search\_result\_rank'\] - INT - Order of field on search results
page.

$data\['required'\] - BOOLEAN - Field is required: TRUE/FALSE

$data\['location'\] - TEXT - Template Location to display field at.

$data\['display\_on\_browse'\] - BOOLEAN - Should this field be
displayed on the search results page?.

$data\['search\_step'\] - INT - Value to step search values by for
min/max fields

$data\['display\_priv'\] - INT - 0 = Show Field to All Visitors, 1 =
members & Agents, 2 = Agents Only, 3 = Admin Only

$data\['field\_length'\] - INT - Maximum Length of Field

$data\['tool\_tip'\] - TEXT -Tooltip to show

$data\['search\_label'\] - TEXT - Label to show user when searching this
field

$data\['search\_type'\] - TEXT - Search field type, Allowed Values:
'ptext','optionlist','optionlist\_or','fcheckbox','fcheckbox\_or','fpulldown','select','select\_or','pulldown','checkbox','checkbox\_or','option','minmax','daterange','singledate','null\_checkbox','notnull\_checkbox'

$data\[$searchable'\] - BOOLEAN - Is the field searchable?

  

------------------------------------------------------------------------

  

(revised in v3.2.10)

metadata($data) - Used to get internal listing or Agent or Member field
metadata (information).

metadata(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the lookup request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['fields'\] - ARRAY - Array of all field info. Key is the numeric
'listingsformelements\_id',  'agentformelements\_id',
'memberformelements\_id' or 'feedbackformelements\_id' whichever
resource is applicable.

\['fields'\]\['field\_id'\] - INT - Numeric listing field ID\#  

\['fields'\]\['field\_type'\] - TEXT - Valid options are
'text','textarea','select','select-multiple','option','checkbox','divider','price','url','email','number','decimal','date','lat','long'

\['fields'\]\['field\_name'\] - TEXT - OR's internal field name for this
field.

\['fields'\]\['field\_caption'\] - TEXT - Description of the field. e.g.
'List Price' or 'Bedrooms'

\['fields'\]\['default\_text'\] - TEXT - Default text or values that
will display pre-selected in the search form

\['fields'\]\['field\_elements'\] - ARRAY - Array of any defined field
elements

\['fields'\]\['rank'\] -  INT  - Numeric order/rank of Field.

\*\['fields'\]\['search\_result\_rank'\] - INT -  Numeric search results
page order/rank of Field

\*\['fields'\]\['required'\] - YES/NO - Is this field required when
creating a listing

\['fields'\]\['location'\] - TEXT - Display location in OR's Listing
Editor or Lead Form Editor

\*\['fields'\]\['display\_on\_browse'\] - YES/NO - Does this field
display on the search\_results page.

\*\['fields'\]\['searchable'\] - BOOLEAN - Is field set to be used for
searches

\*\['fields'\]\['search\_type'\] - TEXT - Search Type of field, if field
set to be searchable.

\*\['fields'\]\['search\_label'\] - TEXT - Text label used if field is
used for searches.

\*\['fields'\]\['search\_step'\] - INT - Numeric step value if field is
used for searches and is a min/max search type.

+\['fields'\]\['display\_priv'\] - TEXT - Set only if the field is
restricted from viewing to only certain user types

\['fields'\]\['tool\_tip'\] - TEXT - Any defined tooltip text

(\* only applicable to 'listing' resource.  + not applicable to 'member'
and 'feedback' resouorces)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['resource'\] - TEXT - What resource do you want to get fields
for. Allowed Options are 'listing', 'agent', 'member' or 'feedback'.

$data\['class'\] - ARRAY - Optional array of property class IDs when
reading the listing resource. Only fields assigned to these IDs will be
returned.

$data\['field\_id'\] - INT  - Optional Field ID. If set only the
metadata for the field specified is returned..

$data\['searchable\_only'\] - BOOLEAN - Optional - If set to 'true' only
fields set to be searchable are returned.

  

'listing' resource example:

&lt;?php

       //Call the API and Get all Field  information for Property Class
1

       $pclass\_list = array(1);

       $result = $api-&gt;load\_local\_api('fields\_\_metadata',array(

               'resource'=&gt;'listing',

               'class'=&gt;$pclass\_list

       ));

       if($result\['error'\]){

               //If an error occurs die and show the error msg;

               die($result\['error\_msg'\]);

       }

       //No error so assign the array of fields that were returned to
$field\_list.

       $field\_list = $result\['fields'\];

       

       // format and output the contents of the $field\_list array so
the keys

       // and values can be viewed.

       echo '&lt;pre&gt;';

       print\_r($field\_list);

       echo '&lt;/pre&gt;';

?&gt;

  

'agent' resource example that targets a specific field\_id:

&lt;?php

       //get all metadata for Agent field that is agentformelements\_id
= 5

       $result = $api-&gt;load\_local\_api('fields\_\_metadata',array(

               'resource'=&gt;'agent',

               'field\_id' =&gt; 5

       ));

  

       if($result\['error'\]){

       //If an error occurs die and show the error msg;

               die($result\['error\_msg'\]);

       }

       //No error so assign the array of fields that were returned to
$field\_list.

       $field\_list = $result\['fields'\];

       

       // format and output the contents of the $field\_list array so
the keys

       // and values can be viewed.

       echo '&lt;pre&gt;';

       print\_r($field\_list);

       echo '&lt;/pre&gt;';

  

?&gt;

------------------------------------------------------------------------

  

values($data) - Used to obtain a list of unique values and the number of
listings that match each unique value for a given listing field.

values(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the lookup request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['field\_values'\] - ARRAY - Array of all unique values that exist for
this listing field.

\['field\_counts'\] - ARRAY - Array containing the number of listings
that match each unique stored value for this listing field. The keys of
this array equal the unique field\_values

  

Parameters:

$data (array)

Expects an array containing the following required array keys:

$data\['pclass'\] - ARRAY - Array of property class ID of the field you
want to obtain values for.

$data\['field\_type'\] - TEXT - Type of field. Valid options are
'decimal', 'number', or ''

$data\['field\_name'\] - TEXT  - Name of field to lookup

  

example:

&lt;?php

       //lookup listing counts that match each number of bedrooms

       //only calculate for residential class (class ID\#1)

       $pclass = array(1);

       $result = $api-&gt;load\_local\_api('fields\_\_values',array(

               'pclass'=&gt; $pclass,

               'field\_name'=&gt;'beds',

               'field\_type'=&gt;'decimal'

       ));

       

       // format and output the contents of the $result array so the
keys

       // and values can be viewed.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;


