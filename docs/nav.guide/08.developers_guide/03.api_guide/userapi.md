# user\_api

(added in v3.2.10)

The user API contains all API calls for creating and retrieving media
(files/photos/vtours) info.

  

Methods: user\_\_    

  

create($data) - Used to create a new user:

create(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the create request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['user\_id'\] - INT - Numeric listing ID\# of the newly created listing

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

$data\['user\_details'\] - ARRAY/TEXT - This should be an array
containing the following.

$data\['user\_details'\]\['user\_name'\] - TEXT- REQUIRED. This is the
user's account user name.

$data\['user\_details'\]\['user\_first\_name'\] - TEXT - REQUIRED. This
is the user's First Name

$data\['user\_details'\]\['user\_last\_name'\] -TEXT - REQUIRED. This is
the user's Last name.

$data\['user\_details'\]\['emailaddress'\] - TEXT - REQUIRED. This is
the user's email address.

$data\['user\_details'\]\['user\_password'\] - TEXT - REQUIRED. This is
the user's password.

$data\['user\_details'\]\['active'\] - TEXT - OPTIONAL - yes/no Make the
user active. Defaults to no

$data\['user\_details'\]\['is\_admin'\] - TEXT - OPTIONAL - yes/no Grant
this Admin privileges. Defaults to 'no'

$data\['user\_details'\]\['is\_agent'\] - TEXT - OPTIONAL - yes/no
create this user as an Agent. Defaults to 'no'

$data\['user\_fields'\] - ARRAY/MIXED - OPTIONAL The data you wish to
insert for this user for any custom user fields. Fields like 'phone' or
'mobile'. The array keys should reference the Field Names and the array
values should be the Field Values. Only valid fields will be used, other
key/data pairs will be dropped.

  

create a new Agent who is active. Insert data for a couple of existing
user defined fields.

&lt;?php

       //creates a new active Agent user 

       $result = $api-&gt;load\_local\_api('user\_\_create',array(

               'user\_details'=&gt;array(

                       'user\_name' =&gt; 'timuser',

                       'user\_first\_name' =&gt;'Tim',

                       'user\_last\_name' =&gt;'Userby',

                       'emailaddress' =&gt;'timmeh@somedomain.com',

                       'user\_password' =&gt;'password',

                       'active' =&gt;'yes',

                       'is\_admin' =&gt;'no',

                       'is\_agent' =&gt;'yes'

               ),

               'user\_fields' =&gt; array(

                       'phone' =&gt; '555-987-6543',

                       'mobile' =&gt; '555-123-4567'

               )

               

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

delete($data) - Used to delete a user completely. Any listings or media
belonging to them is also removed.

delete(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the delete request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['user\_id'\] - INT - Numeric userdb\_id of the user deleted.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['user\_id'\] - INT - Numeric userdb\_id of the user to delete.

  

example:

&lt;?php

       //deletes userdb\_id \#4 and any associated listings and media

       $result = $api-&gt;load\_local\_api('user\_\_delete',array(

                               'user\_id'=&gt;4

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

read($data) - Used to read/retrieve user data for a specific user.

read(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the read request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['user'\] - ARRAY - Array containing information and settings relevant
to the user requested. Array keys within \['user\] correspond to their
OR user\_db table column names (system fields) or any custom field
names. The following keys refer to OR's system fields which are always
available, any custom (user definable) fields such as "phone " or "Fax"
will be unique to each OR install.

\['user'\]\['userdb\_id'\] - INT -  

\['user'\]\['userdb\_user\_name'\] - TEXT - The user's account's user
name.

\['user'\]\['userdb\_emailaddress'\] - TEXT - The user's First Name

\['user'\]\['userdb\_user\_first\_name'\] - TEXT -

\['user'\]\['userdb\_user\_last\_name'\] - TEXT -

\['user'\]\['userdb\_comments'\] - TEXT -

\['user'\]\['userdb\_user\_password'\] - TEXT -

\['user'\]\['userdb\_is\_admin'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_site\_config'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_member\_template'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_agent\_template'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_listing\_template'\] - TEXT -

\['user'\]\['userdb\_creation\_date'\] - DATE -

\['user'\]\['userdb\_can\_feature\_listings'\] - TEXT -

\['user'\]\['userdb\_can\_view\_logs'\] - TEXT -

\['user'\]\['userdb\_last\_modified'\] - TIMESTAMP -

\['user'\]\['userdb\_hit\_count'\] - INT -

\['user'\]\['userdb\_can\_moderate'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_pages'\] - TEXT -

\['user'\]\['userdb\_can\_have\_vtours'\] - TEXT -

\['user'\]\['userdb\_is\_agent'\] - TEXT -

\['user'\]\['userdb\_active'\] - TEXT -

\['user'\]\['userdb\_limit\_listings'\] - INT -

\['user'\]\['userdb\_can\_edit\_expiration'\] - TEXT -

\['user'\]\['userdb\_can\_export\_listings'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_all\_users'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_all\_listings'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_property\_classes'\] - TEXT -

\['user'\]\['userdb\_can\_have\_files'\] - TEXT -

\['user'\]\['userdb\_can\_have\_user\_files'\] - TEXT -

\['user'\]\['userdb\_blog\_user\_type'\] - INT -

\['user'\]\['userdb\_rank'\] - INT -

\['user'\]\['userdb\_featuredlistinglimit'\] - INT -

\['user'\]\['userdb\_email\_verified'\] - TEXT -

\['user'\]\['userdb\_can\_manage\_addons'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_all\_leads'\] - TEXT -

\['user'\]\['userdb\_can\_edit\_lead\_template'\] - TEXT -

\['user'\]\['userdb\_send\_notifications\_to\_floor'\] - BOOL - 1 = yes

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['user\_id'\] - INT - This is the numeric OR userdb\_id of the
user you wish to access.

$data\['resource'\] - TEXT - Type of user to read: 'agent' or 'member'

$data\['fields'\] - ARRAY/TEXT - This is an optional array of fields to
retrieve, if left empty or not passed all fields and info will be
retrieved.

  

Example: retrieve all info for all fields for Agent, user\_id \#4

&lt;?php

       //retrieve all info for Agent, user\_id \#4

       $result = $api-&gt;load\_local\_api('user\_\_read',array(

               'user\_id'=&gt;4,

               'resource' =&gt; 'agent'

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

    print\_r($result);

echo '&lt;/pre&gt;';

?&gt;

  

Example: retrieve specific field info info for a Member, userdb\_id \#30

&lt;?php

       //gets specific info for Member, userdb\_id \#30

       $result = $api-&gt;load\_local\_api('user\_\_read',array(

               'user\_id' =&gt; 30,

               'resource' =&gt; 'member',

               'fields' =&gt; array(

                       'userdb\_emailaddress',

                       'userdb\_creation\_date',

                       'userdb\_last\_modified',

                       'userdb\_hit\_count'

               )

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

    print\_r($result);

echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

search($data) - Used to search user data:

search(array $data) : array

  

Returns: (array)

The array returned will contain the following:

\['error'\] - TRUE/FALSE - Error status of the search request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['user\_count'\] = Number of records/users found that match the search,
if using a limit this is only up to the number of records that match
your current limit/offset results.

\['users'\] = Array of user ID\#s that match the search.

\['info'\] = The info array contains benchmark information on the
search, includes: process\_time, query\_time, and total\_time

\['sortby'\] = Contains an array of fields that were used to sort the
search results. Note if you are doing a count only search the sort is
not actually used as this would just slow down the query.

\['sorttype'\] = Contains an array of the sorttype (ASC/DESC) used on
the sortby fields.

\['limit'\] - INT - The numeric limit being imposed on the results.

\['resource'\] - TEXT - The resource the search was made against,
'agent' or 'member'

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['resource'\] - TEXT - REQUIRED - Type of user to search: 'agent'
or 'member'. Used for min/max, number or price searches only.

$data\['parameters'\] - This is a REQUIRED array of the fields and the
values we are searching for.

$data\['sortby'\] - This is an optional array of fields to sort by.

$data\['sorttype'\] - This is an optional array of sort types (ASC/DESC)
to sort the sortby fields by.

$data\['offset'\] - This is an optional integer of the number of users
to offset the search by. To use offset you must set a limit.

$data\['limit'\] - This is an optional integer of the number of listings
to limit the search by. 0 or unset will return all listings.

$data\['count\_only'\] - This is an optional integer flag 1/0, where 1
returns a record count only, defaults to 0 if not set. Useful if doing
limit/offset search for pagination to get the initial full record
count..

  

example:

&lt;?php

       //get the userdb\_id for all Members active or not, sort results
by userdb\_id in ASC order

       $result = $api-&gt;load\_local\_api('user\_\_search',array(

               'parameters'=&gt;array(

                       'userdb\_is\_agent' =&gt;'no',

                       'userdb\_active' =&gt;'any'

               ),

               'sortby'=&gt;array('userdb\_id'),

               'sorttype'=&gt;array('ASC'),

               'resource' =&gt;'member',

               'limit'=&gt;0,

               'offset'=&gt;0,

               'count\_only'=&gt;0

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

update($data) - Used to update a specific user.

update(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the delete request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['user\_id'\] - INT - Numeric userdb\_id  ID\# of the updated user

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['user\_id'\] - INT - REQUIRED - This is the userdb\_id ID of the
user you are updating.

$data\['user\_details'\] - ARRAY - An array containing one or more of
the following OPTIONAL keys.

$data\['user\_details'\]\['active'\] - BOOL - OPTIONAL - Set true to
make this listing Active. Only set if you need to change.

$data\['user\_details'\]\['blog\_user\_type'\] - INT - OPTIONAL - The
Agent's blogging privilege level:

1 = Subscriber

2 = Contributor

3 = Author

4 = Editor

$data\['user\_details'\]\['can\_edit\_agent\_template'\] - BOOL -
OPTIONAL - Agent can create and edit Agent fields

$data\['user\_details'\]\['can\_edit\_all\_leads'\] - BOOL - OPTIONAL -
Agent can create and edit any leads in the Lead Manager

$data\['user\_details'\]\['can\_edit\_all\_listings'\] - BOOL - OPTIONAL
- Agent can create and edit other Agent's Listings

$data\['user\_details'\]\['can\_edit\_all\_users'\] - BOOL - OPTIONAL -
Agent can edit other users via User Manager

$data\['user\_details'\]\['can\_edit\_expiration'\] - BOOL - OPTIONAL -
Agent can set listing expiration dates.

$data\['user\_details'\]\['can\_edit\_lead\_template'\] - BOOL -
OPTIONAL - Agent can create and edit Lead Manager fields

$data\['user\_details'\]\['can\_edit\_listing\_template'\] - BOOL -
OPTIONAL - Agent can create and edit Listing  fields

$data\['user\_details'\]\['can\_edit\_member\_template'\] - BOOL -
OPTIONAL - Agent can create and edit Member fields

$data\['user\_details'\]\['can\_edit\_pages'\] - BOOL - OPTIONAL - Agent
can create and edit Page Editor Pages

$data\['user\_details'\]\['can\_edit\_property\_classes'\] - BOOL -
OPTIONAL - Agent can create and edit Property Classes

$data\['user\_details'\]\['can\_edit\_site\_config'\] - BOOL - OPTIONAL
- Agent can access and set Site Config options.

$data\['user\_details'\]\['can\_export\_listings'\] - BOOL - OPTIONAL -
Agent can set listings to be featured.

$data\['user\_details'\]\['can\_feature\_listings'\] - BOOL - OPTIONAL -
Agent can set listings to be featured.

$data\['user\_details'\]\['can\_have\_files'\] - BOOL - OPTIONAL - Agent
can upload listing files such as PDF.

$data\['user\_details'\]\['can\_have\_user\_files'\] - BOOL - OPTIONAL -
Agent can upload files associated with their profile

$data\['user\_details'\]\['can\_have\_vtours'\] - BOOL - OPTIONAL -
Agent can upload listing vtours.

$data\['user\_details'\]\['can\_manage\_addons'\] - BOOL - OPTIONAL -
Agent can operate add-on Manager

$data\['user\_details'\]\['can\_moderate'\] - BOOL - OPTIONAL - Agent
can moderate other users

$data\['user\_details'\]\['can\_view\_logs'\] - BOOL - OPTIONAL - Agent
can view the Activity Log

$data\['user\_details'\]\['comments'\] - TEXT - OPTIONAL - Comment. Not
visible to regular users

$data\['user\_details'\]\['email\_verified'\] - BOOL - OPTIONAL - This
user was email verified when the account was created.

$data\['user\_details'\]\['emailaddress'\] - TEXT - The user's email
address

$data\['user\_details'\]\['featuredlistinglimit'\] - INT - OPTIONAL -
The maximum number of listings an Agent can set to be featured.

$data\['user\_details'\]\['hit\_count'\] - INT - OPTIONAL - THe number
of hits (views) the Agent has received.

$data\['user\_details'\]\['limit\_listings'\] - INT - OPTIONAL - The
maximum number of listings an Agent can create. -1 = unlimited.

$data\['user\_details'\]\['rank'\] - INT - OPTIONAL - The user's order
or rank. 0 = hidden.

$data\['user\_details'\]\['send\_notifications\_to\_floor'\] - BOOL -
OPTIONAL - Automatically forward this Agent's leads to the floor Agent.

$data\['user\_details'\]\['user\_first\_name'\] - TEXT - OPTIONAL - The
user's first name

$data\['user\_details'\]\['user\_last\_name'\] - TEXT - OPTIONAL - The
user's last name

$data\['user\_details'\]\['user\_password'\] - - TEXT - OPTIONAL - The
user's password..

  

example:

&lt;?php

//updates an existing user, setting it to inactive

//and changing its password, and phone number custom user field at the
same time

$result = $api-&gt;load\_local\_api('user\_\_update',array(

  'user\_id' =&gt; 30,

  'user\_details'=&gt;array(

    'active' =&gt; false,

    'user\_password' =&gt;'password',

  ),

  'user\_fields' =&gt; array(

    'phone' =&gt; '555-555-1212',

  )

));

// format and output the contents of the $result array so the keys

// and values can be seen.

echo '&lt;pre&gt;';

print\_r($result);

echo '&lt;/pre&gt;';

&lt;/code&gt;&lt;/pre&gt;

?&gt;


