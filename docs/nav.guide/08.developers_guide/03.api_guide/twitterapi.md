# twitter\_api

The twitter API contains the API method for posting to twitter

  

Methods: twitter\_\_

  

post($data) - Used to create a new property class.

post(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the post attempt

\['message'\] - TEXT - The final posted message, or an error message if
\['error'\] is true.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['message'\] - TEXT - This is a message 140 chars or less that you
want to post to twitter.

  

example:

  

       $result = $api-&gt;load\_local\_api('twitter\_\_post', array(

               'message'=&gt; 'A message of less than 140 characters'

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';


