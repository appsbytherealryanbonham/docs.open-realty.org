# listing\_api

The listings API contains all API methods for creating and retrieving
listing data.

  

Methods: listing\_\_

  

create($data) - Used to create a new listing:

create(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the create request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['listing\_id'\] - INT - Numeric listing ID\# of the newly created
listing

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['class\_id'\] - This should be an integer of the class\_id that
this listing is assigned to.

$data\['listing\_details'\] - This should be an array containing the
following.

$data\['listing\_details'\]\['title'\] - REQUIRED. This is the
descriptive title for the listing.

$data\['listing\_details'\]\['seotitle'\] - OPTIONAL. If not set the API
will create a SEO friendly title based on the title supplied. The
seotitle must be unique, so the any you supply will be modified
automatically if needed.

$data\['listing\_details'\]\['notes'\] - OPTIONAL - Privatenotes about
this listing, only visible to admin and agents.

$data\['listing\_details'\]\['featured'\] - REQUIRED - Boolean - Is this
a featured listing. TRUE/FALSE

$data\['listing\_details'\]\['active'\] - REQUIRED - Boolean - Is this
an active listing. TRUE/FALSE

$data\['listing\_agents'\] - REQUIRED - This should be an array
containing the OR agent/user ID\# , the listing\_agents key must be 0.

&lt;?php

       //This listing belongs to agent 5. All other keys are currently
ignored.

       $data\['listing\_agents'\]\[0\]=5;

?&gt;

  

$data\['listing\_fields'\] - This should be an array of the actual
listing data. The array keys should be the field name and the array
values should be the field values. Only valid fields will be used, other
data will be dropped.

&lt;?php

       // This example defines a field value of 126 for a field called
mls\_id

       // and a value of "126 E Buttler Ave" for the address field.

       $data\['listing\_fields'\] = array('mls\_id' =&gt;
126,'address'=&gt;'126 E Buttler Ave');

?&gt;

  

$data\['listing\_media'\] - Currently not used and MUST be an empty
array.

  

------------------------------------------------------------------------

  

delete($data) - Used to delete a listing

delete(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the delete request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['listing\_id'\] - INT - Numeric listing ID\# of the deleted listing

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['listing\_id'\] - INT - Listing ID to delete.

  

example:

&lt;?php

       //delete OR listing ID\# 1

       $result = $api-&gt;load\_local\_api('listing\_\_delete',array(

               'listing\_id'=&gt; 1

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

get\_statistics($data) - Used to obtain numeric listing statistics for a
specific listing data field.

get\_statistics(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the statistics request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

Any additional array keys returned will depend on the which statistics
function you passed via $data\['function'\] the keys will be named the
same as the function and will contain the result(s) of the calculation.
MIN or MAX calculations for instance return both a \['min'\] and
\['max'\] key and values.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

$data\['function'\] - TEXT - Which statistics function to use.
Possibilities are:  'max','min','avg','median','mode','range'

$data\['pclass'\] - ARRAY - Array containing the numeric OR property
class ID\#s you wish to use or use an empty array() for all

$data\['field\_name'\] - TEXT - Field Name of the OR field you wish to
use to calculate statistics for.

  

example:

&lt;?php

       // Does a median calculation on an OR 'price' field

       // using data from OR property class ID\# 1 only

       $result = $api-&gt;load\_local\_api('listing\_\_get\_statistics',
array(

               'function'=&gt; 'median',

               'field\_name' =&gt; 'price',

               'pclass' =&gt; array(1)

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

------------------------------------------------------------------------

  

read($data) - Used to read/retrieve listing data for a specific listing.

read(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the read request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['listing'\] - ARRAY - Array containing information and settings
relevant to the listing requested. Array keys within \['listing'\]
correspond to their OR listingsdb table column names (system fields) or
custom field names. The following keys refer to OR's system fields which
are always available, any custom (user definable) fields such as "price"
or "bedrooms" will be unique to each OR install.

\['listing'\]\['listingsdb\_id'\] - INT - The numeric listing ID\# of
the listing

\['listing'\]\['listingsdb\_pclass\_id'\] - INT - The numeric
pclass\_id\# for teh OR Property class this listing belongs to.

\['listing'\]\['userdb\_id'\] - INT - The numeric user\_id\# for teh
Agent the listing belongs to

\['listing'\]\['listingsdb\_title'\] - TEXT - The descriptive title of
teh listing

\['listing'\]\['listingsdb\_expiration'\] - DATE - Date listing will
expire

\['listing'\]\['listingsdb\_notes'\] - TEXT - Private listing notes

\['listing'\]\['listingsdb\_creation\_date'\] - DATE - Listing creation
date

\['listing'\]\['listingsdb\_last\_modified'\] - DATE - Listing last
Modified date

\['listing'\]\['listingsdb\_hit\_count'\] - INT - Number of times
listing has ben viewed if hit counter enabled

\['listing'\]\['listingsdb\_featured'\] - YES/NO - Featured Listing
status

\['listing'\]\['listingsdb\_active'\] - YES/NO - Listing active status

\['listing'\]\['listingsdb\_mlsexport'\] - YES/NO - Legacy add-on
support

\['listing'\]\['listing\_seotitle'\] - TEXT - The SEO friendly version
of the listing title

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['listing\_id'\] - INT - This is the numeric OR Listing ID\# of
the listing you wish to access.

$data\['fields'\] - ARRAY/TEXT - This is an optional array of fields to
retrieve, if left empty or not passed all fields and info will be
retrieved.

  

Example: retrieve all info for all fields for listing ID \#1

&lt;?php

       //'fields' input array omitted. lookup everything for OR listing
ID\# 1

       $result = $api-&gt;load\_local\_api('listing\_\_read',array(

               'listing\_id'=&gt;1

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

Example: retrieve the contents of 2 OR system fields and one custom
field: 'price' for listing ID \#1

&lt;?php

       // lookup the SEO title and creation date OR system fields

       // and "price" custom listing field for OR listing ID\# 1

       $result = $api-&gt;load\_local\_api('listing\_\_read', array(

               'listing\_id'=&gt;1,

               'fields' =&gt; array (

                       'listing\_seotitle',

                       'listingsdb\_creation\_date',

                       'price'

               )

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

search($data) - Used to search listing data:

search(array $data) : array

  

Returns: (array)

The array returned will contain the following:

\['error'\] = TRUE/FALSE

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['listing\_count'\] = Number of records found, if using a limit this is
only the number of records that match your current limit/offset results.

\['listings'\] = Array of listing IDs.

\['info'\] = The info array contains benchmark information on the
search, including process\_time, query\_time, and total\_time

\['sortby'\] = Contains an array of fields that were used to sort the
search results. Note if you are doing a count only search the sort is
not actually used as this would just slow down the query.

\['sorttype'\] = Contains an array of the sorttype (ASC/DESC) used on
the sortby fields.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['parameters'\] - This is a REQUIRED array of the fields and the
values we are searching for.

$data\['sortby'\] - This is an optional array of fields to sort by.

$data\['sorttype'\] - This is an optional array of sort types (ASC/DESC)
to sort the sortby fields by.

$data\['offset'\] - This is an optional integer of the number of
listings to offset the search by. To use offset you must set a limit.

$data\['limit'\] - This is an optional integer of the number of listings
to limit the search by. 0 or unset will return all listings.

$data\['count\_only'\] - This is an optional integer flag 1/0, where 1
returns a record count only, defaults to 0 if not set. Useful if doing
limit/offset search for pagination to get the initial full record
count..

example:

&lt;?php

       // returns an array, $result that contains a maximum of 5
featured listings from property class ID\#1

       // sorted by the values of the 'price' field in ASC order

       $result = $api-&gt;load\_local\_api('listing\_\_search',array(

               'parameters'=&gt;array(

                       'featuredOnly'=&gt;'yes',

                       'pclass'=&gt;array(1)

               ),        

               'sortby'=&gt;array('price'),

               'sorttype'=&gt;array('ASC'),

               'limit'=&gt;5,

               'offset'=&gt;0,

               'count\_only'=&gt;0

       ));

       

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

update($data) - Used to update a specific listing.

update(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the delete request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['listing\_id'\] - INT - Numeric listing ID\# of the updated listing

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['listing\_id'\] - INT - This is the Listing ID of the listing you
are updating.

$data\['class\_id'\] - INT - The numeric class\_id that this listing is
currently assigned to.

$data\['listing\_details'\] - ARRAY - An array containing the following
three keys.

$data\['listing\_details'\]\['title'\] - TEXT - The listing title. Set
only if you want to change the existing title.

$data\['listing\_details'\]\['seotitle'\] - TEXT - The SEO title. Set
only if you want to change the existing one, or set to 'AUTO' to have
the system generate a new SEO title based on the \['title'\] you have
updated.

$data\['listing\_details'\]\['featured'\] -  TRUE/FALSE - Set true if
this is to be a featured listing. Only set if you need to change it.

$data\['listing\_details'\]\['active'\] - TRUE/FALSE - Set true to make
this listing Active. Only set if you need to change.

$data\['listing\_agents'\] - ARRAY - This sets the listing's agent ID,
the listing agent ID must always be key 0. example:

&lt;?php

       //This listing is assigned to agent 5. All keys other than \[0\]
are currently ignored.

       $data\['listing\_agents'\]\[0\]=5;

?&gt;

  

$data\['listing\_fields'\] - This should contain an array of the listing
field names and value pairs for any listing data you wish to update. The
array keys should reference the field\_name in OR and the array values
should contain the field values, i.e. the data you want to insert. Only
valid matching listing fields will be used, other data will be
discarded. example:

&lt;?php

       //This sets a field value of 126 for a field named 'mls\_id' and

       //a field value of "126 E Buttler Ave" for the 'address' field.

       $data\['listing\_fields'\] = array('mls\_id' =&gt; 126,
'address'=&gt; '126 E Buttler Ave');

?&gt;


