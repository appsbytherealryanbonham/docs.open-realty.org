# Invoking and accessing the API

There are 3 ways of invoking and accessing the Open-Realty API:

  

1\. Invoke internally from local add-ons, or hooks.

Using the API from within an  add-on is very simple.

  

-   Declare the global variable $api in your function
-   Call $api-&gt;load\_local\_api('API\_FUNCTION',$params);

               \*where API\_FUNCTION is a valid API function

  

Example: Add a remotely linked photo to a specific listing, using the
'media\_\_create' API function:

function myaddon\_addPhotoFromURL(){

       global $api;

       //Create the Media Object(photo) we want to add

     
 $mediaobject\['some\_photo.jpg'\]\['data'\]='http://www.somesite.com/some\_photo.jpg';

       //Tell the API that this media object should be linked, and not
stored locally.

       $mediaobject\['some\_photo.jpg'\]\['remote'\]=TRUE;

       //Link this remote photo to Listing ID\# 112

$result=$api-&gt;load\_local\_api(

               'media\_\_create', array(

                       'media\_parent\_id'=&gt;'112',

                       'media\_type'=&gt;'listingsimages',

                       'media\_data'=&gt;$mediaobject

               )

       );

       echo '&lt;pre&gt;'.print\_r($result,TRUE)."&lt;/pre&gt;\\r\\n";

}

?&gt;

  

2\. Invoke internally from other PHP scripts within the same hosting
account as your OR software.

This is also simple to do and works like the Internal API call in the
example above.

-   Include the api.inc.php file and load the api class.
-   Call $api-&gt;load\_local\_api('API\_FUNCTION',$params);

  

Example: adding a linked photo to a specific listing

&lt;?php

require('/path/to/open-realty/api/api.inc.php');

    $api = new api();

       //Define the Media Object(photo) we want to add

     
 $mediaobject\['some\_photo.jpg'\]\['data'\]='http://www.somesite.com/some\_photo.jpg'\];

       //Instruct the API that this media object should be linked, and
not stored locally.

       $mediaobject\['some\_photo.jpg'\]\['remote'\]= TRUE;

       //Add this photo to Listing 112 using the 'media\_\_create' API
function

    $result = $api-&gt;load\_local\_api('media\_\_create', array(

               'media\_parent\_id'=&gt;'112',

               'media\_type'=&gt;'listingsimages',

               'media\_data'=&gt;$mediaobject

       ));

       echo '&lt;pre&gt;'.print\_r($result,TRUE)."&lt;/pre&gt;\\r\\n";

?&gt;

  

3\. Invoke externally from remote websites or apps using remote API
calls to connect to a host OR web site.

Invoking and using the API remotely is also very straightforward. The
remote website or app does not have to be using OR itself, but must have
PHP available and be able to make outbound HTTP/HTTPS
connections to the host OR site.

  

-   Copy the /api/api.inc.php file from the host OR site to a location
    on the remote web site where you want to run the remote api commands
    from.
-   Create a file in the same location as the api.inc.php file named
    'common.php' as follows. You then need to place the API Key from the
    remote OR site you want to connect to (obtain from its Site Config)
    within this file and assign it to the "apikey" $config array.

  

custom common.php file:

&lt;?php

       global $config;

       $config\['apikey'\] = 'THE OR HOST SITE API KEY GOES HERE';

?&gt;

  

-   Setup/create your remote API commands in a different file such as
    example.php or within your custom php script, the following is an
    example for creating a new property class using a remote API call..

  

example.php. This example creates a new "Residential Property" property
class on the distant host OR site and echos its new class ID\# if
successful.

&lt;?php

//include the API file, assumes it is in the same folder as this

       require dirname(\_\_FILE\_\_).'/api.inc.php';

       $orapi = new api();

       //Define API Server. This is the host OR site you wish to connect
to

       define('API\_SERVER','http://yourwebsite.com/admin/index.php');

       //Set login credentials:

       //This does not have to use the Admin account,

       //but specific account permissions are enforced.

       define('API\_USER','admin');

       define('API\_PASS','password');

       //Create a new property class named Residential Property

       //using the 'pclass\_\_create' API function

       $result = $orapi-&gt;send\_api\_command(

               API\_SERVER,

               'pclass\_\_create', array(

                       'class\_system\_name'=&gt;'Residential Property',

                       'class\_rank'=&gt;1

               ),

               API\_USER,

               API\_PASS

       );

       if($result\['api\_result\_code'\] != FALSE){

               echo '&lt;pre&gt;Failed Creating Class:
'.print\_r($result,TRUE).'&lt;/pre&gt;';

               die;

       }

       else{

               $class\_id =$result\['api\_result'\]\['class\_id'\];

       }

       echo $class\_id;

?&gt;

  

The examples for the methods in the following sections assume you are
invoking the API locally as in \#1 above.

