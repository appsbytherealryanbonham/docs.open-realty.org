# Api Guide

Starting with Open-Realty v3.0.4, a programming API has been available
for use to access and modify listing and other data. We will be
expanding the API to cover all of the core functionality of Open-Realty
over time.

  

Like any modern API, Open-Realty's makes extensive use of variable
arrays to store and manipulate information. If you use an API function
to request information you will interact with it using one or more
arrays, and it is going to return one or more arrays of
results/information back to you which you will can then parse for your
needs. Arrays can be compared, flipped, ordered, pruned, sorted, and
manipulated in all sorts of useful ways.

  

Useful info regarding PHP arrays, and multi-dimensional arrays, and PHP
array functions:

<http://php.net/manual/en/language.types.array.php>

<http://www.w3schools.com/php/php_arrays_multi.asp>

<http://php.net/manual/en/ref.array.php>

  

When working with arrays, especially multi-dimensional arrays it is
often necessary to view the contents. print\_r($array); and other usual
methods to view them alone can leave much to be desired in terms of
legibility. A simple way to make the contents of an array more legible
when viewing the contents follows and is used in several of the API
method examples and is useful for viewing any array.

  

&lt;?php

       // format and output the contents of the $result array so the
keys

       // and values are legible.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

  

If you ever want to know what is being returned by an API method so you
can view available keys, values or etc, simply use the above to view
your $result array.

  

You can learn how to invoke the Open-Realty API for use locally or
remotely from external web sites and view a full list of all API
Commands, and their methods in the next sub-sections of this
documentation.

  

  

