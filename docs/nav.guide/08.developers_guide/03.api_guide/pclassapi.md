# pclass\_api

The pclass API contains all API methods for creating new and retrieving
existing property class data.

  

Methods: pclass\_\_

  

create($data) - Used to create a new property class.

create(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the creation request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['class\_id'\] - INT - Numeric pclass ID\# assigned to the new class

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['class\_system\_name'\] - The Name of the Property Class

$data\['class\_rank'\] - Rank that Class should be displayed in

$data\['field\_id'\] - Optional Array or Field IDs that should be
assigned to this property class on creation. If used this MUST be an
array.

  

&lt;?php

       //Create a new property class named Residential Property

       // and set its rank to 1 using the 'pclass\_\_create' API
function

       $result = $api-&gt;load\_local\_api('pclass\_\_create', array(

               'class\_system\_name'=&gt;'Residential Property',

               'class\_rank'=&gt;1

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

------------------------------------------------------------------------

  

  

read($data) - Used to read detailed information from a single property
class.

read(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the read request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['class\_id'\] - INT - Numeric class ID\#

\['class\_name'\] - TEXT - The name of the Property Class

\['class\_rank'\] - INT - The numeric rank of the Property Class

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['class\_id'\] - The ID of the property class to return
information from.

  

------------------------------------------------------------------------

  

metadata($data) - Used to get information on  ALL property classes.

metadata(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the read request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['metadata'\] - ARRAY - multi dimensional array where they key is the
property class ID\#.

\['metadata'\]\[PCLASSID'\]\['name'\] - TEXT - The name of the Property
Class

\['metadata'\]\[PCLASSID'\]\['rank'\] - INT - The numeric rank of the
Property Class

  

Parameters:

$data (array)

Expects an empty array: array()

  

example:

&lt;?php

       $result =
$api-&gt;load\_local\_api('pclass\_\_metadata',array());

?&gt;

  


