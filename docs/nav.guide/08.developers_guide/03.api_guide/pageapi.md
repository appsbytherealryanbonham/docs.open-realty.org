# page\_api

The page API contains all API methods for creating and deleting Page
Editor Pages                                                      
 (added in v3.3.0)

  

Methods: page\_\_

  

create($data) - Used to create a new Page Editor Page

create(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the create request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['pagesmain\_id'\] - INT - Numeric Page ID\# of the newly created Page
Editor Page.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['pagesmain\_title'\] - TEXT - REQUIRED - The Friendly Title of
your Page, e.g.: "About us".

$data\['pagesmain\_full'\] - TEXT - OPTIONAL - The HTML content (markup)
to be used for the Page".

$data\['pagesmain\_published'\] - INT - OPTIONAL - The numeric publish
status of the Page. 0 = Draft  1= Live. Default value is: 0/Draft

$data\['pagesmain\_description'\] TEXT - OPTIONAL - HTML Meta
Description. A 160 character or less summary of the page contents.

$data\['pagesmain\_keywords'\] TEXT - OPTIONAL - Comma delimited list of
HTML Meta Keywords.

  

Example: Create a new Page that is status: Live. Insert the page's
Title, HTML markup, Meta Description, and Keywords.

  

//Create a new Page

       $result = $api-&gt;load\_local\_api('page\_\_create',array(

               'pagesmain\_title' =&gt; 'Hello World!',

               'pagesmain\_full' =&gt; '&lt;h1&gt;Hello
World!&lt;/h1&gt;

                                                             
 &lt;div&gt;

                                                                     
 Example: Coming Soon...

                                                             
 &lt;/div&gt;',

               'pagesmain\_published' =&gt; 1,

               'pagesmain\_description' =&gt; 'My first "Hello World"
page',

               'pagesmain\_keywords' =&gt; 'Hello, World, Example'

       ));

       if($result\['error'\]){

               //If an error occurs die and show the error msg;

               die($result\['error\_msg'\]);

       }

  

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

  

  

  

update($data) - Used to insert a Page Editor Page

update(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the create request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['pagesmain\_id'\] - INT - Numeric page ID\# of the newly created Page
Editor Page.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['pagesmain\_id'\] - INT - REQUIRED - The numeric ID\# of the Page
Editor page to be deleted.

$data\['pagesmain\_title'\] - TEXT - OPTIONAL - The Friendly Title of
your Page, e.g.: "About us".

$data\['pagesmain\_full'\] - TEXT - OPTIONAL - The HTML content (markup)
to be used for the Page".

$data\['pagesmain\_published'\] - INT - OPTIONAL - The numeric publish
status of the Page. 0 = Draft  1= Live. Default value is: 0 (Draft).

$data\['pagesmain\_description'\] TEXT - OPTIONAL - HTML Meta
Description. A 160 character or less summary of the page contents.

$data\['pagesmain\_keywords'\] TEXT - OPTIONAL - Comma delimited list of
HTML Meta Keywords

$data\['page\_seotitle'\] TEXT - OPTIONAL - Properly formatted SEO title
to be used as the HTML Page Name. Should not contain spaces or special
characters that are not URL encoded. Will be automatically generated if
you change the 'pagesmain\_title'.

  

Note: If you attempt to change the 'pagesmain\_title'  and
'page\_seotitle' at the same time, 'page\_seotitle' will be ignored in
favor of the new Page SEO title that was generated as a result of
submitting a 'pagesmain\_title' change.

  

Example: Update Page ID\# 7

  

//Update an existing Page ID\# 7

       $result = $api-&gt;load\_local\_api('page\_\_update',array(

               'pagesmain\_id' =&gt; 7,

               'pagesmain\_title' =&gt; 'Hello World! 2',

               'pagesmain\_full' =&gt; '&lt;h1&gt;Hello World\\'s page!
2&lt;/h1&gt;

                                               &lt;div&gt;

                                                       Example: Coming
Sooner...

                                               &lt;/div&gt;',

               'pagesmain\_published' =&gt; 1,

               'pagesmain\_description' =&gt; 'My first "Hello World"
page take 2',

               'pagesmain\_keywords' =&gt; 'Hello, World, Example,
Sooner',

               'page\_seotitle' =&gt; 'this seo title will be replaced
anyway',

       ));

       if($result\['error'\]){

       //If an error occurs die and show the error msg;

               die($result\['error\_msg'\]);

       }

  

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;'; =
$api-&gt;load\_local\_api('page\_\_update',array(

  

  

  

delete($data) - Used to delete a Page Editor Page

delete(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the create request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['pagesmain\_id'\] - INT - Numeric page ID\# of the newly created lPage
Editor Page.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['pagesmain\_id'\] - INT - REQUIRED - The numeric ID\# of the Page
Editor page to be deleted.

  

Example: Delete Page ID\# 7

  

       //Delete Page \#7

       $result = $api-&gt;load\_local\_api('page\_\_delete',array(

               'pagesmain\_id'=&gt; 7

       ));

       if($result\['error'\]){

               //If an error occurs die and show the error msg;

               die($result\['error\_msg'\]);

       }        

               

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

  


