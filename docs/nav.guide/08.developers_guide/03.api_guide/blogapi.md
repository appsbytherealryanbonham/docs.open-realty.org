# blog\_api

(added in v3.2.11)

The blog API contains all API calls for creating and retrieving media
(files/photos/vtours) info.

  

Methods: blog\_\_    

search($data) - Used to search blog data.

search(array $data) : array

  

Returns: (array)

The array returned will contain the following:

\['error'\] - TRUE/FALSE - Error status of the search request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['blog\_count'\] = Number of Blog articles found that match the search,
if using a limit this is only up to the number of records that match
your current limit/offset results.

\['blogs'\] = Array of blogmain\_id\#s that match the search.

\['info'\] = The info array contains benchmark information on the
search, includes: process\_time, query\_time, and total\_time

\['sortby'\] = Contains an array of fields that were used to sort the
search results. Note: if you are doing a count only search the sort is
not actually used as this would just uselessly slow down the query.

\['sorttype'\] = Contains an array of the sorttype (ASC/DESC) used on
the sortby fields.

\['limit'\] - INT - The numeric limit being imposed on the results.

\['offset'\] - INT - The numeric offset that was used to generate these
results

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['parameters'\]\['FIELDNAME'\] - REQUIRED - Array of the fields
and the values we are searching for. At least one option below is
required:

blogmain\_id - INT - the ID\# of the blog

userdb\_id - INT - The OR User ID\# of the blog's creator

blogmain\_title - TEXT - One or more words of the expected Blog Title

blog\_creation\_date\_greater - TIMESTAMP

blog\_creation\_date\_less - TIMESTAMP

blog\_creation\_date\_equal\_days - TIMESTAMP

blog\_creation\_date\_greater\_days - TIMESTAMP

blog\_creation\_date\_less\_days - TIMESTAMP

blogmain\_full - TEXT - A word or phrase expected to be in an existing
Blog Article. Note: all markup is stripped before comparison when this
option is used, searching for HTML or similar markup elements is not
possible.

blogmain\_description - TEXT -  A word or phrase expected to be in the
Meta Description of the Blog Article

blogmain\_keywords - TEXT -  One or more Meta Keywords expected to be
set as the Meta Keywords of the Blog Article

blogmain\_published - INT/TEXT - The Blog publish status: Options: any =
all statuses, 0 = Draft, 1 = Live , 2 = Review. providing any other
value will default to "Live"

blog\_categories - ARRAY - INT -  An array of the numeric blog
category\_id \#s

blog\_post\_tags - ARRAY - INT -  An array of the numeric blog tag\_id
\#s

  

$data\['sortby'\] - This is an optional array of fields to sort
by.Options are::

blogmain\_id

userdb\_id

blogmain\_title

blogmain\_date

blogmain\_published

random

$data\['sorttype'\] - This is an optional array of sort types (ASC/DESC)
to sort the sortby fields by.

$data\['offset'\] - This is an optional integer of the number of Blog
articles to offset the search by. To use offset you must also set a
limit.

$data\['limit'\] - This is an optional integer of the number of blogs to
limit the results by. 0 or unset will return all blogs. Only active/live
blogs will be returned if the user is not logged into an account with
permission.

$data\['count\_only'\] - This is an optional integer flag 1/0, where 1
returns a record count only, defaults to 0 if not set. Useful if doing
limit/offset search for pagination to get the initial full record
count..

  

example:

&lt;?php

       //get the blogmain\_id for all the blogs with a published status
of Live for User ID \#2 that contain the text "repo" in the blog
article. Sort results by blog date in DESC order

       $result = $api-&gt;load\_local\_api('blog\_\_search',array(

        'parameters'=&gt;array(

            'blogmain\_published' =&gt;1,

            'userdb\_id'=&gt;2

        ),

        'sortby'=&gt;array('blogmain\_date'),

        'sorttype'=&gt;array('DESC'),

        'limit'=&gt;10,

        'offset'=&gt;0,

        'count\_only'=&gt;0

    ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

read($data) - Used to read/retrieve data from a specific blog.

read(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the read request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['blog'\] - ARRAY - Array containing information relevant to the blog
requested. Array keys within \['blog'\] correspond to their OR blogmain
table column names.

\['blog'\]\['blogmain\_id'\] - INT - Blog Article ID\#

\['blog'\]\['userdb\_id'\] - INT - The userdb\_id of the Blog's Author

\['blog'\]\['blogmain\_title'\] - TEXT - The Title of the Blog Article

\['blog'\]\[blog\_seotitle\] - TEXT - The SEO friendly page title

\['blog'\]\[blogmain\_date\] - TIMESTAMP - UNIX Timestamp of the
Article's creation date

\['blog'\]\[blogmain\_full\] - TEXT/MARKUP - The raw HTML markup and
text of the Blog article.

\['blog'\]\[blogmain\_description\] - TEXT - The Meta Description of the
Blog Article

\['blog'\]\[blogmain\_keywords\] - TEXT - The Meta keywords, comma
delimited e.g. keyword1, keyword2, etc

\['blog'\]\[blogmain\_published\] - INT  - Published status of the Blog
article. 0 = Draft, 1 = Live, 2 = Pending Review

\['blog'\]\['blog\_url'\] - TEXT - URL - Full text URL to the blog page,
uses SEO friendly if set

\['blog'\]\['blog\_author\_firstname'\] - TEXT - The Blog Author's First
Name

\['blog'\]\['blog\_author\_lastname'\] - TEXT - The Blog Author's Last
Name

\['blog'\]\['blog\_categories'\] - ARRAY - Each array key is the
category\_id and each value is the category\_name the blog is assigned
to.

\['blog'\]\['blog\_post\_tags'\]\['\#'\] - MULTI ARRAY - A
multi-dimensional array of each Blog tag\_id\# assigned to the blog.
Contains keys:

\['\#'\] \['tag\_name'\] - TEXT - The blog tag name

\['\#'\] \['tag\_seoname'\] - TEXT - the blog tag SEO name

\['\#'\] \['tag\_description'\] - TEXT - Description of the blog tag

\['\#'\] \['tag\_link'\] - TEXT - URL - Full text URL to the Blog page
for blogs that use this tag.

\['blog'\]\['blog\_comment\_count'\] - INT - The number of blog comments

\['blog'\]\['blog\_comments'\] - TEXT - The Blog Author's First Name

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['blog\_id'\] - INT - REQUIRED This is the numeric OR blogmain\_id
of the Blog Article you wish to access.

$data\['fields'\]\['FIELDNAME'\] - ARRAY/TEXT - OPTIONAL This is an
optional array of fields to retrieve, if left empty or not passed all
fields and info will be retrieved. Applicable field names are as
follows:

blogmain\_id

userdb\_id

blogmain\_title

blog\_seotitle

blogmain\_date

blogmain\_full

blogmain\_description

blogmain\_keywords

blogmain\_published

blog\_url

blog\_author\_firstname

blog\_author\_lastname

blog\_categories

blog\_post\_tags

blog\_comment\_count

blog\_comments

  

Example: retrieve all info for all fields for Blog article blogmain\_id
\#4

&lt;?php

       //retrieve all info for Blog Article ID \#4

       $result = $api-&gt;load\_local\_api('blog\_\_read',array(

               'blog\_id'=&gt;4

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

    print\_r($result);

echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

delete($data) - Used to delete a blog article completely. Any associated
media belonging to it is also removed.

delete(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the delete request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['blog\_id'\] - INT - Numeric blogmain\_id of the Blog Article deleted.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['blog\_id'\] - INT - Numeric blogmain\_id of the blog to delete.

  

example:

&lt;?php

       //deletes userdb\_id \#4 and any associated listings and media

       $result = $api-&gt;load\_local\_api('blog\_\_delete',array(

                               'blog\_id'=&gt;4

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  


