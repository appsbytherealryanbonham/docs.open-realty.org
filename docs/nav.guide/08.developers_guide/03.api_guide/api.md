# api

The load\_local\_api() function is used to initiate local api commands
from within add-ons, hooks, and from within the same hosting account as
the Open-realty software is running in:

  

load\_local\_api(string $api\_command, array $api\_data, string
$api\_user, string $api\_password) : array

  

Returns: (array)

  

Parameters:

$api\_command (string)

Name of the api command to run. Example: 'pclass\_\_create'

  

$api\_data (array)

REQUIRED - Array of data to pass to the API Command. See Each API
Command for details for what each expects to be passed to it.

  

$api\_user (string)

OPTIONAL user to run the api command as. This is only required if called
from outside of Open-Realty.

  

$api\_password (string)

OPTIONAL password for the user running api command. This is only
required if called from outside of Open-Realty.

  

------------------------------------------------------------------------

  

The send\_api\_command()function is just like the  load\_local\_api()
function except it is used to initiate api commands on a distant
Open-Realty (host) site from a location outside of where that
Open-Realty site is hosted/installed (remote). The remote site does not
have to be running Open-Realty, but will need PHP available.

  

send\_api\_command(string $api\_server, string $api\_command, array
$api\_data, string $api\_user, string $api\_password) : array

  

Returns: (array)

  

Parameters:

$api\_server (string)

REQUIRED - URL of the Open-Realty site to execute the api command on. Ex
http://yourdomain.com/admin/index.php

  

$api\_command (string)

REQUIRED - Name of the api command to run. Example. pclass\_\_create

  

$api\_data  (array)

REQUIRED - Array of data to pass to the API Command. See Each API
Command for details on what to pass.

  

$api\_user (string)

REQUIRED - OR username to run the api command as.

  

$api\_password (string)

REQUIRED - Password for the user running the api command.

  

  


