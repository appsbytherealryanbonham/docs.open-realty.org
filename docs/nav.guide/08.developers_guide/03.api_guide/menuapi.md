# menu\_api

The menu API contains all API methods for creating, modifying, and
reading menus.

  

Methods: menu\_\_

  

create($data) - Used to create a new menu.

create(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

delete($data) - Used to delete a menu.

delete(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

add\_menu\_item($data) - Used to create a new media item:

add\_menu\_item(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

delete\_menu\_item($data) - Used to delete a menu item.

delete\_menu\_item(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

item\_details($data) - Used to get menu item details:

item\_details(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

metadata($data) - Used to get a menu item's metadata (info):

metadata(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

read($data) - Used to read a menu item.

read(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

save\_menu\_item($data) - Used to save a menu item:

save\_menu\_item(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  

set\_menu\_order($data) - Used to set menu order.

set\_menu\_order(array $data) : array

  

Returns: (array)

  

Parameters:

$data (array)

Expects an array containing the following array keys:

  

  

------------------------------------------------------------------------

  


