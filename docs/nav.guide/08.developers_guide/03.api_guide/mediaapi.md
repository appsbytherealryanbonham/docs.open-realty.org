# media\_api

The media API contains all API methods for creating and retrieving media
(files/photos/vtours) info.

  

Methods: media\_\_

  

create($data) - Used to create a new media item:

create(array $data) : array

  

Returns: (array)

\['media\_error'\]\['MEDIA NAME'\] - FALSE - Returned on error. Key
includes name of media file

\['media\_response'\]\['MEDIA NAME'\] - TEXT - Response text of what
happened. Key includes name of media file.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['media\_parent\_id'\] - This should be either the User ID or
Listing ID that the media is to be associated with, whichever is
applicable to \['media\_type'\].

$data\['media\_type'\] - TEXT - Type of Media:
'listingsimages','listingsfiles','listingsvtours','userimages','usersfiles'

$data\['media\_data'\] -  ARRAY media\_data should be an array as
follows, the key being the FILENAME for the media file, example
"greenhouse.jpg".

$data\['media\_data'\]\['FILENAME'\]\['caption'\] = TEXT - OPTIONAL used
to set the media caption: "Your Caption goes here" .

$data\['media\_data'\]\['FILENAME'\]\['description'\] = TEXT - OPTIONAL
- used to set the media description: 'You description goes here" - .

$data\['media\_data'\]\['FILENAME'\]\['data'\] = BINARY / URL - REQUIRED
- This should be either the BINARY data for this media or a URL (link)
to the media.

$data\['media\_data'\]\['FILENAME'\]\['remote'\] = TRUE /FALSE -
OPTIONAL If set to true the media will only be linked to and not
downloaded. Only applies when the 'data' key contains a URL.

$data\['media\_data'\]\['FILENAME'\]\['rank'\] - INT - OPTIONAL -
Numeric ordering (rank) of the media. If left empty the media will be
assigned the next (last) available rank.

  

------------------------------------------------------------------------

  

delete($data) - Used to delete an existing media item.

delete(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the delete request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['status\_msg'\] - TEXT - Success message (if successful)

\['media\_type'\] - TEXT - Type of Media
('listingsimages','listingsfiles','listingsvtours','userimages','usersfiles')

\['media\_parent\_id'\] - INT - The numeric 'userdb\_id' or
'listingsdb\_id' of the item deleted, whichever is applicable to  the
\['media\_type'\] defined.

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['media\_type'\] - TEXT - Type of Media
('listingsimages','listingsfiles','listingsvtours','userimages','usersfiles')

$data\['media\_parent\_id'\] - INT-  This should be either the User ID
or Listing ID that the media is associated with, whichever is
applicable.

$data\[media\_object\_id\] - TEXT/EMPTY - Should be set to \* (asterisk)
for removing all items, or the numeric 'userdb\_id' or 'listingsdb\_id'
of a specific media item whichever is applicable to  the
\['media\_type'\] defined.

  

&lt;?php

       //deletes all user files for user\_id \#4

       $result = $api-&gt;load\_local\_api('media\_\_delete',array(

               'media\_type'=&gt;'usersfiles',

               'media\_parent\_id'=&gt;4,

               'media\_object\_id'=&gt;'\*'

       ));

       // format and output the contents of the $result array so the
keys

       // and values can be seen.

       echo '&lt;pre&gt;';

       print\_r($result);

       echo '&lt;/pre&gt;';

?&gt;

  

------------------------------------------------------------------------

  

read($data) - Used to retrieve OR listing photo information.

read(array $data) : array

  

Returns: (array)

\['error'\] - TRUE/FALSE - Error status of the read request

\['error\_msg'\] - TEXT - Error message returned (if \['error'\] =
TRUE).

\['media\_object'\]  - ARRAY- Array of media item info as follows.

\['media\_object'\]\['media\_id'\] - INT - listingsimages\_ID the media
item is assigned to.

\['media\_object'\]\['media\_rank'\] - INT - Rank of media item

\['media\_object'\]\['caption'\] - TEXT - Media item caption

\['media\_object'\]\['description'\] - TEXT - Media item description

\['media\_object'\]\['thumb\_file\_name'\] - TEXT - Listing photo image
thumbnail name. Will contain a URL if linked image

\['media\_object'\]\['file\_name'\] TEXT - Listing image name. Will
contain a URL if linked image

\['media\_object'\]\['thumb\_file\_src'\] - TEXT - URL to thumbnail
photo.

\['media\_object'\]\['file\_src'\] - TEXT - URL to primary photo.

\['media\_object'\]\['thumb\_width'\] - INT - Width of listing photo
thumbnail on disk (not applicable for linked photos)

\['media\_object'\]\['thumb\_height'\]  - INT - Height of listing photo
thumbnail on disk  (not applicable for linked photos)

\['media\_object'\]\['file\_width'\]  - INT - Width of full size listing
photo on disk  (not applicable for linked photos)

\['media\_object'\]\['file\_height'\] - INT - Height of full size
listing photo on disk  (not applicable for linked photos)

\['media\_object'\]\['remote'\] -  TRUE/FALSE - Location of the media
file. TRUE = Remote FALSE = Local

\['media\_count'\] - Number - Number of media\_objects (images) returned

  

Parameters:

$data (array)

Expects an array containing the following array keys:

$data\['media\_parent\_id'\] - INT - The Listing ID or User ID that the
media is associated with, whichever is applicable.

$data\['media\_type'\] - TEXT - Type of Media: 'listingsimages',
'listingsfiles', 'userimages', 'usersfiles' (Last 3 types added in OR
v3.3)

$data\['media\_output'\] - TEXT - 'DATA' will return the raw binary
data/image. 'URL' will return the link to the remote media. You may not
get all expected images if using DATA mode and you have a mix of local
and linked images, as remote images will not be returned.

$data\['media\_limit'\] - INT - Optional value to limit the number of
media results returned. If this option is not set, or is set to 0, all
available media items will be returned. Setting this to 1 will return
the first media item determined by the item's rank, 2 will return items
rank \#1 and \#2 if present. (new in OR v3.3)

  

  

example:

&lt;?php

       //Call the API and get all photo image info excluding binary file
data for Listing ID\# 4

       $result = $api-&gt;load\_local\_api('media\_\_read',array(

               'media\_type'=&gt;'listingsimages',

               'media\_parent\_id'=&gt;4,

               'media\_output'=&gt;'URL'

       ));

       if($result\['error'\]){

       //If an error occurs die and show the error msg;

               die($result\['error\_msg'\]);

       }

       //No error so display the thumbnail images that were returned.

       foreach($result\['media\_object'\] as $obj){

               echo '&lt;img src="'.$obj\['thumb\_file\_src'\].'"
width="'.$obj\['thumb\_width'\].'" height="'.$obj\['thumb\_height'\].'"
/&gt;&lt;br /&gt;';

       }

  

       //Call the API and return all uploaded file information for
listing ID\# 3

    $result = $api-&gt;load\_local\_api('media\_\_read',array(

            'media\_type'=&gt;'listingsfiles',

            'media\_parent\_id'=&gt;3,

            'media\_output'=&gt;'URL'

    ));

    if($result\['error'\]){

    //If an error occurs die and show the error msg;

            die($result\['error\_msg'\]);

    }

    echo '&lt;pre&gt;';

    print\_r($result);

    echo '&lt;/pre&gt;';

  

//Call the API and return all Agent photo info excluding binary file
data for User ID\# 3,

$result = $api-&gt;load\_local\_api('media\_\_read',array(

            'media\_type'=&gt;'userimages',

            'media\_parent\_id'=&gt;3,

            'media\_output'=&gt;'URL'

    ));

    if($result\['error'\]){

    //If an error occurs die and show the error msg;

            die($result\['error\_msg'\]);

    }

    //No error so display the thumbnail images that were returned.

    foreach($result\['media\_object'\] as $obj){

        echo '&lt;img src="'.$obj\['thumb\_file\_src'\].'"
width="'.$obj\['thumb\_width'\].'" height="'.$obj\['thumb\_height'\].'"
/&gt;&lt;br /&gt;';

  

    }

  

    echo '&lt;pre&gt;';

    print\_r($result);

    echo '&lt;/pre&gt;';

  

  

?&gt;


