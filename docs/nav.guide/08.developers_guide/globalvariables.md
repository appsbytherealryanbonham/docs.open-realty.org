# Global variables

The following global variables are available for declaring in add-ons.

example:

```php
<?php
       global $conn, $config, $misc;
?>
```

`$conn` - Contains the connection object to the Open-Realty database..
Open-Realty v3 uses ADODB Lite, see the ADODB Lite Documentation for a
list of functions you can use with the `$conn` object.

`$config` -  Array that contains the configuration options. Refer to OR's
/include/common.php file for a complete list of the `$config` array keys
that are available for your setup.

`$lang` - Contains the entire OR language variable array, e.g.:
`$lang['lang_var'];`

`$misc` - Contains the "miscellaneous" class object containing logging
utility functions such as `log_action($log)`.

`$api` - Contains the OR local [API](03.api_guide/apiguide.md) object

`$jscript` - String that contains the contents of `{load_js}` (javascript
inserted by OR) which is loaded in the `<head>` of your templates.
You can either concatenate your own additional JavaScript to add to OR's
or you can replace it entirely with your own, or insert CSS, or anything
that is valid within the HTML `<head>`.  Note: `{load_js}` is
deprecated as of OR v3.2.10 and is replaced by: `{load_ORjs}`

Concatenate:

```php
$jscript .= '<script type="text/javascript> alert("hello world");</script>';
```

```php
$jscript .= '<script type="text/javascript" src="http://link/to/some/JS\_lib.js"></script>';
```

```php
$jscript .= '<link href="http://fonts.googleapis.com/css?family=Arimo:regular" rel="stylesheet" type="text/css">';
```

Replace:

```php
$jscript =  '<script type="text/javascript" src="http://link/to/my/custom/JS\_lib.js"></script>';
```

`$jscript_last` - Same as \$jscript but references/replaces the
`{load_js_last}` tag in the template that occurs before the closing HTML
`</body>` tag.
