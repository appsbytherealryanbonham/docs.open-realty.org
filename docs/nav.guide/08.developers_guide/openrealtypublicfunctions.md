# Open-Realty Public Functions

The following public functions in Open-Realty are available for use by
add-on Developers and their corresponding files are located in the OR
/include/ folder.

### File: misc.inc.php

Class: misc

Functions:

`log_error($sql)` - Used to report Fatal SQL errors where `$sql` is the
(text) mySQL query. Sends the error by email to site Admin and halts the
script.

`log_action($log)`- Inserts a message `$log` (text) to the OR activity
log.

### File: core.inc.php - Contains the majority of OR's template enginemfunctionality.

Class: page_admin  or  page_user

The correct class to use depends on if you are working with a template
in the administration area of the site (page_admin) or the public side
(page_user).

Functions:

`auto_replace_tags($section = '', $admin = FALSE)`

`replace_blog_template_tags()`

`load_page($template)` - Loads a template file into the OR template
engine $page object for use with the following page functions. $template
must include the full file path to the template file.

example:

```php
<?php

global $config;

require_once($config['basepath'] . '/include/core.inc.php');

//invoke this as a user and not admin page

$page = new page_user();

$template = $config['template_path'] . '/my_template.html'

$page->load_page($template);

?>
```

`load_addon_file('ADDONNAME','mytemplate.html')` - For use in add-ons: Loads the template file specified as the second
variable from the /addons/ADDONNAME/template folder into the OR template engine `$page` object.

`get_template_section_row($section_name)`

`get_template_section($section_name, $page='')`

`cleanup_template_block($block, $section)` - Removes any matching opening and closing template `{$block_block}`
and `{/$block_block}` block tags from the `$section` string provided.

`remove_template_block($block, $section)` - Removes all markup/content between any matching opening and closing
template `{$block_block}` and `{/$block_block}` block tags from the `$section` string  provided.

`form_options($options,$selected_value, $template_section)`

`replace_tag($tag, $replacement)`

`parse_template_section($section_as_variable, $field, $value)`

`replace_template_section($section_name, $replacement, $page = '')`

`replace_template_section_row($section_name, $replacement)`

`cleanup_fields($section)`

`cleanup_images($section)`

`output_page()` - Echoes the page content instead of returning it to the function that
called it

return_page() - Returns the page content to whatever called it,

eg.

```php
$output = $page->return_page;
```

`replace_permission_tags()` - Replaces any `{check_XXX)` permission template tags on the page being rendered.

`replace_urls()` -Replaces any {url_XXX) template tags on the page being rendered.

`replace_meta_template_tags()`

`replace_css_template_tags($admin = FALSE, $tempate_section='')`

> `$admin` - BOOLEAN - If set to TRUE applies to public templates, if FALSE (default) applies to Administration area templates.  
> `$template_section` - TEXT - Optional  template section to parse, if empty, processes the entire page. Otherwise must contain be a string of raw HTML code that contains `{load_css_XXX)` template tags for replacement.

`replace_lang_template_tags($admin = FALSE, $tempate_section='')`

> `$admin` - BOOLEAN - If set to TRUE applies to public templates, if FALSE (default) applies to Administration area templates.  
> `$template_section` - TEXT - Optional  template section to parse, if empty, processes the entire page. Otherwise must contain be a string of raw HTML code that contains language template tags for replacement.

`cleanup_template_sections($next_prev='', $next_prev_bottom='')` - Used to replace the contents of `{next_prev}` and `{next_prev_bottom}` pagination template tags with user-provided HTML markup. If either `$next_prev` or `$next_prev_bottom` are empty (default) the tags will simply be removed from the template.

`replace_search_field_tags($tempate_section = '')`

`$tempate_section` - TEXT - Optional template section to parse, if empty, processes the entire page. Otherwise must contain be a string of raw HTML code that contains search_page template tags for replacement.

`replace_listing_field_tags($listing_id, $tempate_section = '', $utf8HTML = FALSE, $skipImageTags=FALSE)`

> `$listing_id` - INT - The numeric listingdb_id of the listing  
> `$tempate_section` - TEXT - Optional template section to parse, if empty, processes the entire page. Otherwise must contain be a string of raw HTML code that contains listing_detail page template tags for replacement.  
> `$utf8HTML` - BOOLEAN - UTF8 encode output. "MBString is enabled at the server: must be set in Site Config. Defaults to FALSE  
> `$skipImageTags` - BOOLEAN - Always set to FALSE.

`replace_user_field_tags($user_id, $template_section='', $tag_prefix='member')`

> `$user_id` - INT - The numeric userdb_id of the user  
> `$tempate_section` - TEXT - Optional template section to parse, if empty, processes the entire page. Otherwise must contain be a string of raw HTML code that contains view_user_default page template tags for replacement.  
> `$tag_prefix` - TEXT - agent or member determines whether to parse member_XXX_XXX template tags or agent_XXX_XXX template tags.Defaults to 'member' if not passed.

### File: login.inc.php - Contains authentication verification functions

Class: login

Function:

`loginCheck($loginType,TRUE)`  - Used to test if the current user is
logged in as a particular user type, Agent, Member, or Admin

example:

```php
<?php

global $config;

require_once($config['basepath'] . '/include/login.inc.php');

$login = new login();

//Takes two parameters.

//The first is the permissions to check: Admin, Member, Agent

//The second parameter should always be TRUE.

$security = $login-&gt;loginCheck('Admin', true);

if($security !== true){

//Run this code if this is NOT an admin user.

}

else{

//Run this code if this is an admin user.

}

?>
```
