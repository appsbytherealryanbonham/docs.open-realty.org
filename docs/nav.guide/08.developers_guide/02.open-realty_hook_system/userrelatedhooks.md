# User Related Hooks

after\_user\_signup($userID) - Hook function called after a successful
registration

  

after\_user\_change($userID) - Hook function called after a user
changes/updates their profile.

  

after\_user\_delete($userID) - Hook function called after a user is
deleted.

  

before\_user\_change ($userID) - Hook function called after a user
change is verified and before it is actually executed. This allows you
to get the current users email address, etc before it is changed.

  

$userID (INT) - The numeric OR user ID\# of the user.


