# Login Related Hooks

after\_user\_login($userID)- This hook function is called after a user
logs in.

  

after\_user\_logout($userID) - This hook function is called after a user
logs out.

  

$userID (int) - the numeric OR user ID\# of the user logging in or out.

  

example:

&lt;?php

function after\_user\_login($userID){

// adds a log entry into the OR activity log when a user logs in.

global $conn, $config, $misc;

  $sql = "SELECT userdb\_user\_first\_name, userdb\_user\_last\_name

          FROM ".$config\['table\_prefix'\]."userdb

          WHERE userdb\_id = '".$userID."'";

  $recordSet = $conn-&gt;Execute($sql);

  $fname = $recordSet-&gt;fields\['userdb\_user\_first\_name'\];

  $lname = $recordSet-&gt;fields\['userdb\_user\_last\_name'\];

  $misc-&gt;log\_action('User ID\#'.$userID. ' '. $fname .' '.  $lname
.' Logged in');

return null;

}        

?&gt;


