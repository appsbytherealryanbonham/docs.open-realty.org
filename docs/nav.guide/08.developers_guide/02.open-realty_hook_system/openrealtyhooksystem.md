# Open-Realty Hook System

The hook system allows you to trigger your own custom code after certain
events occur in OR.

  

You can create multiple hook files within a single OR install. Each hook
file should be named  'X\_hook.php', for example: sample\_hook.php  

  

Within the hook file there must be a class named 'X\_hook.php'  named
identically as your hook file, as in the case of our example
sample\_hook.php file below that is using a Listing related hook.

  

sample\_hook.php

&lt;?php

class sample\_hook {

       function after\_new\_listing($listingID) {

               //your code goes here

       }

}

?&gt;

  

Each hook file can contain any of the following group of functions, each
is explained in the following pages..

  

-   Lead system related Hooks
-   Listing related Hooks
-   Login/Authentication related Hooks
-   Media related Hooks
-   User account related Hooks

  

Open-Realty also has a few special hooks, that may only exist as a
single occurrence between your hook files.

  

-   Single Use Hooks

  

  

