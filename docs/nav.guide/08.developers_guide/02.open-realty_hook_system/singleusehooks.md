# Single Use Hooks

replace\_meta\_template\_tags($action) - Used to Replace the Meta
Description, Keywords, and/or HTML page &lt;title&gt; for a page.

  

$action (string) - the value of the current page's 'action' $\_GET
variable, if any.  e.g.: action=searchresults

  

This function must return an array with one or more of the following
keys set:

\['keywords'\]

\['description'\]

\['title'\]

  

example for replacing the title and keywords on a specific page (search
results):

&lt;?php

function replace\_meta\_template\_tags($action) {

if ($action =='searchresults'){

return array('title'=&gt;'This is my custom Search Results page Title',
'keywords' =&gt;'custom keywords rock');        

  }

else {

return array('title'=&gt;'This is a non targeted custom page Title',
'keywords' =&gt;'non targeted custom keywords rock');

  }

}        

?&gt;

  

------------------------------------------------------------------------

  

detect\_mobile\_browser($user\_agent) - Hook function used to override
internal mobile browser detection

This function must return an array with the key 'is\_mobile' if you want
to override the built in detection. To fall back to the built in
detection return NULL.

  

$user\_agent (string) - The User Agent being reported by the browser.

  

example:

&lt;?php

function detect\_mobile\_browser($user\_agent) {

//check user agent, If this is a Motorola Xoom, force it to use the
mobile template

if(stripos($user\_agent,'xoom build') !== FALSE){

return array('is\_mobile'=&gt;TRUE);

  }

//If not the xoom then fall back to internal detection.

return NULL;

}        

?&gt;

