# Media related Hooks

(added in v3.2.10)

after\_new\_media($mediainfo) - This hook function is called after a new
media item (photo, file, vtour) is uploaded/created for a user or
listing.

  

before\_media\_delete($mediainfo) - This hook function is called before
a media item is deleted.

  

after\_media\_delete($mediainfo) - This hook function is called after a
media item is deleted.

  

$mediainfo is an array, and contains 3 keys:

  

$mediainfo\['file\_name'\] - (TEXT) - The name of the media file.

$mediainfo\['media\_type'\] - (TEXT) - type of media file. One of 5
possible types:

listingsimages

userimages

listingsfiles

usersfiles

listingsvtours

  

$mediainfo\['media\_parent\_id'\]-  (INT) - Numeric listing ID\# or user
ID\# (whichever is applicable)  of the listing or user the media belongs
to.

  

Note: linked (remote) listing photos will contain http:// or https://
full URLs as their file\_name. Locally stored photos will return their
file name only with no path information. Do not echo or print any
results from your script except for debugging purposes, that will
otherwise break the expected json return codes in the Media widget
making it appear broken. Send any absolutely necessary output to the OR
activity log instead. Return NULL in your function when completed.

  

example:

... other php code ...

return NULL;

}


