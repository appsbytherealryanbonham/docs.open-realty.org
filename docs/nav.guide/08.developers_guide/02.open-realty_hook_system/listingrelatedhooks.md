# Listing Related Hooks

after\_new\_listing($listingID) - Hook function called after a listing
is created. (Listing may not yet be active, so you may want to use the
after\_actived\_listing hook instead if you only want to deal with
active listings.

  

after\_listing\_change($listingID) - Hook function called after a
listing is modified.

  

after\_listing\_delete($listingID) - Hook function called after a
listing is deleted.

  

before\_listing\_delete($listingID) - Hook function called before a
listing is deleted.

  

after\_actived\_listing($listingID) - Hook function called after a
listing is marked as active.

  

$listingID (int) - The numeric OR listing ID\# of the listing.

  

  


