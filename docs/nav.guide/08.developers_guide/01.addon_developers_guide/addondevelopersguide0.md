# Add-on Developer's guide

Add-on System Specifications Version 3 for Open-Realty®

  

Description: The purpose of this specification is to make a module
system that auto-loads any add-ons into Open-Realty.

  

Add-ons are very powerful modules for Open-Realty that will allow most
modifications, customizations and additions to be put into an add-on
format instead of having to edit the core program code of Open-Realty.
This makes upgrading to future versions of OR much easier for the site
developers and prevents users from having an unsupported or broken
version of OR due to core code modifications.

  

All add-ons MUST contain an addon.inc.php file. - This file is the heart
of the add-on system. It is what Open-Realty will look for initially to
setup and integrate the add-on.


