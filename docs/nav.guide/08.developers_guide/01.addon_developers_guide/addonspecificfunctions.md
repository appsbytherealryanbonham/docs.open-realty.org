# Add-on specific Functions

Add-on specific functions are functions that are created by you specifically for the add-on's use. An example of this would be if a user wanted to create an add-on for a customized Featured Listing layout then the user can write their own Featured listing display function and have it called by the template tag `{addon_superniftyfeaturedlisting_display}` Using all of the above examples of properly formatted add-on functions you would be calling the following Add-on Specific Functions:

```php
<?php

// Addon Specific Function
function name_display_addon_link() {
    $display = '<a href="index.php?action=addon_name_showpage1">Name Test</a>';
    return $display;
}


// Addon Specific Function
function name_display_addon_page(){
    $display = 'This is an add-on page';
    return $display;
}


// Addon Specific Function
function name_display_admin_page() {
    $display = 'This is an ADMIN add-on page';
    return $display;
}

?>
```
