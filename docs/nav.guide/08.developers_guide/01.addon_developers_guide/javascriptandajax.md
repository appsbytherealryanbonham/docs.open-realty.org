# JavaScript and Ajax

### JavaScript

Add-ons may insert JavaScript for loading via the HTML `<head>` of your templates using the following global variables. Make sure to include valid `<script></script>` tags around any inserted JavaScript. These variables are:

1. `$jscript` - JavaScript added to this global variable will be loaded into the main template via the `{load_js}` template tag.

2. `$jscript_last` - JavaScript added to this global variable will be loaded into the main template via the `{load_js_last}` template tag.

You must concatenate any Javascript additions to the end of the existing variable content using:

```php
<?php
    global $jscript;
    $jscript .= '<script type="text/javascript">your JS code</script>';
?>
```

using `.=` instead of `=` appends (concatenates) your custom Javascript code
at the end of the existing OR JavaScript already stored in the global
$jscript variable so the existing content is not overwritten with your
add-on's JavaScript code. You MUST declare the $jscript variable as a
global variable in your function(s) before appending your custom
JavaScript code.

Use these variables in any add-on specific function(s) that require
jQuery or JavaScript.

### Ajax

Add-ons can make use of the "ajax.php" file and its functions included in Open-Realty's administration area for functionality or processes that do not need to involve the template engine.

The add-on will need to have a function named:

`name_run_action_ajax()`

The name_prefix should be changed to match the add-on and add-on folder name in the same manner as other add-on functions documented elsewhere.

Add-ons can also make use of the "ajax.php" file and its functions included in Open-Realty specifically for the site visitor (public) area of the web site for functionality or processes that do not need to involve the template engine.

The add-on will need to have a function named:

`name_run_user_action_ajax()`

The name_prefix should be changed to match the add-on and add-on folder name in the same manner as other add-on functions documented elsewhere.
