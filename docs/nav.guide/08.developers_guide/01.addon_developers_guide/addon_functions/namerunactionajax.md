# name\_run\_action\_ajax()

name\_run\_action\_ajax()

  

This function handles the add-on's $\_GET actions for calls to the
ajax.php file from the back end (administration area) of Open-Realty.
Each $\_GET action should have the function to be called defined. The
action must be named using this method: addon\_name\_description.

  

An example of valid code for this function:

&lt;?php

function name\_run\_action\_ajax() {

       switch ($\_GET\['action'\]) {

               case 'addon\_name\_showpage1':

                       $data = name\_display\_addon\_page();

               break;

               default:

                       $data = '';

               break;

       }

return $data;

}

?&gt;

