# name\_uninstall\_tables()

name\_uninstall\_tables()

  

This function runs the SQL commands to delete any tables or values that
were added during the installation of the add-on. The return string
should return TRUE to indicate the add-on's tables were removed
properly.

  

An example of a valid function:

&lt;?php

function name\_uninstall\_tables(){

       global $conn, $config;

       require\_once($config\['basepath'\] . '/include/misc.inc.php');

    $misc = new Misc();

    $sql\_uninstall\[\] = "DELETE FROM  " .
$config\['table\_prefix\_no\_lang'\] . "addons

                        WHERE addons\_name ='framework'";

       foreach($sql\_uninstall as $elementContents) {

        $recordSet = $conn-&gt;Execute($elementContents);

               if ($recordSet === false) {

                       echo "&lt;strong&gt;&lt;span
style=\\"red\\"&gt;ERROR -
$elementContents&lt;/span&gt;&lt;/strong&gt;&lt;br/&gt;";

                       return FALSE;

               }

               else {

                       return TRUE;

               }

       }

}

?&gt;


