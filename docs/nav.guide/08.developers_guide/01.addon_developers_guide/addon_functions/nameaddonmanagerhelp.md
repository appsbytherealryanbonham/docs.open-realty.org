# name_addonmanager_help()

name_addonmanager_help()

This function provides the information that is displayed in the Add-on
Manager help link. There are arrays for Template Tags and Action URLs.
The is a variable for a link to the Add-on's documentation URL.

An example of a valid function:

&lt;?php

function name_addonmanager_help() {

\$template_tags=array();

\$action_urls=array();

\$doc_url='http://yoursiste.com/docs';

\$template_tags\['addon_framework_link'\]='This tag places a
link to the Framework Admin.';

\$action_urls\['addon_framework_admin'\]='Displays the
Framework Admin page.';

return array($template\_tags,$action_urls,\$doc_url);

}

?&gt;
