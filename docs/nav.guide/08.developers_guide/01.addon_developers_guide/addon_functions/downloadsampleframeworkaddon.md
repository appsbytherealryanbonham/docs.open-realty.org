# Download Sample Framework add-on

All of the previous add-on functions have been rolled into a sample
add-on called "Framework". You can download the sample Framework Add-on
to use as an example or starting point for building your own add-ons.

  

Download the sample Framework add-on to use as an example of a correctly
formatted add-on. The Framework add-on can be located on the Open-Realty
extras web site at the following link:

  

http://www.open-realty.org/extras/framework.html

  


