# name\_install\_addon()

name\_install\_addon()

  

This function is used to add new database tables or to modify existing
OR tables and data when necessary. This should be used at a minimum to
store the version number of your add-on in the add-ons table and compare
the add-on version to the previously installed version (if any) and
determine if an installation or an update is needed and then carry out
any required database modifications.

  

An example of a valid function:

function name\_install\_addon(){

    $current\_version = "1";

       global $conn, $config;

       require\_once($config\['basepath'\].'/include/misc.inc.php');

    $misc = new Misc();

       //Check Current Installed Version

    $sql = 'SELECT addons\_version FROM
'.$config\['table\_prefix\_no\_lang'\].'addons

            WHERE addons\_name = \\'name\\'';

    $recordSet = $conn-&gt;Execute($sql);

    $version = $recordSet-&gt;fields\[0\];

       if ($version == ){

               // Preform a new install. Create any database tables,
insert version \# into addons table.

        $sql = 'INSERT INTO
'.$config\['table\_prefix\_no\_lang'\].'addons

               (addons\_version, addons\_name)

                VALUES (\\'1\\',\\'name\\')';

        $recordSet = $conn-&gt;Execute($sql);

               return TRUE;

    }

       elseif($version != $current\_version) {

               //Preform Updates to database based on previous installed
version.

               switch($version){

                       case '0';

                       break;

        } // switch

               return TRUE;

    }

       return FALSE;

}

?&gt;


