# name\_load\_template()

name\_load\_template()

  

This should return an array with all the template tags for Open-Realty's
template engine to parse. If you don't define your template tags for the
add-on here they won't be parsed by the template system and properly
displayed. The actual replacement of the tags that are defined here is
done by the function name\_run\_template\_user\_fields.

  

An example of a valid function:

&lt;?php

function name\_load\_template() {

       $template\_array = array('addon\_name\_link');

       return $template\_array;

}

?&gt;


