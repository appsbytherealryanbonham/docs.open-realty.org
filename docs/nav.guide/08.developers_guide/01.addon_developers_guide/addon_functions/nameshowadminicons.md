# name\_show\_admin\_icons()

name\_show\_admin\_icons()

  

This function should return an array of the html links or text that
should be displayed in the OR administration area. This can be as simple
as a text name for your add-on to show users it is simply installed, or
a link to one or more administrative or configuration pages for your
add-on. You can display a link using this example:

  

index.php?action=name\_addonname\_admin

  

You will need to define the action $\_GET variable
"name\_addonname\_admin" in the name\_run\_action\_admin\_template()
function to define it as an action variable for use from the OR
administration area.

  

An example of a valid function:

&lt;?php

function name\_show\_admin\_icons() {

       $admin\_link = '&lt;a
href="index.php?action=addon\_name\_admin"&gt;name&lt;/a&gt;';

       return $admin\_link;

}

?&gt;


