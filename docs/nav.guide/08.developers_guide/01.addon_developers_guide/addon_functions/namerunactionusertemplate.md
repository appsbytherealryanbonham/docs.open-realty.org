# name\_run\_action\_user\_template()

name\_run\_action\_user\_template()

  

This function handles the add-on's $\_GET actions for the USER (public)
pages. Each get action should have the function to be called defined.
The Function must be named using this method: addon\_name\_description.

  

An example of a valid function:

&lt;?php

function name\_run\_action\_user\_template() {

       switch ($\_GET\['action'\]) {

               case 'addon\_name\_showpage1':

                       $data = name\_display\_addon\_page();

               break;

               default:

                       $data = '';

               break;

       }

return $data;

}

?&gt;


