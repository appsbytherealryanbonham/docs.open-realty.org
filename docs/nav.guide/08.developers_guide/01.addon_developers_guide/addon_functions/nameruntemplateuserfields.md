# name\_run\_template\_user\_fields()

name\_run\_template\_user\_fields()

  

This function handles all the replacement of
{addon\_name\_template\_tags} with the actual content. The tag
replacement can call any function that already exists in Open-Realty or
you can create your own add-on specific functions. All tags setup here
must also be added to the name\_load\_template function's array in order
for Open-Realty to parse them properly.

  

An example of a valid function:

&lt;?php

function name\_run\_template\_user\_fields($tag = ) {

       switch ($tag) {

               case 'addon\_name\_link':

                       $data = name\_display\_addon\_link();

               break;

               default:

                       $data = '';

               break;

       }

return $data;

}

?&gt;


