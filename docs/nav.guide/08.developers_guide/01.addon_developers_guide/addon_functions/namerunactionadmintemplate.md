# name\_run\_action\_admin\_template()

name\_run\_action\_admin\_template()

  

This function handles the add-on's $\_GET actions for the ADMIN area
pages. Each get action should have the function to be called defined.
The Function must be named using this method: addon\_name\_description.

  

An example of a valid function:

&lt;?php

function name\_run\_action\_admin\_template() {

       switch ($\_GET\['action'\]) {

               case 'addon\_name\_admin':

                       $data = name\_display\_admin\_page();

               break;

               default:

                       $data = '';

               break;

       }

return $data;

}

?&gt;


