# Add-On Packaging Requirements

The Open-Realty Add-On Manager can automatically install OR add-ons that are compliant with these add-on specifications. Add-ons must be packaged properly in order for the Add-On Manager to be able to handle installations and upgrades properly.

Add-ons must be compressed in a single ZIP file that is named using the add-on's name exactly. The sample [Framework add-on](https://gitlab.com/appsbytherealryanbonham/framework) would be named, for
example: "framework.zip"

The ZIP file should contain the add-on folder that is the same name of the add-on, and that folder must contain the "addon.inc.php" file.

### Example .zip file contents for 'widget' add-on:

/widget/addon.inc.php
