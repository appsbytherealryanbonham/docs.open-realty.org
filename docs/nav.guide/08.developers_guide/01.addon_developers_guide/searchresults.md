# Search Results

If creating an add-on that will be used on the search results page, and you would like to obtain the listing_id \#s for the listings in the search results you can use one of these bits of code:

### To return an array of all the listing ids for the current search results.

```php
    $matched_listing_ids = search_page->search_results(true);
```

### To return an array of the listing ids for the current page of the search results.

```php
    $matched_listing_ids = search_page->search_results('perpage');
```
