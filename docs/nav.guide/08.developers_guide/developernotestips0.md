# Developer Notes/Tips

While developing with Open-Realty you may find the frequent need to reinstall or update your software. Removing the /install folder and replacing it every time can become very tiresome after doing it a few dozen times. You can disable OR's warnings about your /install folder and files being present by creating a blank file in the root folder of your installation named "devignoreinstall" and Open-Realty will no longer perform the install folder check.

---

When attempting to adopt 3rd party template frameworks into OR templates that are heavy in javascript and/or CSS (Bootstrap, Foundation, etc.) you may wish to eliminate all of the default Javascript and CSS ( `{load_js}` and `{load_css_style_default}` ) that is ordinarily included in the `<head></head>` of one of the example OR templates in favor of  what is included with the framework you are using. This will eliminate any potential jQuery version or CSS class conflict headaches and will help the process move forward faster, and you can add back any individual CSS classes or IDs to your framework's CSS or a custom CSS file as needed or if necessary along with any desired individual javascript or jQuery functions.

---

When designing and styling templates, you can insure that your design elements and links won't break if the site URL is changed, the site is moved to/from a folder, or when copying templates between a development server and a live site by using absolute URLs in your templates via the `{baseurl}` and `{template_url}` template tags. Additionally, any CSS you include using OR's `{load_css_XXX}` tag will inherit the ability to use these URL tags.

Template example -  link to a photo in an images sub folder of your custom template folder

```html
<imgsrc="{template_url}/images/myphoto.jpg" />
```

Template example -  load a javascript library from a lib sub folder of your custom template folder

```html
<script type="text/javascript" src="{template_url}/lib/superfish.js"></script>
```

Template example -  link to a file/page in the root of your OR install

```html
<a href="{baseurl}/somefile.html">A link to Some File</a>
```

You can use these tags in your template's CSS files too. In your custom template's main.html file, load your css file in the `<head>` as follows and it will inherit the ability to use URL template tags

    `{load_css_yourcssfile}` (omit .css from the file name)

The tag above will load a CSS file named 'yourcssfile.css' from the currently selected site template folder in Site Config as if you had hard coded the following into your template.

    <link href="http://yoursite.com/template/TEMPLATE\_NAME/yourcssfile.css"rel="stylesheet" type="text/css">

By loading your CSS files this way, in addition to never having to update the URL to load them again, you gain the benefit of being able
to apply the same  URL tags within your CSS file(s).

```css
.myclass {
background: url('{template_url}/images/ui-bg_glass_65_ffffff_1x400.png')
} Using
```

this method your templates become truly portable as all your links and design
elements will always get the correct URLs from OR, and you never have to go back
to edit links in your template files if you move your template or copy it  to a
different site, and you can virtually eliminate having customers edit and update
links in any templates you sell.

---

SEF links are desirable for a live site but can interfere with your ability to identify `$_GET` variable names and values in the browser's address bar when performing development work. Turn off SEF links in Site Config and you will be able to identify any available `$_GET` variables for the pages you are viewing.
