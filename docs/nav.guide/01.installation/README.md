# Overview

Installing Open-Realty is not difficult, but if you have never installed a PHP application or created a MySQL database before, or worked with extracting .ZIP files and using FTP software, it would probably be advisable to obtain the assistance of someone who has some experience in those areas.

It is advisable to make sure the web hosting you are planning to use is suitable for operating Open-Realty by checking its [server requirements](./server_requirement.md) beforehand, doing so can save a lot of time debugging problems caused due to inadequate or incompatible hosting. 

The Open-Realty [installation process](03.new_install.md) essentially involves: 

1. Extracting and uploading the program files to your web hosting account root, or a sub-folder of your choosing.
2. Creating a new empty database and a new database user for Open-Realty to use.
3. Running the Open-Realty installer from your web browser.
4. Logging-in for the first time using the [default 'admin' username and password](04.default_login.md) to insert your license key and set other configuration options. 
5. Removing or renaming the installer folder when finished.
