# General

General Info

<img src="/assets/57b8803530ac11b59da4514e977ee31cb5ec505b.jpg" class="libimg" width="861" height="338" />

  
Company Name - This is your company name, it can be used on any page by
referencing the {company\_name} template tag.

  

Company Location - This is your company location, it can be used on any
page by referencing the {company\_location} template tag. 

  

Company Logo - This is the name of the company/site logo on your web
server, it can be used on any page by referencing the {company\_logo}
template tag.  This logo file should be located in the /images/ folder
beneath your currently selected template folder, e.g.
/template/SELECTEDTEMPLATE/images/

  

Example:

&lt;img src="{template\_url}/images/{company\_logo}" alt="" /&gt;

  

Automatic Update Check - Open-Realty will automatically check for
available updates to your installed version. By default this is turned
off to keep the control panel as fast as possible. Turning it on will
make Open-Realty® check our distribution server for available updates
and this may slow the control panel down slightly. 

  

Demo Mode- This will disable users from editing their passwords or any
of the default Open-Realty® pages and the Site Configuration.

  

Maintenance Mode - This will enable maintenance mode which will display
the maintenance mode page to all users except the "admin" user. The
maintenance mode page layout and content is controlled by the
"maintenance\_mode.html" template file in the site template folder. The
site admin will see the site as normal so they can test any changes they
are making before making them live for site visitors. 

  

Local Timezone - This should be set to your local timezone and will
adjust time entries for your local timezone.

  

Character Set - This is character set that will be used for the text
encoding on the site. WARNING: Changing the Character Set after you have
your site running with live data can cause unexpected results displaying
any existing content that has been already saved. Special characters
entered under one character set may not display properly under a
different one.

  

Remote API Key -

------------------------------------------------------------------------

  

Mail Settings

<img src="/assets/87aeda811feffeaec57fa7b8977c5bd767eab04f.jpg" class="libimg" width="859" height="178" />

  

Mail System - The default setting will use PHP's
[mail()](http://php.net/manual/en/function.mail.php)
function to send email. Mail can optionally be sent via
[phpmailer](https://github.com/PHPMailer/PHPMailer)

(It should be noted that using PHP's mail() function is not always the
best choice, many mail exchangers will treat email sent in this fashion
as spam).

  

Admin Name - This should be the name you want to appear on any emails
sent from the site. 

  

Admin Email - This is the email address that all messages will be sent
from. 

  

Site Email Address - Optional email address to use as the sender for
site emails. If left blank, the admin email address will be used.

  

------------------------------------------------------------------------

  

Server Paths

<img src="/assets/8acd1d53b5e97f8117dd5e3c1969f846ade9939c.jpg" class="libimg" width="860" height="123" />

  

Open-Realty obtains these values automatically when the installer is
run, you cannot change them unless you re-run the installer.

  

Base URL - The Web site URL for Open-Realty. If you installed OR in a
folder instead of the root, your URL might look like
http://www/yourdomain.com/open-realty

  

Base Path - The physical disk location on the web server where
Open-Realty is installed.

  

------------------------------------------------------------------------

  

WYSIWYG Editor Settings

<img src="/assets/c688ad6ada1f97725dce02bcc7dabe50a517d857.jpg" class="libimg" width="858" height="200" />

  

Show Edit link on frontend- Optionally displays a link on any Blog page
or Page editor page that will allow you to edit the contents of that
page. This link will only display to the site Admin, or Agents with
permission to edit the pages.

  

Execute PHP code - If this is turned on, PHP code entered into the Blog
editor or Page editor pages will be executed when that frontend page is
rendered. Turning this on is a BAD IDEA unless you trust everyone that
has Blog and/or Page editor privileges. USE AT YOUR OWN RISK!. When
entering the PHP command, you must  enter it in the source code view of
the editor (and not WYSIWYG mode) and surround it in html comments.

  

For example, if you are trying to enter the regular PHP code:

&lt;?php echo 'text'; ?&gt;

  

You need to enter it into the editor as:

&lt;!--&lt;?php echo 'text'; ?&gt;--&gt;

Note:  there must be no white space (spaces) between the html comment
marks and the PHP opening/closing tags.

  

MBString is enabled at the server?- By default this is set to 'No'.
Check with your web host's support to see if PHP MBString (MultiByte) is
enabled. If this is available on your server set this to "Yes". PHP
MBString support is required if you are going to store special
characters such as non-english characters in the Database (using the
Listing/Page/Blog Editors with or without a the WYSIWYG Editor).

  

------------------------------------------------------------------------

  

HTML Settings

<img src="/assets/e475bb831210d5b8c54cf5efdf30a788e54e904b.jpg" class="libimg" width="861" height="118" />

  

Add Linefeeds - Should Open-Realty convert return into a line feed?

  

Strip HTML - Should any HTML code entered into the listing editor
manually be stripped or removed before saving?

  

Allowed HTML - If you are stripping out  HTML which HTML tags do you
wish to allow?

  

------------------------------------------------------------------------

  

Security Settings

<img src="/assets/d97429fb59ed7391f0f4af955f3a0e7e6753fd70.jpg" class="libimg" width="857" height="137" />

  

Allow template switching - Allow the template to be changed by passing
the 'select\_users\_template'  $GET or $POST variable.

Example use:

http://www.yoursite.com/index.php?select\_users\_template=TEMPLATENAME

  

where TEMPLATENAME is the name of the template folder beneath /template/
you want to switch to use, such as 'lazuli'.

  

Allow language switching - Allow the site language to be changed by
passing 'select\_users\_lang'. This changes the lang files that OR uses.

Example use:

http://www.yoursite.com/index.php?select\_users\_lang=LANG

  

where LANG is the name of the language folder beneath
/include/class/language/ you want to switch to use, such as 'en' for
English or 'es' for Spanish.

  

Captcha System - You can select which CAPTCHA image verification system
to use for contact and signup forms. Presently supports Securimage or
re-Captcha




