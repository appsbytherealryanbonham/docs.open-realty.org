# Search Engine Optimization

<img src="/assets/315c8924b4f858d918cd7365e18e51f4ae5dee88.jpg" class="libimg" width="894" height="423" />

  

URL Type - This is used to select standard or search engine friendly
URLs. This is used in conjunction with the .htaccess file that's
included in the root directory of the OR .ZIP file. Your server must
allow URL Rewriting using APACHE's mod\_rewrite for the friendly URLs to
operate.

  

Space Character - If SEF URLs are turned on this is the character that
will replace spaces. By default it will replace spaces with a +
character. Using a hyphen - is argued to be more search engine friendly.

  

Default Page - This is the main page to display on your website. Either
WYSIWYG Page 1 or the Blog Index.

  

Default Page Title - This is the default page title for your site. This
will be the title on your home page and all pages other than the listing
detail pages unless it is a blog or page editor created page and you
specify a custom title for that page in the editor.

  

Default META Keywords - These are the default keywords that Open-Realty®
will place into the META keywords tag for your site. These will be used
for all pages other than the listing detail pages unless it is a page
editor created page and you specify a custom META Keywords for that page
in the page editor.

  

Default META Description - This is the default description that
Open-Realty® will place into the META description tag on your site. This
will be used for all pages other than the listing detail pages unless it
is a page editor created page and you specify a custom META Description
for that page in the page editor.

  

Listing Page Title - This is the title for your listing detail pages.
You can use normal text here or place listing data into the title by
using the SEO Listing Template Tags (See the Listing Template Tags
section of the documentation). You can use a combination of text and
Template tags in this field.

  

Listing META Keywords - These are the keywords for the META keywords tag
for your listing detail pages. You can use normal text here or place
listing data into the keywords by using the SEO Listing Template Tags
(See the Listing Template Tags section of the documentation). You can
use a combination of text and Template tags in this field.

  

Listing META Description - This is the description for the META
description tag for your listing detail pages. You can use normal text
here or place listing data into the description by using the SEO Listing
Template Tags (See the Listing Template Tags section of the
documentation). You can use a combination of text and Template tags in
this field.

