# SEO Links

SEO Links

<img src="/assets/189ac829644ec750f23824aa5824b83265455529.jpg" class="libimg" width="834" height="528" />

  

Usage: SEO Links consist of two parts, the "Slug" and the "URI". For
example: For the Listing URL, if the Slug is set "listing/" and the URI
is set to be "/{listing\_seotitle}.html" and a specific listing's SEO
Title is "my-seo-listing-title" the public URL that will be used to
reach the listing detail page for that specific listing will be:

  

http://www.yourdomain.com/listing/my-seo-listing-title.html

  

Why is the URL broken up into two parts and what can you change?  

  

Slug: The Slug is an indicator flag to Open-Realty to tell it what kind
of content you want. While you can leave the Slug empty, it is not
recommended. If there is no Slug Open-Realty has to look at all of your
site's content and try and find what you are looking for which can
adversely affect your sites performance. The Slug can be any text you
want, however you can not use template tags in your slug  it must always
be static text.

  

URI: The URI is where creating creative page URLs can be achieved. The
URI portion of the SEO URL, allows you to use template tags to assist in
generating unique URLs. Below are the available template tags you may
use, and which tags are required.

  

Listing URL:

Required Tags - You must use one of the following tags.

-   {listing\_id}  - This is the numeric OR listing ID\#
-   {listing\_seotitle} - This is the listing's Seo Title

Option Tags - You can use any of the tags listed in the "Listing Field
Tag" section of the docs.

  

Listing Image URL:

Required Tags - You must use one of the following tags.

-   {image\_id}  - This is the numeric ID\# for the Listing Photo

  

WYSIWYG Page URL:

Required Tags - You must use one of the following tags.

-   {page\_id}  - This is your numeric ID\# for the WYSIWYG page
-   {page\_seotitle} - This is SEO Title for the WYSIWYG page

Option Tags -

-   {page\_title} - This is the Title of the WYSIWYG Page

  

Agent page URL:

Required Tags - You must use one of the following tags.

-   {agent\_id}  - This is the numeric ID\# for the Agent

Option Tags -

-   {agent\_fname} - This is the Agent's first name.
-   {agent\_lname} - This is the Agent's last name.

  

Contact Agent URL:

Required Tags - You must use one of the following tags.

-   {listing\_id}  - This is the numeric OR listing ID\# for the
    specific listing the Agent is being contacted about.

Option Tags -

-   {agent\_id} - This is the Agent's numeric ID\#.
-   {agent\_fname} - This is the Agent's first name.
-   {agent\_lname} - This is the Agent's last name.

  

Blog Article URL:

Required Tags - You must use one of the following tags.

-   {blog\_id}  - This is your numeric id for the blog article
-   {blog\_seoname} - This is SEO Name for the blog article

Option Tags -

-   {blog\_title} - This is the name of the blog article
-   {category\_path} - This is the category path.. Example, if your blog
    is in a category called "Widget-1" and that has a parent category of
    "Widgets" this will generate a path of "Widgets/Wdiget-1"

Blog Tag URL:

Required Tag

-   {tag\_seoname} - This is the SEO Name of the blog tag.

  

Blog Category URL:

Required Tags - You must use one of the following tags.

-   {cat\_id}  - The numeric ID\# for the Blog category
-   {cat\_seoname} - The SEO Name for the Blog category

Option Tags -

-   {cat\_parent\_seoname} - This is the Seo Name of the Blog category's
    parent.

  

Blog Archive URL:

Required Tag

-   {archive\_date}

  

------------------------------------------------------------------------

  

Static URLs

<img src="/assets/fa17ac4612857068b721ef9110c33bfa818f92d6.jpg" class="libimg" width="812" height="370" />

  

The above URLs  do not allow the use of template tags, as they are
simply links to some static features of OR. You can however customize
these URLs to be what ever you want using plain text.

  

NOTE: the Search Results URL: affects "view all listings" only, and will
not work with other search results.


