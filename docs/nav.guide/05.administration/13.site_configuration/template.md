# Template

Template settings

<img src="/assets/cd704b74c3d5fab81f82025ddfa2dbe6ec3f6620.jpg" class="libimg" width="896" height="388" />

  

Site Template - This is the Template that your OR site site will use.
The default example template is named "lazuli". Any template folders
created beneath the /template/ folder will appear in this pull-down
selection. To customize this or any other example template first make a
copy of the source template folder  such as:  /template/lazuli/ and copy
the contents to its own unique folder beneath /template/. For example:
/template/lazuli\_custom.

  

Mobile Template - This is the optional Template used for mobile devices,
and by default uses the template files located in the  /template/mobile
folder. Any template folders created beneath the /template folder will
appear in this pull-down selection. To customize this or any other
example template first make a copy of the source template folder  such
as:  /template/lmobile/ and copy the contents to its own unique folder
beneath /template/. For example: /template/mobile\_custom.

  

Administrative Template - This is the Template for the Administration
area of your OR site. The default example Administration template is
named "OR\_small". Any template folders created beneath the
/admin/template/ folder will appear in this pull-down selection. To
customize this or any other example administration template, first make
a copy of the source template folder  such as:
 /admin/template/OR\_small/ and copy the contents to its own unique
folder beneath /admin/template/. For example:
/admin/template/OR\_small\_custom.

  

Listing Template - This is the template used for the layout of the
listing display. These templates are placed in the currently selected
Site Template folder and are named 'listing\_detail\_TEMPLATENAME.html'
where 'TEMPLATENAME' is the name of any version of this template file.
By default this is set to the template file:
'listing\_detail\_default.html'

  

Listing Template Sections - This is where to define all template
sections for the listing template that you want users to be able to map
fields to. Be careful about removing items from this list if you have
fields mapped to that template section already

  

Search Result Template - This is the template used for the layout of the
search results. These templates are placed in your template directory
and are named "search\_result\_templatename.html"

  

View Agent Template - This is the template for the View Agent page.

  

VTour Template - This is the template that will be used for the VTour
pop-up windows.

  

Listing Notification Template - This is the template used to send
Members notification of new listings that match their saved searches.
The notifications are processed through a CRON job. See the "Automating
Tasks" section of the documentation for more details on setting up the
Listing Notification system.

  

  


