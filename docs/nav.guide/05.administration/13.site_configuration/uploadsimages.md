# Uploads/Images

Image Upload Settings

<img src="/assets/b82529c5a3cd586d8da50e292e118c51a33513de.jpg" class="libimg" width="755" height="153" />

  

Allowed Extensions - Allowed file extensions for uploads. Add additional
extensions here as required, some extensions may not be an allowed file
type so be sure to check the file type and update that as needed.

  

Image Tool - Default image processor is GD libs as it is almost always
installed and compiled into most Apache/PHP web servers. However, you
can also use ImageMagick in case  you don't have the GD Libs and your
server supports ImageMagick

  

ImageMagick Path - This is the path to your ImageMagick installation on
your server. This is only used if you have set the image tool to
ImageMagick, and your server supports it.

  

Create Thumbnails - Creates thumbnails of any uploaded images.

  

------------------------------------------------------------------------

  

Listing Image Upload Settings

<img src="/assets/03e0691b9d5eeb6c6c959ca986ae5479fb0fc687.jpg" class="libimg" width="755" height="373" />

  

Max \# of images per user - Maximum allowed number of images that can be
uploaded for a listing.

  

Max File Size - Maximum allowed file size in bytes for uploaded listing
images.

  

JPEG Quality - Used with GD libs only. This is the quality of JPEG
images created with GD Libs. A smaller number results in a smaller file
size and lower quality, a higher number results in a larger file size
and higher quality.

  

Resize Image - If set to "Yes",  uploaded images will be resized to
their Maximum Image Width for their image type as defined by Max File
Width

  

Resize By - What to use when resizing images:

       Width - Use the image width setting to resize the image

       Height - Use the image height setting to resize the image

       Best Fit - Uses both the width and height settings to determine
which setting to resize by in order to make the resized image fit within
both the maximum width and maximum height settings

       Both - Resizes by both the maximum width and height settings\*.

       \*THIS WILL LIKELY CAUSE DISTORTION UNLESS YOUR IMAGE AND THE
WIDTH AND HEIGHT SETTINGS ARE THE EXACT SAME ASPECT RATIO.

  

Max Image Width - Maximum image width for uploaded listing photos. If
"Resize Image" is set to "Yes" and any uploaded images are larger than
their "Maximum Image Width" setting, those images will be automatically
resized to their Max Image width.

  

Max Image Height - Maximum image height for uploaded listing photos. If
"Resize Image" is set to "Yes" and any uploaded images are larger than
their "Max Image Height" setting, those images will be automatically
resized to their Max Image height.

  

Display Thumbnails by - Which dimension, (width, height, both) should OR
display thumbnails? If create thumbnails is set to "Yes" this is the
dimension OR will use to create thumbnails.

  

Thumbnail width - The width in pixels that thumbnails will be created.

  

Thumbnail height - The height in pixels that thumbnails will be created.

  

------------------------------------------------------------------------

  

<img src="/assets/4c486377c8e6d70d337d88f6649542d5256eb2db.jpg" class="libimg" width="758" height="248" />

  

Main Image Display by - How to display the Main Image called with the
main image or main image java tags?

  

Main Image width - Main Image display width.

  

Main Image height - Main Image display height.

  

Number of Columns - The number of columns to use on the Java Listing
Images by Rows template Tag before starting a new row.

  

Slideshow \# of thumbnails per group - The number of thumbnails to
display per group on the listing slideshow

  

Use No Photo - If a listing does not have any photos, should Open-Realty
display the /images/nophoto.gif instead?

  

------------------------------------------------------------------------

  

<img src="/assets/7ce3b600d2346494b79af1779093b29c65235495.jpg" class="libimg" width="755" height="361" />

  

Max \# of images per user - Maximum allowed number of images that can be
uploaded for an Agent.

  

Max File Size - Maximum allowed file size in bytes for uploaded Agent
images.

  

JPEG Quality - Used with GD libs only. This is the quality of JPEG
images created with GD Libs. A smaller number results in a smaller file
size and lower quality, a higher number results in a larger file size
and higher quality.

  

Resize Image - If set to "Yes",  uploaded images will be resized to
their Maximum Image Width for their image type as defined by Max File
Width

  

Resize By - What to use when resizing images:

       Width - Use the image width setting to resize the image

       Height - Use the image height setting to resize the image

       Best Fit - Uses both the width and height settings to determine
which setting to resize by in order to make the resized image fit within
both the maximum width and maximum height settings

       Both - Resizes by both the maximum width and height settings\*.

       \*THIS WILL LIKELY CAUSE DISTORTION UNLESS YOUR IMAGE AND THE
WIDTH AND HEIGHT SETTINGS ARE THE EXACT SAME ASPECT RATIO.

  

Max Image Width - Maximum image width for uploaded Agent photos. If
"Resize Image" is set to "Yes" and any uploaded images are larger than
their "Maximum Image Width" setting, those images will be automatically
resized to their Max Image width.

  

Max Image Height - Maximum image height for uploaded Agent photos. If
"Resize Image" is set to "Yes" and any uploaded images are larger than
their "Max Image Height" setting, those images will be automatically
resized to their Max Image height.

  

Display Thumbnails by - Which dimension, (width, height, both) should OR
display thumbnails? If create thumbnails is set to "Yes" this is the
dimension OR will use to create thumbnails.

  

Thumbnail width - The width in pixels that thumbnails will be created.

  

Thumbnail height - The height in pixels that thumbnails will be created.

  

------------------------------------------------------------------------

  

<img src="/assets/6110816abdf851a2b59a57581b0a135f1f62221c.jpg" class="libimg" width="756" height="314" />

  

Agent Use No Photo - If an Agent does not have any photos, display the
default "nophoto" photo.

  

Allowed Extensions - Allowed file extensions

  

Max \# of files per listing - Max \# of files for a given listing

  

Max File Size - Max File Size in bytes for listing files.

  

Max \# of files per user - Max \# of files for a given user

  

Max File Size - Max File Size in bytes for user files.

  

------------------------------------------------------------------------

  

<img src="/assets/532a87b1704c2423f1a4d79f3862aacb9bc83541.jpg" class="libimg" width="757" height="264" />

  

Show the File Icon -  If this is set to yes, Open-Realty will display an
icon image from the /files/ folder where the image's name is the file
extension of the file. Open-Realty expects that there will be a matching
file for all allowed extensions.

  

File Display Option - Should Open-Realty display the file caption or the
file name as the link? If caption is selected and it is empty, this
defaults to filename

  

Show File Size- Should Open-Realty display the file size?

  

File Icon Width - Default icons included with Open-Realty are 16x16.
However, if you use a different icon set you may want to change these
sizes to the size of your new icons. All icons are expected to be the
same size.

  

File Icon Height- Default icons included with Open-Realty are 16x16.
However, if you use a different icon set you may want to change these
sizes to the size of your new icons. All icons are expected to be the
same size.

  

------------------------------------------------------------------------

  

<img src="/assets/b3d853b43cec9ecbf5df3f0acc68af127a60549f.jpg" class="libimg" width="756" height="291" />

  

Max \# of virtual tours per listing-Maximum number of virtual tours
allowed for a given listing.

  

Max File Size - Maximum allowed file size in bytes for uploaded VTour
files.

  

Max Image Width - Maximum image width for uploaded virtual tour files.

  

VTour Width - The width in pixels of the VTour display.

  

VTour Width - The width in pixels of the VTour display

  

VTour FOV - The Field Of View of the VTour display window. This setting
 controls the initial Zoom of the VTour. This setting is in degrees with
a Min of 12 and Max of 165.

  

Popup Width - Width in pixels of the VTour popup window

  

Popup Height - Height in pixels of the VTour popup window


