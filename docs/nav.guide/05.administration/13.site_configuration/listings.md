# Listings

Number Formatting

<img src="/assets/8646648669aea558d10a1d5425f9380acf97a3ed.jpg" class="libimg" width="856" height="326" />

  

International Number Format - Support for international numbering
formats, such as 1,000 or 1.00,00

  

Decimals for numbers - Number of decimals to show for number fields.

  

Decimals for prices - Number of decimals to show for price fields.

  

Force Display of Decimals - By default, if a number is a whole number we
will not show decimal places. If set to "yes", then we will force the
display of decimal places even on whole numbers.

  

Money Format - Defaults to $123, but others use different formats

  

Money Sign - Default is dollars ($), but it could be £(£) for pounds or
€(€) for euros

  

Date Format - Controls the display format of dates on the site.

  

Blank Price Text - Should Open-Realty replace blank price values with
"Call For Price" language variable? This variable can be changed in your
lang.inc.php file if you wish to use different text.

  

Price Field - Select the listing field that contains the listing price.

  

------------------------------------------------------------------------

  

Listing Settings

<img src="/assets/4cc4e2cf77450a4a4267cc24ca9d192c8db97cfa.jpg" class="libimg" width="807" height="482" />

  

Number of Featured Listings - How many Featured listings should be
displayed?

  

Number of Latest Listings - How many Latest listings should be
displayed?

  

Number of Popular Listings - How many Popular listings (listings with
most hits/pageviews) should be displayed?

  

Number of Random Listings - How many random listings should be
displayed?

  

Use Expiration - Should Open-Realty enforce expiration?

  

Expire After - How long should listings be active?

  

Moderate Listings - Should new listings require moderator approval in
order to be Active

  

Allow Listing Exports - If enabled agents will be given the option of
setting if a listing should be included in an MLS export or not. You
need a custom export script currently to use this.

  

Show Listedby Link For Admin's Listings - Should we show the listedby
link for listings owned by the site admin.

  

Show NextPrev Bar On Listing Page - Show the next\_prev bar on listing
pages, to allow users to move between listing without having to go back
to the search result page. Warning: this will cause a performance
degradation for sites with a lot of listings.

  

Show Notes on listing editor - Yes will show the notes field on the
listing editor. No will make the notes field hidden but still exist.

  

Feature list separator - Enter the html or text you want to use to
separate the items in the feature list.

  

Admin Listings Per page - Number of listings to display per page in the
listing editor.

  

------------------------------------------------------------------------

  

Search Options

<img src="/assets/f8505b5957ff89f2dda8d10aaff3bac519d5508a.jpg" class="libimg" width="857" height="376" />

  

Max Min/Max Steps - The max allowed steps in min/max searches. If your
step value will create more steps then this maximum, a new step value
will be calculated to stay within the max allowed steps to prevent
memory overflow problems. Setting this to 0 will disable this protection

  

As an example, assume you have a price field set to searchable with a
min/max search and have a step value of 50000 (50,000) entered. If you
have a listing in your database with no price, and one with a price of
1000000 (1,000,000) then the min/max search function will calculate and
generate a range of 20 values for both the min and max search boxes for
a total of 40 options. This doesn't take much time and won't be noticed.
However, if a agent makes a typo while entering a listing, or you
configure the step value too low things could get out of hand quickly.
For example, say an agent entered 100000000 (100,000,000) in that same
scenario. Open-Realty would now need to calculate and render 2,000
search options for both the min and max search boxes for a total of
4,000 options. This will likely cause a memory overflow on the server,
and if not, then would cause the end user to have to wait for 4,000
search options to be downloaded and rendered by their browser.

  

Having the Max Steps option set will first calculate how many step
values would be calculated in a given scenario and if it is more than
the max allowed, will override the settings with a more reasonable
amount.

  

Listings Per Page - Number of listings to show on each Search Result
Page.

  

Sort Searches By - The field you want your search results sorted by
default. Only fields that are set to be displayed on browse are
available for selection plus the option to sort randomly. This should be
a required field that exist in every class. Sorting by a field that does
not have a value or does not exist on a listing or property class can
result in Open-Realty not displaying the listing, as the SQL server will
not return it.

  

Sort Type - How you want your searches sorted: ASC will sort lowest to
highest, DESC will sort highest to lowest.

  

Static Sortby Option- Special Static Sortby option. If this is
configured search results will ALWAYS first be sorted by this sortby
before the default or user selected sortbys. This is useful primarily if
you want to sort Featured Listings first in your search results

  

Static Sortby's Sorttype - If you have a Static Sortby Option selected
this is the Sorttype for that Static Sortby option.

  

Show Count in Search Options - Would you like the number of listings
shown in the search options?

  

Maximum Search Results- Maximum number of search results any search
shall return. 0 = NO LIMIT

  

Search form checkbox list separator - Enter the html or text you want to
use to separate each of the checkbox options.

  

Short textarea characters - How many characters to display when using
the text area short template tag?

  

------------------------------------------------------------------------

  

Map Settings

<img src="/assets/b78bdfcd78c2f0efe45e2e81bb4e18e22f1ef89b.jpg" class="libimg" width="858" height="346" />

  

Use Map - What Map system would you like to use? US Google is the
default, Mapquest and Yahoo Maps are other choices.

  

Address Field - Name of the address field? This field will be the first
address field.

  

Address Field 2 - Second Address Field. This field will be appended to
the end of the first address field. (This is used when storing street
number/names/suffix/direction in separate fields)

  

Address Field 3 - Third Address Field. This field will be appended to
the end of the second address field. (This is used when storing street
number/names/suffix/direction in separate fields)

  

Address Field 4 - Fourth Address Field. This field will be appended to
the end of the third address field. (This is used when storing street
number/names/suffix/direction in separate fields)

  

City Field - Name of the city field?

  

State Field - Name of the State field?

  

Zip Field - Name of the Zip Code field?

  

Country Field - Your country field. (Only used if not using a country
specific mapping choice.)

  


