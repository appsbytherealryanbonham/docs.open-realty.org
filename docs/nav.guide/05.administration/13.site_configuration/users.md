# Users

Signup Settings

<img src="/assets/565f8df14610200c47feb8e65661015cdfa50d96.jpg" class="libimg" width="857" height="117" />

  

Use image verification on signup page- Adds an image verification code
(CAPTCHA) to the signup forms. Users are required to enter the code in
order to submit the form. Requires that the PHP server has the PHP GD
image libraries installed.

  

Require email address confirmation - Setting this to yes, will require
that the user confirm their email address by clicking a link sent to
them in the signup email.

  

------------------------------------------------------------------------

  

Member Permissions

<img src="/assets/1e53903bc8a0ac947bc79ec45f27f3048400b0bb.jpg" class="libimg" width="862" height="94" />

Moderate Members - Do new members require the admin to approve them
before they are active?

  

Member Signup - Are members allowed to signup? If you set this to 'Yes'
you will need to  to place a link to your member signup page somewhere
within  your web site. The link is:

http://www.yoursite.com/index.php?action=signup&type=member

  

or, you can use the following template tag within your site to place the
above signup link: {url\_member\_signup}

  

IMPORTANT: For security purposes and to prevent abuse of the
registration feature, the Member signup page must be accessed from a
link within your website. If you attempt to link to the signup page from
another website, email, or by directly typing the link address into your
web browser you will receive a "Not Authorized" error message.

  

------------------------------------------------------------------------

  

Agent Permissions

<img src="/assets/00d4417e50d265f41903b257e548a7490f5620e0.jpg" class="libimg" width="856" height="697" />

  

Moderate Agents - Do new agents require the admin to approve them before
they are active?

  

Agent Signup - Are agents allowed to signup? If you set this to 'Yes'
you will want to place a link to your agent signup page somewhere on
your site. The link is:

http://www.yoursite.com/index.php?action=signup&type=agent

  

or, you can use the following template tag within your site to place the
above signup link: {url\_agent\_signup}

  

IMPORTANT: To improve security, the Agent signup page must be accessed
from a link within your website. If you attempt to link to the signup
page from another website, email, or by directly typing the link address
into your browser you will receive a "Not Authorized" error message.

  

Active - Are new agents active by default?

  

Admin - Are new agents admins by default?

  

Can edit other users - By default can new agents modify other
agents/member profiles?

  

Can edit other agents listings - By default, can new agents modify other
agents listings?

  

Feature listings - Can new agents feature listings by default?

  

Moderate listings - Can new agents moderate listings by default?

  

View site log - Can new agents view log by default?

  

Can edit site config - By default, can new agents edit site config?

  

Can edit member template - By default, can new agents edit the member
template?

  

Can edit agent template - By default, can new agents edit the agent
template?

  

Can edit listing template - By default, can new agents edit the listing
template?

  

Export listings - Can new agents mark listings as exportable by default?
You must enable Allow Listing Exports in the Listing Section of the Site
Config to use this option.

  

Change listing expirations - Can new agents change listings expirations
by default?

  

Edit pages - Can new agents edit pages by default?

  

Virtual Tours - By default, Can new agents add virtual tours to
listings?

  

Files - By default, Can new agents add files to listings?

  

Number of Listings - This is the number of listings agents can create by
default. (-1 is unlimited).

  

Number of Featured Listings - This is the number of listings that an
agent can set to Featured. (-1 is unlimited).

  

------------------------------------------------------------------------

  

Agent Settings

<img src="/assets/af117e8f540887574a149cbf61d67ce8606bb55b.jpg" class="libimg" width="859" height="399" />

  

Agents per page - Number of agents to display per page on the view
agents page.

Show Admin on agent list - Should we display admins on the agent list.
Default is no. You will also want to enable the show listed by admin
option so that admins can be viewed from the listing details page.

  

Agent Vcard Settings

  

The Agent's first name, last name, and email address are automatically
retrieved from their user account. This information is sufficient to
create the vcard. The below fields can be used to include additional
information fields in the vcard. Use the Agent Field Editor to create
any necessary fields.

  

Phone - Name of the phone field?

Fax - Name of the fax field?

Mobile - Name of the mobile phone field?

Address - Name of the address field?

City - Name of the city field?

State - Name of the state field?

Zip - Name of the zip code field?

Country - Name of the country field?

Notes - Name of the notes field?

Homepage - Name of the homepage field?

  

------------------------------------------------------------------------

  

Ban Users

<img src="/assets/cfe688760862f6015a1815ac199fe48ce67edbc2.jpg" class="libimg" width="858" height="323" />

  

Banned Domains- Users that provide email addresses within a domain name
on this list, for example: 'aol.com', 'mail.com' or 'hotmail.com', will
not be allowed to sign up as Members or Agents. Place one domain name
per line that you wish to restrict.

  

Banned IPs - A site visitor that is using an IP placed on this list will
not be allowed to sign up as Members or Agents. Place one IP address
 per line that you wish to restrict.

  

Blocked IPs -  A site visitor that is using an IP placed on this list
will be blocked from viewing your site entirely. Place one IP address
 per line that you wish to restrict.
