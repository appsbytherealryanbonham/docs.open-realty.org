# Site Log

The Site Log maintains a list of all actions performed in Open-Realty in
the database for an accountability record. The site log uses a next/prev
menu at the top to skip between pages of the log.

  

The red 'X' is an option to Clear the Log. This option is only available
to site Admins. Clicking this will ask for a confirmation of the action
and then will clear the log, placing a new log entry notating that the
log was cleared, when it happened and who cleared the log.

  

-   ID - The ID of the logged action
-   Date - The date and time the logged action occurred
-   User (IP) - The user ID and the user's IP address are logged
-   Action - The action that was performed

  


