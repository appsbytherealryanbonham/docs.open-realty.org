# Add/Edit Agent & Member Fields

The Agent and Member Field Editors are  basically the same for Editing
or Adding a field. The only difference will be when editing you'll
already have information in the fields and will have the Update & Delete
buttons and when adding you will only have an Add Field button. Edit an
existing field by selecting it from the pulldown menu selection.

<img src="/assets/f32f30c4d78a89238be95d39608dbbac4db704c0.jpg" class="libimg" width="489" height="466" />

  

Field Name - This is the name of the field as it will be stored in the
database.

Field Type - The Type of field that this is. (Text, Textarea, select
list, select box, etc....)

Required - Is this field required to be filled out?

Field Caption - The Caption for the field that will be displayed to
users.

Field Elements - The choices for multiple selection Field Types such as
select lists, option boxes etc. Each choice should be separated by
double pipes (\|\|) Do not use spaces or returns after each option, run
them all together as one line.

  

Example:

  

Option 1\|\|Option 2\|\|Option 3\|\|Option 4\|\|Option 5

  

Default Text - The Default text to be displayed in this field.

Field Tool Tip - Adds a tool tip to the add/edit user pages for the
field.

Field Order on Listing Page - The order in which the Agent fields should
be displayed.

Show Field to - Use this option to restrict who can see this field. If
you have a field you only wish other Agents to see, you would select
"Agents Only" so that only logged in agents will see this field.


