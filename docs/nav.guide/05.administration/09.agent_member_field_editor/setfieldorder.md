# Set Field Order

This is a single page for setting the field order of all of your Agent
or Member fields at a glance. You can edit the Field Order on Listing
Page value to raise or lower a field's position.

  

<img src="/assets/ea3da9325a96dc7190f21135d4b71632771c340f.jpg" class="libimg" width="717" height="315" />

  

Click \[Set Order\] to save your changes.


