# Page Editor


The Open-Realty Page Editor, is a WYSIWYG and HTML source code editor
for editing and adding pages to your OR web site. All page content
created in the Page Editor is stored in the OR database, and is
displayed when retrieved using a numeric page ID\# . Using  template
tags you can also embed dynamic content within any page created

  

The content of the index (Home) page is controlled by the Open-Realty®
Page Editor. In addition the default installation also has three
additional pages that are Page Editor created pages, those pages are the
About Us, Contact Us and the Legal Page.

  

The Page Editor Screen:

  

Edit Page - Select the Page Title you wish to edit and select Edit Page

  

Using PHP in a page - To insert PHP into the WYSIWYG pages, you first
must turn on Execute PHP in the WYSIWYG Configuration. Then when
entering the PHP command, you need to enter it in code view on the
editor and surround it in html comments. For Example, if you are trying
to enter "&lt;?php echo 'text'; ?&gt;" you need to enter
"&lt;!--&lt;?php echo 'text'; ?&gt;--&gt;". Note that there are no
spaces between the html comment marks and the PHP opening/closing tags.

  

Create New Page - Click on this to Create a new page.

  

Title - The Title of the page - Will be the full title of the page, will
not append to the configured default page title.

  

Meta Description - The Meta Description for this page.

  

Meta Keywords - The Meta Keywords for this page.

  

Template tag for page - This is the template tag for the page you are
editing. Use the template tag in your templates to link to the page you
are working on.

  

Link to this Page - This is a link to the page, in case you wish to link
from an external source to the page you are working on.

  

Page Editor Window - In this area the page editor you selected in the
WYSIWYG Configuration will be displayed. Usage of each editor would be
too extensive to include here but most of it is pretty standard WYSIWYG
editor stuff.

  

Submit - Save your changes

  

Delete Page - Delete the page you have selected from the database.


