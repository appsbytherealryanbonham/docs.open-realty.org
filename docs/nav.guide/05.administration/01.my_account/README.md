# My Account

The My Account (User Manager) contains options for editing the currently
logged-in user's profile information, Images and Files.


All profile fields are created via the Agent or Member Field editor.
Fields with a red \* next to them indicate that field is required and
must be filled-in in order to save the record.


