# Displaying Files

To display files on your listings, user profile or agent lists you must add a template tag to your template where you want a list of the available files to appear so they may be downloaded. See the [Template Documentation](../../../06.template_documentation/ortemplatesystemlogic.md) section for available template tags.
