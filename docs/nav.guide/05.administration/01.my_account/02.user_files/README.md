# User Files

The File Editor is where you can upload or modify files for your users.
The File Editor is located by first editing a user through the User
Manager or by clicking on Edit My Account. You will see on the User
Manager a list of existing files on the left side of the user manager
and an option to Edit Files. Clicking on the Edit Files option will
bring you to the File editor.

  

Upload a File - This option will be available if you have less than the
maximum allowed files uploaded for your user. This value is set in the
Site Config. To upload a file click on browse and select the file you
wish to upload off of your local computer. You will have fields to
upload the number of allowed files that you have remaining from the
maximum allowed files setting. Click submit and it will be uploaded to
your server. See the Upload Settings section of the Site Config for more
information on uploads.

  

Return to Editing Listing - This will return you to the User Manager
screen.

  

Below the Upload a File area a list of all the existing files will be
displayed which will include an icon of the file type, the file name and
the size of the file. Below each file will be the following options:

  

Delete - Clicking this will delete the corresponding image and it's
thumbnail.

  

Field Order on Listing Page - This defines the order the file will be
displayed. This value is automatically incremented for each new file
that is uploaded, you can update this value to change the order the
files are displayed.

  

Caption - The caption that will be displayed with the file.

  

Description - The description that will be displayed when viewing the
file.