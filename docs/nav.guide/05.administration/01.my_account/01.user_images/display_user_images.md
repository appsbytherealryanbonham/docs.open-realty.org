# Displaying User Images

To display your user images on your listings, user profile or View Agent list you must add a template tag to your user template where you want the images to be displayed. This is already done for any example templates provided. There are several methods for displaying thumbnails of your images and full size images. See the [Template Documentation](../../../06.template_documentation/ortemplatesystemlogic.md) section for available user image template tags.
