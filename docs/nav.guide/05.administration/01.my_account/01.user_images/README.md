# User Images

The Image section of the media widget on the right of the page is where
you would upload, modify or delete images for a user account. Clicking
on an existing image will allow you to manage that image. Clicking on
the "Upload a Picture" button will allow you to upload a new image.  You
can re-order the photos by clicking in the upper right-hand corner and
dragging each to the desired locations.

  

Upload a Picture - This button will be available if you have less than
the maximum allowed images currently uploaded for your user account. The
maximum user photo limit is set in Site Config. To upload a picture
click on browse and select the photo you wish to upload from your local
computer. Click submit and the image will be uploaded to OR. See the
Image Upload Settings section of the Site Config for more information
regarding uploads.

  

  

Edit Media popup window

  

This window displays details of the uploaded image and thumbnail. It
also displays the thumbnail and the full size image.

  

Caption - The caption that will be displayed with the image.

  

Description - The description that will be displayed when viewing the
image.

  

Delete - Clicking this will delete the corresponding image and it's
thumbnail.
