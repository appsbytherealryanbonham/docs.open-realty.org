# Modify Property Class
 
This page is used to modify basic Property Class details

<img src="/assets/74d9f15c0c7d4f3cc1b3f5ce58acd452d7bf3c31.png" class="libimg" width="819" height="212" />

  

Class Name - This is the name of the class as it will be stored in the
database and displayed to users.

Rank - The numeric rank of the class for the display order of the class.
Classes set with a lower rank will display first.
