# Insert Property Class

Insert New Property Class

This page is used to add a new property class.

<img src="/assets/126541691893370f057ded92650b42063903ba6d.png" class="libimg" width="802" height="344" />

  

  

Class Name - This is the name of the class as it will be stored in the
database and displayed to users.

Class Rank - The rank of the class for the display order of the class.
Classes with a lower number will display first.

Select the listing fields to add to this class - This option allows you
to quickly select any existing listing fields that you wish to
automatically add for use with this new property class. The insert page
is the only place that you will have the ability to mass assign listing
fields to the property class so it is a good idea to already know which
fields you want in your new property class when adding it. Listing
fields can still be added or asigned later, however you must edit each
listing field individually to select the property class in the
[Listing Field Editor](../08.listing_field_editor/quickeditor.md)

  


