# Search Results Order Tab

<img src="/assets/589f724a7c5f0c5da53b16dd48265f7c9231aec4.jpg" class="libimg" width="548" height="402" />

  

This section displays a list of all  Listing Fields that are set to
display on the Search Results page.

  

Clicking on a field caption (name) opens the Listing Field Editor with
that field selected for editing.These Listing Fields can be reordered by
dragging them to change the order they will appear on the Search Results
page. Reordering here will override and update the Field Order setting
on the Search Result Option tab.

  

Note: A field with a Field Order of 1 (first) will be displayed in the
{field\_1} location in the search\_results template. The template you
use may only contain a few {field\_X} template tags, you may wish to
examine your search results template and add or remove any tags as
necessary to suit your needs.

  

example:

&lt;tr&gt;

    &lt;td&gt;{field\_1}&lt;/td&gt;

    &lt;td&gt;{field\_2}&lt;/td&gt;

    &lt;td&gt;{field\_3}&lt;/td&gt;

    &lt;td&gt;{field\_4}&lt;/td&gt;

    {field\_5\_block}

      &lt;td&gt;{field\_5}&lt;/td&gt;

    {/field\_5\_block}

&lt;/tr&gt;

  

Review the Search Results Template Tags section of the documentation for
more info regarding usable template tags.



