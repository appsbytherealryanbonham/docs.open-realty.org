# Search Result Options

<img src="/assets/8345797dd99471c34c1ac715014f667dce5d86e4.jpg" class="libimg" width="634" height="565" />

  

Display on Search Results page - Select 'yes' to make this field show up
on the search results page.

  

Field Order on Search Results Page - The rank of this field on the
Search page. It corresponds to the {field\_\#} tag present in the search
result template (see
[Search Results template tags](../../../06.template_documentation/03.Public%20Site%20Templates/12.search_results_templates/searchresultstemplatetags.md).
A field with a Field Order of 1 will be displayed in the {field\_1}
location in the search\_results template. Having multiple fields set
with the same Field Order will cause unpredictable results.



