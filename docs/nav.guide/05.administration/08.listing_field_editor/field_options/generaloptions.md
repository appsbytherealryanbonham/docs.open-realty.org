# General Options

<img src="/assets/d0e0d2e8c89587c50a2203f86a74f8b854615cf8.jpg" class="libimg" width="632" height="563" />

  

Field Name - This is the name of the field as it will be stored in the
database.

Field Caption - The Caption (friendly name) for the field that will be
displayed to users.

Required - Is this field required to be filled out?

Field Type - The Type of field that this is. Affects the Add listing and
Edit listing forms that Agents/users fill out.

-   Text - Provides agents with a standard text box to enter data into.
-   Textarea - Provides agents with a larger text box to enter data
    into. Use full for things like remarks.
-   Select - Provides agents with a drop down list based on the field
    elements, from which they can select one option.
-   Select Multiple - Provides agents with a drop down list based on the
    field elements, from which they can select multiple options.Option
    Box
-   Option Box - Provides agents with a list based on the field elements
    with radio options next to each item, from which they can select one
    option.
-   Check Box - Provides agents with a list based on the field elements
    with radio options next to each item, from which they can select
    multiple options.
-   Divider - Creates a divider "--------------" in place of the field
    to help break up a edit listing or add listing page's layout. No
    real data can be entered into this field.
-   Price - Provides agents with a standard text box to enter a price
    into. Price should be entered without any money sign, commas, or
    decimals. The money sign will prefix the text box in the add listing
    page and ".00" will suffix it. This also tells the system to format
    the price as setup in the site config for all listing display pages.
-   URL - Provides agents with a standard text box to enter a URL into.
    Agents should enter the full url including http:// the system will
    then create a hyperlink for then on the listing pages with from this
    URL.
-   Email - Provides agents with a standard text box to enter an email
    address into. Email address will be clickable links on the listing
    page. NOT RECOMMENDED TO USE, as spam bots can easily read this
    address from your page and start spamming the email address entered.
-   Number - Provides agents with a standard text box to enter a number
    value into. Will be formatted with decimal points as setup in site
    config.
-   Decimal - Provides agents with a standard text box to enter a
    decimal value into. Same as number, except meant for values that
    will always have a decimal value.
-   Date - Provides agents with a standard text box to enter data into,
    will show and enforce the required date format as defined in the
    site config.
-   Latitude - Provides agents with a standard text box to enter a
    latitude into. There can only be one latitude field defined. It is
    used by the search engine when doing distance searches and by some
    mapping add-ons such as TransparentMaps.
-   Longitude - Provides agents with a standard text box to enter a
    latitude into. There can only be one latitude field defined. It is
    used by the search engine when doing distance searches and by some
    mapping add-ons such as TransparentMaps.

  

Field Elements - The choices for multiple selection Field Types such as
select lists, option boxes etc. Each choice should be separated by
double pipes (\|\|) Do not use spaces or returns after each option, run
them all together as one line. For example: Option 1\|\|Option
2\|\|Option 3\|\|Option 4

  

Default Text - The Default text to be displayed in this field

  

Field Tool Tip - Add a tool tip to the field that will display a
question mark icon next to the field in the listing editor/add listing
page so users can click on the icon and obtain information/tips
regarding completing the field.

  

Maximum Field Length - Configure the maximum number of characters that
can be entered into the field.

  

Field Tool Tip - Adds a tool tip to the add/edit listing pages for the
field. (note: requires new css settings.

  

Maximum Field Length - Maximum number of characters that can be entered
into this field. Applies to text, price, url, email, number, latitude,
longitude, and decimal field types.

  

Show Field to - Use this option to restrict who will see this field. If
you have the field set to display on your site using one of the template
areas or individual field rending template tags then this will determine
who will see this field. Available options are:

-   All Visitors - Everybody will be able to view this field.
-   Members and Agents - Only registered users of your site can see this
    field.
-   Agents Only - Only Agents will be able to view this field.
-   Admin Only - Only Admins will be able to view this field on the
    front end of the site.

  

Available in the following property classes - Use this option to select
which property classes this field will be available in. You can select
multiple classes by clicking and dragging or by holding down the "Ctrl"
key while clicking.


