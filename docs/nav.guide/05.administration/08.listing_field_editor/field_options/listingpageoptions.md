# Listing Page Options

<img src="/assets/17b669fa622d4b8594e92361028765dc6d03bb00.jpg" class="libimg" width="633" height="566" />

  

Location on Listing Page: Selects which display location on the Listing
detail page where this field will appear, such as top\_left, top\_right,
center, and etc. There are 8 Display Locations by default, additional
locations can be defined in Site Config

  

Field Order on Listing Page: Determines the order on the Listing Detail
page this field is displayed. A lower number will display before a
higher number (1 = first), and in cases where two fields set to the same
display location have the same Field Order, OR will order alphabetically
using the Field Name.

  

