# Search Page Options

<img src="/assets/79cabf97f721c7a272853f5124a5ffe1b570ed8a.jpg" class="libimg" width="634" height="566" />

  

Display on Search Page - Check this option to make the field searchable

Field Order on Search Page - The rank of this field on the Search page,
the lower the number the higher on the list of options it will appear
(1= first).  In cases where two fields on the Search Page have the same
Field Order, OR will arrange alphabetically using the Field Name.

Search Label - The label (caption) for this field that will appear on
the Search Page. The name you want site visitors to see.

  

Search Type - The type of search field that should be used for this
field, available options:

-   Partial Text Match - This search option presents the user with a
    text box to enter text into. It will then find any listings where
    this field contains the entered text.
-   Option list of individual values - Builds a option list, that allows
    multiple selections, based on the field elements for this field.
    This option requires that a listings have ALL the selected options
    to be returned in the search results.
-   Option list of individual values (OR search logic) - Builds a option
    list, that allows multiple selections, based on the field elements
    for this field. This option will return listings that contain ANY
    (one or more) of the selected options, vs. AND logic which would
    return only listings that match ALL selected options.
-   CheckBox list of individual values - Builds a list of options from
    the field elements with checkboxes next to each item. This option
    requires that a listings have ALL the selected options to be
    returned in the search results.
-   CheckBox list of individual values (OR search logic) - Builds a list
    of options from the field elements with checkboxes next to each
    item. This option will return listings that contain ANY (one or
    more) of the selected options, vs. AND logic which would return only
    listings that match ALL selected options.
-   Pull down list of individual values - Builds a pulldown list, that
    allows a single selection, based on the field elements for this
    field.
-   Distinct list - allow multiple - Builds a option list, that allows
    multiple selections, based on the actual field values for the
    existing listing. This option requires that a listings have ALL the
    selected options to be returned in the search results.
-   Distinct list - allow multiple (OR search logic) - Builds a option
    list, that allows multiple selections, based on the actual field
    values for the existing listing. This option will return a listings
    has ANY of the selected options.
-   Pull down menu, 1 selection only - Builds a pull down list, that
    allows a single selection, based on the actual field values for the
    existing listing.
-   Checkbox list of distinct values - Builds a list of options, based
    on the actual field values for the existing listings, with check
    boxes next to each item. This option requires that a listings have
    ALL the selected options to be returned in the search results.
-   Checkbox list of distinct values (OR search logic)- Builds a list of
    options, based on the actual field values for the existing listings,
    with check boxes next to each item. This option will return listings
    that contain ANY (one or more) of the selected options, vs. AND
    logic which would return only listings that match ALL selected
    options.
-   Radio button list of distinct values - Builds a list of options,
    based on the actual field values for the existing listings, with
    radio options next to each item. Radio Options allow only a single
    selection to be made.
-   Two pull downs for min/max ++ - Builds two drop down boxes, one for
    the user to select the minimum value from and one drop down for the
    maximum value. Builds the drop down by looking at the minimum value
    in the database and the maximum and then placing incremental values
    based on the Step value specified by the user. For Example if the
    minimum value is 1 and the maximum is 10 and you specify a step
    value of 2 you will get drop downs with the following options.
    1,3,5,7,9,10
-   Date range; two text fields - Provides two text boxes to allow user
    to enter two dates. Will return any listings where the date for this
    field is between the dates entered by the user.
-   Single/Exact Date Search - Provides a text boxes to allow user to
    enter a date. Will return any listings where the date for this field
    matches the date entered by the user.
-   Search For Empty(NULL) Values - Provides a check box, if checked
    will return only listings where there is NO value entered for this
    field.
-   Search For Non Empty Values - Provides a check box, if checked will
    return only listings where there is a value entered for this field.

  

++ Step by - If you used a range selection for the Search Type then this
is the value to step or increment each available option in the range
selection.

  

Note: Open-Realty by default uses the lowest value in the database and
highest value in the database foe the field as the lower and upper
bounds for a min/max search, and then uses the step value as the value
to increment the options by.

  

Assuming a "price" field example, if the lowest priced listing in OR is
500 and the highest priced is 1000 and you enter a ++ Step by: value of
100, you will end up with the following increments:

  

500, 600, 700, 800, 900, 1000

  

For performance reasons, there is a limit to how many incremental steps
Open-Realty will build. This limit is controlled via Site Config, (see:
[Listings tab](../../13.site_configuration/listings.md) Max Min/Max Steps) and this
defaults to a max of 100 steps or increments. If you choose a step value
that would result in more then the defined number of "Max Min/Max Steps"
steps being generated, OR will ignore your step value and calculate it's
own value that produces the max number of allowed steps.

  

To override the upper and lower bounds of the min/max box and define
your own, you can manually define the them as part of the Step Value, by
entering the step value in the following format.

  

MIN\_VALUE\|MAX\_VALUE\|STEP\_VALUE

  

For example, using "0\|300000\|20000" will start with 0 and increment to
300,000 in steps of 20,000.



