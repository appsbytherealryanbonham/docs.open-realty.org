# Quick Editor

<img src="/assets/63f3f1cb94c0672cfebda9fa00a3f6984cc920de.jpg" class="libimg" width="816" height="495" />

  

This section displays a list of all existing Listing Fields, displayed
in their Display Locations. Clicking on a Field Caption (name) opens the
Listing Field Editor for that field. The listing fields can be dragged
with the mouse to re-order them, and can be moved from one Display
Location to another which changes their arrangement on the listing
detail page. The default Quick Editor display contains the default
listing template's display location sections. New listing template
sections that are added to Site Config will also be added to this page.

  

The layout expects the original 8 Open-Realty Display Locations (Listing
Template Sections) to be present. Any user-created custom locations
should be added only at the end of the list of the 8 default locations
in Site Config.

  

Example with 2 added custom locations:

-   headline,top\_left,top\_right,center,feature1,feature2,bottom\_left,bottom\_right,custom1,custom2

  

You can control the styles of any new custom locations via CSS, simply
add the location name as a new class in the listing template editor CSS
file.

  

Example:

.custom1 {color: \#000000; font-size: 10pt;}

  

The other 8 default locations such as top\_left, top\_right and etc, are
already defined in CSS this way.


