# User Manager

The User Manager, as the name suggests, allows you to manage your OR
site users: Members, Agents, and the Admin.

<img src="/assets/caab4d39373d8f4bd56118c4f6f91c81fe6960c3.jpg" class="libimg" width="664" height="455" />

  

The navigation bar at the top of the User Manager allows the entry of a
specific user ID or other criteria to locate specific users, or the
display of users on this page can be sorted using different criteria.
New Agents or Members can be added via the "Add User" button at the top
right.

  

The User Manager displays a list of all OR users and some basic details
for each user.

  

Clicking on the Edit button will allow you to Edit that User. Clicking
on the Delete Button will Delete that user.

  

::: danger
Deleting an Agent user will also delete ALL of that Agent's listings.
Use with caution as this is not reversible.
:::


