# Blog Publisher (Navigation View)

The main screen of the Blog Manager is the navigation view which
displays all of your blog posts and details about them. At the top of
the page is several filtering options for displaying All blogs,
Published blogs, Drafts and blogs that are require review.

  

<img src="/assets/83fd2b88dc0438998c5417b4eadef9980d988959.png" class="libimg" width="840" height="525" />

  

Adding a blog post: Centered at the top of the blog navigation view is
the "Add Post" field and button. To add a blog post simply type the
title for your new blog post and click the "Add Post" button. You will
be taken to the editor view to complete entering your new blog post (see
the editor view section below for further information)

  

Working with existing Blog Posts.

Post - Shows the title of the blog post. Clicking on the post will take
you to the Blog Publisher.

Author - Shows which user created the blog post.

Keywords - Displays keywords associated with the blog post

Comments - Shows the number of comments for each blog post. Clicking on
this number or the edit icon beside it will take you to the Edit
Comments page of the Blog Manager

Date - Displays the data the blog post was created

Publication Status - Displays the status of the blog post. Statuses are:

-   Published - The blog post is published and active
-   Draft - The blog post is not active and still in draft mode
-   Review - The blog post requires review before it will be published

  

Click on the blog post's name or the edit icon next to it to edit the
contents of that blog in the editor.

  


