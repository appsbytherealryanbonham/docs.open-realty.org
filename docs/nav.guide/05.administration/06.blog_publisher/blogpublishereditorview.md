# Blog Publisher (Editor View)

The editor view of the Blog Publisher

  

<img src="/assets/9237abea6d1339726326054ed44f27c43a9f491a.png" class="libimg" width="851" height="726" />

The editor consists of the following sections:

  

Post Title - The title for your blog post. (The title must be a unique
title you may not submit a blog post with a duplicate title of another
blog post)

  

Blog Editor Window - This is the main post editor window. This is where
all the content of your blog post will be entered and edited. The
WYSIWYG editor uses the popular
<a href="http://ckeditor.com/" class="rvts159">CKEditor</a> web text
editor.

  

Word Count - The number of words in your blog post.

  

Template tag - This is  a copyable template tag for use in your
template(s). Copy and use this template tag in your templates to link to
the blog post you are working on.

  

Permanent Link - This is the URL to access the blog post directly, in
case you wish to link to the blog post you are working on from another
site, system, or if you do not wish to use a template tag for whatever
reason.

  

Search Engine Optimization: - Customizable SEO page title,
<a href="http://en.wikipedia.org/wiki/Meta_element" class="rvts159">meta description and keywords</a>
for this blog page.

  

Publication Status: Viewable Status of the blog post being edited.

-   Published - The blog post is published and active. Post is not
    viewable by site visitors.
-   Draft - The blog post is not active and still in draft mode. Post is
    not viewable to site visitors.
-   Review - The blog post requires review before it will be published.
    Post is not viewable to site visitors.

This is also where you would save or delete your blog post, or revert
changes if you have any unsaved changes. The revert changes button does
not appear unless there are unsaved changes.

WARNING: deleting a blog post is final and not reversible.

  

Categories:

-   Categories tab - A list of existing Blog categories for organizing
    blog posts. The "Default" category is selected by default, a blog
    post can belong to multiple categories simultaneously You can create
    a new blog category here as well.
-   Most Popular category tab - Displays a list of the most popular blog
    categories.
-   New Category - Create a new category by clicking the New Category
    button

  

Post Tags: Each blog post can be further organized under one or more
post tags. These are different than categories as they are not
hierarchical and cannot have parent-&gt;child relationships, and can be
used to gauge the popularity of frequency of key words in your blog
post. Tag names must be unique. Enter any Post Tags one at a time, and
they will be added.

-   Show Most Used Tags - Displays the most used blog post tags on your
    site.



