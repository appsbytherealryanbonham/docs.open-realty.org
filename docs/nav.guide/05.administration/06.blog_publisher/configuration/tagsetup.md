# Tag Setup

Blog Publisher Configuration: Tag Setup:

This tab of the Blog Configuration is where the tags are setup and
configured.

<img src="/assets/f832b7333104e10809797cafe5db2d969f5ecc0a.png" class="libimg" width="820" height="322" />

  

Tag Name - This is the raw name of the tag as it is stored in the
database.

  

SEO Name - This is the search engine optimized name of the tag. This is
the name that is displayed on the front end and what will be seen by
search engines for better indexing.

  

Description - The description of the tag to explain what topics or
information posts with this tag are talking about.

  

Posts - Displays the number of blog posts that are currently using the
tag.

  

Options - There are 2 icons here to handle the administration of the tag
as follows:

  1. Note Pad - Edit the tag

  2. Red X - Delete the tag

  

Above and below the tag list there is an option to "Add a New Tag".
Clicking on the Add button will bring up a window with the above fields
to fill out to add a new tag. There is an additional option of "Parent"
on the Add and Edit category windows. The parent option is to specify
which, if any, other category is a parent to this category.


