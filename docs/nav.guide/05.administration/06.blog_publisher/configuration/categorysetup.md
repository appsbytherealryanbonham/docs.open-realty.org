# Category Setup

Blog Publisher Configuration: Category Setup:

This tab is where the categories are setup and configured. The default
category can not be removed or edited.

<img src="/assets/8403642de3ba383652385869781fff8fa9cc083a.png" class="libimg" width="815" height="455" />

  

Category Name - This is the raw name of the category as it is stored in
the database.

  

SEO Name - This is the search engine optimized name of the category.
This is the name that is displayed on the front end and what will be
seen by search engines for better indexing.

  

Description - The description of the category to explain what topics or
information posts in this category will be talking about.

Posts - Displays the number of blog posts that are currently using the
category.

Options - There are 4 icons here to handle the administration of the
category as follows:

  1. Up Arrow - Move the Category up on the list

  2. Down Arrow - Move the Category down on the list

  3. Note Pad - Edit the Category

  4. Red X - Delete the Category

  

Above and below the category list there is an option to "Add a New
Category". Clicking on the Add button will bring up a window with the
above fields to fill out to add a new category. There is an additional
option of "Parent" on the Add and Edit category windows. The parent
option is to specify which, if any, other category is a parent to this
category.

  


