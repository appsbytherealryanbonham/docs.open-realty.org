# General Settings

Blog Publisher Configuration: General Settings

<img src="/assets/79193bbecabd20a15b3888ad955cef17197c3bef.png" class="libimg" width="815" height="526" />

  

Blog Posts Per Page - Number of blog post to show per paginated page.

  

Moderate comments - Do comments require moderator approval before
showing on the site?

  

Allow Pingbacks - Allow other blogs to pingback to your posts?

  

Send URLs Pingbacks - Send pingbacks to all URLS in your post?

  

Send Services Pingbacks - Send pingbacks to all services listed below.

  

Pingback Services - List of Pingback services to notify of new blog
posts. (one per line)

  

Pingback services are services to notify of new blog posts. These are
like search engine submission sites specifically for blog posts which
help make sure that people can find the information that you are posting
via your blog.


