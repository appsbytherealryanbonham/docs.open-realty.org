# Menu Editor
  

The Open-Realty Menu Editor is a versatile tool useful for creating
nested custom site navigation menus that link to existing Page Editor
pages, standard actions such as "View Agents" or "Search Listings", as
well as entering links to onsite or offsite content.  A Menu created
with the OR Menu Editor can be added to your custom template (usually in
main.html) by inserting a simple template tag such as:
{render\_menu\_1}. Any changes to the menu made via the editor are then
reflected immediately and automatically to site visitors.  

  

Once a menu item is created, it can be dragged to any location on the
tree-view, and parent --&gt; child (submenu) relationships can easily be
created.

  

<img src="/assets/4b3d4a560ff06e44a972ce4f7892bdacf0c87bdb.jpg" class="libimg" width="666" height="656" /><img src="/assets/893516846d48c095c96d222697bebf58d1b37cbb.png" class="libimg" width="151" height="64" />

  

Menu Editor Options

  

Select Menu: Select the menu you wish to work with.  

New menu: Creates a new empty menu, and assigning the next available
Menu ID\#

Delete menu: Deletes the currently selected menu entirely.

Menu Item Name: The name that will appear for the currently selected
menu item

Viewable By: Used to restrict display of the menu item to specific
users. Unchecked = hidden.

Menu Item Type: Determines the type of link to create, 6 types are
available:

-   Action -  Links to one of many OR actions such as View Users, Search
    listings, View Blog and etc.
-   Page - Links to a specific Page Editor page.
-   Blog - Links to a specific Blog publisher page.
-   Block Contents- Links to OR elements that have multiple values such
    as Recent Blog posts, Recent Blog comments
-   Custom URL - Accepts any valid URL entered.
-   Divider - A placeholder used to separate links on the menu (not
    linkable).

Target: When used with a linkable menu item, allows you to set a valid
HTML &lt;a&gt; target attribute

Custom CSS class: When used with a linkable menu item, allows you to
define a custom CSS class for the &lt;a&gt;

  

Navigation menus are generated and displayed to site visitors using
nested HTML unordered lists, in a format usable my many popular CSS and
javascript dynamic menu systems.

  

For Information on how to apply CSS styling to the menu, see: Styling
Dynamic Menus

  

Note: It is not possible to change or edit the Menu Name present in
Select Menu once it is created. These names are only used to identify
the menu to the person editing it, so if you make a typo or some other
mistake when you create a new menu and you cannot live with it, you will
have to delete that menu and create a new one to fix any mistake in the
name.

