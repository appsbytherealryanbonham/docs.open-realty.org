# Manual Installation

This tab provides a simple file select field to select an add-on to be
manually installed on your Open-Realty site. The file should be a
compressed ZIP file and compatible with the add-on Manager to be
successfully installed.

  

To update an add-on with the Manual Installation function, you must
check the "Update Existing Add-On" option to enable the add-on Manager
to overwrite the existing version of the add-on.


