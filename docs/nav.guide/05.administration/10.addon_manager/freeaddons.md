# Free Addons

This tab displays a list of currently available FREE add-on listings
from the Open-Realty add-ons site.

  

-   add-on Title - Displays the Title of the add-on
-   Author - Displays the Author of the add-on
-   add-on Version - Displays the current version of the add-on
-   Actions - The available Action icons are as follows:

1.  Clipboard - View Add-on Page - Displays the full details of the
    add-on listing from the Open-Realty® Add-Ons site.
2.  Plus Sign (+) - Install Addon - Automatically downloads and installs
    the selected add-on to your Open-Realty site. Once installed, the
    add-on will appear in the "Installed Addons" list on the first tab
    of the Add-on Manager.

  

Show add-ons not listed as compatible with your OR version - By default
the Add-On Manager will only display add-ons that are listed as being
compatible with your version of Open-Realty. Checking this option will
display ALL add-ons even if they are not listed as being compatible with
your installed OR version. Some add-ons may create problems if they are
not fully compatible with your version of OR, while some may function
without any issues. Use caution if using an untested add-on with your
site.


