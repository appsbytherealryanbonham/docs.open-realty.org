# Commercial Addons

This tab displays a list of currently available COMMERCIAL Add-on
listings from the Open-Realty Add-Ons site.

  

-   add-on Title - Displays the Title of the add-on
-   Author - Displays the Author of the add-on
-   add-on Version - Displays the current version of the add-on
-   Actions - The available Action icons are as follows:

1.  Clipboard - View add-on Page - Displays the full details of the
    add-on listing from the Open-Realty add-ons site.
2.  Dollar Sign ($) - Purchase - Opens a new browser window and takes
    the user to the purchase page for the add-on to complete the
    purchase.

  

Show add-ons not listed as compatible with your OR version - By default
the Add-On Manager will only display add-ons that are listed as being
compatible with your version of Open-Realty. Checking this option will
display ALL add-ons even if they are not listed as being compatible with
your installed OR version. Some add-ons may create problems if they are
not fully compatible with your version of OR, while some may function
without any issues. Use caution if using an untested add-on with your
site.


