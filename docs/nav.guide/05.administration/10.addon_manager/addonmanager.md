# Add-On Manager

The add-on Manager displays any installed add-ons, add-on status,
available template tags and help, allows for uninstalling add-ons, and
searching for and installing new add-ons.

The Add-on Manager is organized into three tabbed pages to organize the
content and features of the add-on Manager.

The tabs and their content are as follows:
