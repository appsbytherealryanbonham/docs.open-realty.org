# Installed Addons

The default, first tab displays a list of all add-ons installed in your
Open-Realty site. Details of the add-on are displayed as follows:

  

Addon Name - Displays the name of the add-on

Addon Version - Displays the add-on's Version

Addon Status - Displays a status of 'OK' if the add-on is properly
installed and up to date. If there is an update available or a problem
it will be noted in this column.

Actions - The available Action icons are as follows:

-   Magnifying Glass - Check Update - Checks for available updates if
    supported by the add-on
-   Clipboard - Help - Displays the Add-on Help information if supported
    by the Add-on. Should display a list of all available Add-on
    Template Tags and Actions.
-   Red X - Uninstall - Uninstalls the add-on, will remove ALL of the
    add-on files from the server and, if supported, also remove all
    database entries created by the Add-on.

