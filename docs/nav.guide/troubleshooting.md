# Troubleshooting

I deleted, lost, over-wrote or otherwise messed up my common.php file,
what do I do?

  

Ideally, you are prepared and have a backup of your common.php file with
the proper settings that you can put back on your server. Since you're
reading this, that is probably not the case (but remember this for next
time!). To fix this, do the following:

  

  1. Backup your OR database.

  2. Make sure  the file: /include/common.dist.php is present in your OR
installation.

  3. Restore the /install folder for the specific version of OR you are
using.

  4. Run the installer and select the New Installation option

  5. Select 'yes' for the Developer Mode install at the bottom.

  6. Complete the installation, entering your DB name, username and
password.

  

You will receive a lot of errors when you complete the installation, but
it will write a new common.php file for you with your correct DB
settings. You can now remove the /install folder and your site should
function properly again. This would be an excellent time to backup your
common.php file with all the values stored in it to prevent this from
happening again.

  

------------------------------------------------------------------------

  

I receive 404 Errors when trying to view Open-Realty® pages

  

You have likely enabled Search Engine Friendly (SEF) URLs in your site
configuration under the SEO tab. In order for Open-Realty to function
properly with SEF URLs your server must be running the
<a href="http://www.apache.org/" class="rvts534">APACHE</a> web server,
with
<a href="http://httpd.apache.org/docs/current/mod/mod_rewrite.html" class="rvts159">mod_rewrite</a>
enabled.  See the Search Engine Friendly URLs and Optimization
subsection of the Installing Open-Realty section of this documentation
for more information and details.

  

------------------------------------------------------------------------

  

JPG Images are distorted/discolored after I upload them

  

If you are using the GD Libs resizing tool (set in Site Config) make
sure your server has PHP GD Version 3.x or higher available. Also make
sure any resizing settings in Site Config are not set larger than the
original source images, and use a quality setting of at least 65.

  

------------------------------------------------------------------------

  

JPG Images are rotated sideways after I upload them.

  

Many newer digital cameras (especially smart phones) have a built-in
orientation sensor. The output of this sensor is used to set the EXIF
orientation flag in the image file's metatdata to reflect the
positioning of the camera with respect to the ground. The photo is still
physically stored sideways or rotated, it is then up to any photo
viewing software to attempt to rotate the image on the fly for display
determined by the EXIF orientation value. This is not presently
supported by windows versions earlier than 10, and only in a couple of
web browsers via CSS3.  OR (as of v3.2.11) will attempt to check for the
EXIF orientation flag and physically rotate the image to its intended
orientation prior to storing it during the resize operation. However, if
the photo uploaded is smaller than your resize settings in OR it will be
stored exactly as-is.

  

------------------------------------------------------------------------

  

I see this error on my search results page:

  

Notice: A non well formed numeric value encountered in
include/search.inc.php

  

This error occurs when you have a listing field setup as a field type of
"date" and have a non date value stored in that field. This usually
occurs when the field is originally configured as a different field type
and then changed to "date". To fix it you need to remove any non date
values from the listing field for all of your listings, or change the
field type to "text".

  

------------------------------------------------------------------------

  

Search results don't display any listings but it says there should be
search results

  

This problem occurs when you have a default search results sort by field
defined that does not contain data or is not available in the specific
property class that you are searching. Make sure that whatever field you
have configured as the "Default Sortby" in the Site Configuration under
the Search Results tab is available for use in every property class and
that it always contains data by making it a required field in the
Listing Field Editor.

  

------------------------------------------------------------------------

  

I have my search results page setup to sort by Price, Bedrooms and
Bathrooms, but the sorting does not work correctly and gives what seem
like random results.

  

Check your
[Site Config --> Listings Tab](05.administration/13.site_configuration/listings.md)
for the "Static Sortby Option" setting under Search Options. This needs
to be set to: None.

  

------------------------------------------------------------------------

  

Search Page is slow to display

  

There are a few things you can do to speed up rendering of the search
page.

  

  1. In Site Config under the Listings tab, set "Show Count In Search
Options:" to off.

  2. Increase the "Step" value on any  min/max search fields you have
setup so the form has to build less options.A low setting  for the "Max
Step value" is recommended  so that Open-Realty will not create an
unreasonable amount of step values.

  3. Look for min/max options in your live  forms where the max value is
out of place. Something showing-up as an option like 100 bathrooms is
almost certainly a data entry error. Find the listing with the incorrect
large value and correct it. If you are importing data from a MLS and
find a similar error, contact them to have the data corrected at their
end.

  

The search page is rendered dynamically from the Open-Realty database
using values from listings and settings in Site Config. If, after trying
these tips, you still feel your search page is rendering too slowly, you
may consider creating a hard coded search page. See the Custom Search
Pages section of the Search Page Template documentation for info.

  

------------------------------------------------------------------------

  

RSS Feed is broken

  

RSS Feeds may break in certain circumstances when there are special
characters in the Listing Title field and Multi-Byte String support is
not enabled in PHP. There should not be any problems without Multi-Byte
String support as long as the database collation and Open-Realty are
both set to UTF-8 character sets.

  

------------------------------------------------------------------------

  

I receive a "Not Authorized" error message when trying to sign up as a
Member or Agent

  

For security purposes Open-Realty performs a site referrer check when
somebody visits the Agent or Member sign up page. The referrer check
verifies that the user came to the sign up page from a link within the
Open-Realty site and not from a link elsewhere on the internet or from
typing the address directly into a web browser. The purpose is to help
reduce the number of false sign-ups generated from automated spam bots.
To correct the problem, place a link to the sign-up page within your
Open-Realty site template and click that link to visit the sign-up page.

  

