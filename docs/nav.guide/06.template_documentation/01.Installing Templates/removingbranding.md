# Removing Powered By Tag

  
Open-Realty's powered by image is rendered in templates by the
{powered\_by\_tag}. Simply edit and remove this tag from your custom
template files and the branding logo and link will be removed.

  

Public Site Template branding locations:

The {powered\_by\_tag} can be found in the following template files:

  

/template/yourtemplate/main.html

/template/yourtemplate/printer\_friendly.html

/template/yourtemplate/popup.html

  

  

Administration area Template branding location:

  

/admin/template/yourtemplate/main.html

  

  

  

  


