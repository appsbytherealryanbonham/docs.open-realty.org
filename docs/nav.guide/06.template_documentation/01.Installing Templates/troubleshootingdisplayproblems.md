# Troubleshooting templates

After spending a few hours creating customizations, or making a new
design, you may find that your site does not display as expected, or it
looks fine in one browser and not a different browser. Sometimes you can
spend as much time trying to get one piece of text or an image to
line-up where you want it, as it required to create the whole layout. If
you follow the advice below, you can save significant time during the
debugging process.

  

-   FireFox or Chrome should be used when developing anything for the
    web, because these browsers adhere the most closely to W3C coding
    standards, where Internet Explorer still leaves much to be desired
    in terms of full HTML and CSS compatibility. Chances are, if you
    perform your site development using one of these browsers and not
    IE, your first attempt will probably validate, and will probably
    also display correctly in IE the first time.  

  

-   Validate your code before making a lot of changes trying to track
    down a problem. When PHP or other similar programming languages
    encounter a syntax error in code they are running, the code will not
    run at all, and will display a meaningful syntax error. Coding in
    HTML and CSS on the other hand, can be more forgiving, as most web
    browsers will attempt to re-interpret any coding errors you made and
    will even try to "fix" them for you by reverting to "quirks mode".
    Each browser maker handles quirks mode differently, so one browser
    might display your design the way you expect even though it contains
    coding errors, while another browser would display the same design
    differently. Making sure your HTML and CSS code validates, will
    eliminate the browser needing to run in quirks mode, and only at
    that point is what you see a full and accurate representation of how
    a design is going to display in most browsers. Not all validation
    errors the validation service reports are related to display
    elements, repair items such as unclosed tags, and invalid CSS first.

  

HTML/XHTML validation service:

http://validator.w3.org/

  

CSS Validation service:

http://jigsaw.w3.org/css-validator/

  

  

-   Don't try to create a design to support every version of Internet
    Explorer, especially old ones. HTML5 and all of the richness it
    provides is everywhere, and is the basis for mobile-device browser
    support. If you (or your client) are still worried about supporting
    IE 6 or IE 7, you should seriously re-evaluate that unless you don't
    expect to use a very complicated design, or a design that uses or
    works with modern technology..

  

-   Learn how to use the developer's tools in the Chrome web browser and
    Firefox. Built into these browsers are powerful tools that will
    allow you to examine (and even manipulate) all aspects of the web
    page being viewed. You can obtain a lot of information regarding the
    code and layout by simply highlighting a part of a design and right
    clicking, selecting "Inspect Element".

  

-   If you are using jQuery or JavaScript or CSS embedded into your
    templates, you will want to be careful how you use curly brace
    characters { } in your source code. OR's template engine will treat
    the contents between any curly brace pairs that exist on the same
    line as a template tag. Any "tags" it finds that it also cannot
    match to an internal function or add-on function will be removed
    before the output is sent to the browser which will break inline
    JavaScript and CSS using curly braces in this manner. To avoid this
    happening you only need to ensure that any curly braces in your
    source code have their content contain a leading and trailing space,
    or cause any closing brace to be placed on the next or a different
    line.

  

example:

&lt;!- This will break, OR thinks it contains a template tag because {
and } occur on the same line -&gt;

&lt;script&gt;

       $( "button" ).click(function() {$( "p" ).empty();});

&lt;/script&gt;

  

&lt;!- This will work, curly braces have been separated by new lines
-&gt;

&lt;script&gt;

       $( "button" ).click(function() {

               $( "p" ).empty();

       });

&lt;/script&gt;

  

This consideration does not apply to any JavaScript or CSS files that
you load via external sources/files.

  

example:

  

&lt;script type="text/javascript"
src="http://somesite.com/some/js/lib/lib.js"&gt;&lt;/script&gt;

  

&lt;link href="http://http://somesite.com/some/css/style.css"
rel="stylesheet" type="text/css"&gt;

  


