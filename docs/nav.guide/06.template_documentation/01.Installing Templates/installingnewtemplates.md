# Installing New Templates

Additional templates (third party) can be installed using the following
steps:

  

-   Download the template - Most templates will be distributed in a
    compressed ZIP file.
-   Extract the contents of the compressed file to your hard drive.
-   Wherever you extracted the .zip file, there should be a single
    folder (the template's name), that contains all of the template
    files. Look inside this folder and verify the template files such as
    'main.html' all appear in this folder.\*        
-   Upload the template folder to your server placing it beneath the
    /template/ folder of your site. e.g.: /template/templatename/
-   Log into the OR administration area and click on Site Config.
-   On the Template tab the Site Template option drop down list should
    now contain the name of the template folder you just uploaded.
    Select your new template and save your changes.
-   You may also need to select different templates for the Listing
    Templates, Search Results Templates etc if your new template does
    not have the same named templates of those types.

  

\* In some cases your extracted files may be contained in a second sub
folder of the same template name or may be extracted without being
contained inside a folder. Make sure the main.html and the rest of the
template files are in the proper folder:
/template/YOURTEMPLATENAME/main.html


