# Creating a public site Template

The fastest (and safest) way to hit the ground running building your own
custom template is to start with one of the example templates, such as
"lazuli" and pick it apart to see how things work.

  

  

1\) Copy the contents of the /template/lazuli folder to a new folder
such as:

  

/template/mytemplate

  

2\) Switch to use the template: "mytemplate" in OR's Site Config,
Template Tab

  

3\) Start making your changes to the 'main.html' and the CSS file
contained in your new /template/mytemplate folder.

  

The template file 'main.html' must be present at a minimum for a
template to work. This file contains most of the "look and feel"
elements for a design, and other template files make-up the content
displayed within it. The 'main.html' template file will almost always
contain the header, footer and navigation design and functional elements
of a site.  A visual representation  follows, everything overlaid in
blue is contained within 'main.html' for the lazuli template, the
interior that is not overlaid in blue is where the content is
dynamically loaded using the various template files. This is just one
example of how a template might be laid-out:

  

<img src="/assets/4a80790a25168762d044b58fd4dcc74ae6883071.png" class="libimg" width="545" height="631" />

  

  

A most simplistic 'main.html' template file layout:

&lt;html&gt;

&lt;head&gt;

&lt;/head&gt;

&lt;body&gt;

       &lt;div&gt;

               &lt;div id="header"&gt;&lt;/div&gt;

               &lt;div id="navigation"&gt;&lt;/div&gt;

               &lt;div id="content"&gt;{content}&lt;/div&gt;

               &lt;div id="right\_col"&gt;&lt;/div&gt;

               &lt;div id="footer"&gt;&lt;/div&gt;

       &lt;/div&gt;

&lt;/body&gt;

&lt;/html&gt;

  

Arrangement of the layout is up to you, just make sure you have the
{content} tag inserted in your design wherever you want OR's dynamic
content to be displayed.

  

If a site visitor (public) page you want to modify is not present in
your custom template folder, copy the associated template file from
/template/default. The default folder contains all of the available
template files and your custom template will automatically fall-back to
using one of them when not present in your custom template folder. Do
not make changes to files in the default template, or example template
folders, any changes will be overwritten when you upgrade OR in the
future.


