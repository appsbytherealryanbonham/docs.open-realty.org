# Upgrading/Updating Templates

Whenever selecting or purchasing a template, make sure it is designed
for the version of Open-Realty that you will be using. With the rapid
development of OR, new template tags and new template files are added
frequently. If you attempt to use a template from an older version, it
may or may not function properly due to incorrect or missing template
tags.

  

You may also need to update any CSS code as new CSS definitions may have
been added in order to handle new features of Open-Realty. If you are
unfamiliar with HTML and CSS markup, then you may need to seek help on
the forums, a HTML/CSS help website or from your template designer.

  

Review this OR template documentation and the change logs released via
the official OR Blog after new releases regularly for any changes to the
template tags and update any in your templates design as needed.  Files
located in the /template/default and /admin/template/default folders
will usually contain the latest tags as well as the example templates.

  