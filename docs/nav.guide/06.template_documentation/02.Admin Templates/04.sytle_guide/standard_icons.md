## Icon Guide

The following icons are used throughout Open-Realty Admin. Any core development should use these same icons for a consistent look. We recommend addons use the same icons as well where appropriate.

| Usage             | Icon Code                                   |
| ----------------- | ------------------------------------------- |
| Preview           | `<i class="fas fa-eye"></i>`                |
| Save              | `<i class="fas fa-save"></i>`               |
| Delete            | `<i class="fas fa-trash"></i>`              |
| Download          | `<i class="fas fa-file-download"></i>`      |
| Upload            | `<i class="fas fa-file-upload"></i>`        |
| Drag              | `<i class="fas fa-grip-lines"></i>`         |
| Edit              | `<i class="fas fa-pencil-alt"></i>`         |
| Link              | `<i class="fas fa-link"></i>`               |
| Email             | `<i class="fas fa-envelope"></i>`           |
| Checked/Completed | `<i class="far fa-check-circle"></i>`       |
| Info              | `<i class="fas fa-info-circle"></i>`        |
| Add-On            | `<i class="fas fa-puzzle-piece"></i>`       |
| Stats             | `<i class="fas fa-chart-line"></i>`         |
| Blog              | `<i class="fas fa-blog"></i>`               |
| Dashboard         | `<i class="fas fa-columns"></i>`            |
| Clear             | `<i class="fas fa-times"></i>`              |
| Settings          | `<i class="fas fa-cog"></i>`                |
| Password          | `<i class="fas fa-key"></i>`                |
| Goto Next         | `<i class="fas fa-angle-right"></i>`        |
| Goto Next 10      | `<i class="fas fa-angle-double-right"></i>` |
| Goto Prev         | `<i class="fas fa-angle-left"></i>`         |
| Goto Prev 10      | `<i class="fas fa-angle-double-left"></i>`  |
| Goto First        | `<i class="fas fa-step-backward"></i>`      |
| Goto Last         | `<i class="fas fa-step-forward"></i>`       |

### Admin Pages/Actions

The following Icons are used to represent different Admin Pages/Actions

| Action                | Icon                                |
| --------------------- | ----------------------------------- |
| edit_my_listings      | `<i class="fas fa-house-user"></i>` |
| edit_listing          | `<i class="fas fa-house"></i>`      |
| index                 | `<i class="fas fa-columns"></i>`    |
| edit_menu             | `<i class="fas fa-bars"></i>`       |
| edit_page             | `<i class="fas fa-file-alt"></i>`   |
| configure             | `<i class="fas fa-cog"></i>`        |
| edit_listing_template | `<i class="fas fatasks"></i>`       |
