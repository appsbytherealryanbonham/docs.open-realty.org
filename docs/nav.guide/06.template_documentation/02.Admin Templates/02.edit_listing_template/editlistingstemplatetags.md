# Edit Listing Template Tags

Usage: {tag\_name}.

  

The following tags are available for use within the following Edit
Listing administration template:

edit\_listing.html

  

|                                     |                                                                                                                                                                                                                                                                                                                                                                      |
|-------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Tag                                 | Description                                                                                                                                                                                                                                                                                                                                                          |
| listing\_title                      | This places the listing's title on the page.                                                                                                                                                                                                                                                                                                                         |
| listing\_field\_FIELDNAME           | by using the name of a listing field defined in the listing template editor, you can call the individual field and place it on the page. For example to call the "address" field you would insert {listing\_field\_address} into your template, which would result in both the field caption and value being placed in your template, e.g. "Address: 1600 Penn Ave." |
| listing\_field\_FIELDNAME\_value    | Works the same listing\_field\_FIELDNAME, except you only get the field value. e.g. "1600 Penn Ave."                                                                                                                                                                                                                                                                 |
| listing\_field\_FIELDNAME\_caption  | Works the same listing\_field\_FIELDNAME, except you only get the field caption. e.g. "Address"                                                                                                                                                                                                                                                                      |
| listing\_field\_FIELDNAME\_rawvalue | Works the same listing\_field\_FIELDNAME, except you only get the raw field value. For example when retrieving the price field instead of receiving "$150,000" you would simply get "150000" with no formatting.                                                                                                                                                     |
| listing\_agent\_last\_name          | Displays the listing Agent's last name.                                                                                                                                                                                                                                                                                                                              |
| listing\_agent\_first\_name         | Displays the listing Agent's first name.                                                                                                                                                                                                                                                                                                                             |
| listing\_creation\_date             | Displays the listing's creation date.                                                                                                                                                                                                                                                                                                                                |
| listing\_expiration                 | Displays the listing's expiration date. (Use with block; show\_expiration\_block to hide this if expiration is not used)                                                                                                                                                                                                                                             |
| listing\_last\_modified             | Displays the listing's last modified date                                                                                                                                                                                                                                                                                                                            |
| listing\_hit\_count                 | Returns the listing's hit count, i.e. the number of times it has been viewed.                                                                                                                                                                                                                                                                                        |


