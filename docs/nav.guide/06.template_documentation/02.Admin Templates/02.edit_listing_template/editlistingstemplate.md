# Edit Listing Template

(/admin/template/TEMPLATE\_NAME/FILENAME)

  

This template is used to control the layout of the Edit Listing page
which allows you to edit a specific listing.

  

Edit Listings Template File:

  

edit\_listing.html - This is the Edit Listings template. It has a number
of tags available to it, see the Edit\_Listing\_Template\_Tags page for
details.

  


