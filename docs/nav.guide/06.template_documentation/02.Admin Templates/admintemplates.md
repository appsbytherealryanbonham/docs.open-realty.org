# Admin Templates

/admin/template/TEMPLATE\_NAME

  

These templates control the layout of the administrative area of your
Open-Realty website where agents and admins log in to edit listings,
edit the site configuration and perform other tasks. Template tags from
the Main Admin Templates will work on any other administrative template
page. Other template pages may also have their own set of template
specific tags.


