# Listing Editor

(/admin/template/TEMPLATE\_NAME/FILENAME)

  

This template is used to control the layout of the Edit Listings page
which displays a paginated list of all listings that can be edited.

  

Edit Listings Template File:

  

listing\_editor.html - This is the Edit Listings template. It has a
number of tags available to it, see the Listing Editor Template Tags
page for details.

  


