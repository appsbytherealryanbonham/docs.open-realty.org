# Listing Editor Template Tags

Usage: {tag\_name}.

  

The following tags are available for use within the following Edit
Listings template:

listing\_editor.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_dataset</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>BEGINS the section for the display of each listing in the listing editor. This section of the template will be repeated for each listing. This section must be closed with {/listing_dataset}</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>listingid</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the listing ID on the listing view page.</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_title</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>This places the listing's title on the page.</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_pclass</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the name of the property class the listing is assigned to.</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_agent_last_name</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's last name.</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_agent_first_name</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's first name.</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_creation_date</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the listing's creation date.</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_expiration</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the listing's expiration date. (Use with block; show_expiration_block to hide this if expiration is not used)</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_notes</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the contents of the listing's notes field.</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_active_status</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>If the listing is active returns "Yes" otherwise returns "No".</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_featured_status</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>If the listing is featured returns "Yes" otherwise returns "No".</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>edit_listing_link</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Returns the url to edit the listing for use in a link,</p>
<p>example:</p>
<p>&lt;a href="{edit_listing_link}"&gt;&lt;img src="images/no_lang/listing_editor_edit.jpg" alt="{lang_admin_listings_editor_modify_listing}" width="16" height="16"&gt;&lt;/a&gt;</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>delete_listing_link</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Returns the url to delete the listing for use in a link.</p>
<p>example:</p>
<p>&lt;a href="{delete_listing_link}" onClick="return confirmDelete()"&gt;&lt;img src="images/no_lang/listing_editor_delete.jpg" alt="{lang_admin_listings_editor_delete_listing}" width="16" height="16"&gt;&lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_hit_count</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Returns the listing's hit count</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_creation_date</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the listing's creation date in the configured Open-Realty date format.</p></td>
</tr>
<tr class="odd">
<td width="300" style="border-color: #000000; border-style: solid"><p>listing_last_modified_date</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Displays the listing's modification date in the configured Open-Realty date format.</p></td>
</tr>
<tr class="even">
<td colspan="2" width="992" style="border-color: #000000; border-style: solid"><p>Blocks</p></td>
</tr>
<tr class="odd">
<td colspan="2" width="992" style="border-color: #000000; border-style: solid"><p>All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE_TAG} The following template tags are blocks:</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>show_expiration_block</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>will hide or show expiration date depending on the use expiration setting</p></td>
</tr>
<tr class="odd">
<td colspan="2" width="992" style="border-color: #000000; border-style: solid"><p>Other/Miscellaneous</p></td>
</tr>
<tr class="even">
<td width="300" style="border-color: #000000; border-style: solid"><p>row_num_even_odd</p></td>
<td width="681" style="border-color: #000000; border-style: solid"><p>Gets the currently rendering row number and returns it as 0 or 1. This is used, in combination with the stylesheet to alternate table row colors. Proper usage of this tag in your template would be:</p>
<p>&lt;tr class="result_row_{row_num_even_odd}"&gt;</p></td>
</tr>
</tbody>
</table>


