# Styling Dynamic Menus

[<img src="/assets/aac92f07c8c90fb92f7e76ac16d0582173077a0e.png" class="libimg" width="185" height="60" />](https://www.transparent-support.com/billing/cart.php?a=add&pid=52)

Menu System HTML IDs and Classes

  

OR's Menu Editor, available in the Pro edition,  will allow you to
easily create nested HTML unordered lists &lt;ul&gt;s that can be placed
for use as navigation menus for parts of your site without having to
edit the template files whenever you add new pages or make changes to a
menu.

  

The root level &lt;ul&gt; will have an ID of "or\_menu\_\#"  and a CSS
class "or\_menu". The \# denotes the numeric ID\# of the menu being
used, the menu ID is displayed in parenthesis () in the Menu Editor next
to the name you used when you created it.

<img src="/assets/55bd9d338d774d0e5d2df713d899e207f8bb3c56.jpg" class="libimg" width="328" height="176" />

  

The Menu Editor's OR Menu ID\#  in the above screen shot is 2. The
unordered list below was rendered to the browser by inserting the
{render\_menu\_2} template tag into one of the public template files,
typically 'main.html'.

  

Example generated{render\_menu\_2} menu:

&lt;ul class="or\_menu" id="or\_menu\_2"&gt;

  &lt;li class="or\_menu\_item or\_menu\_item\_type\_divider"
id="or\_menu\_item\_1"&gt;Main Menu&lt;/li&gt;

  &lt;li class="or\_menu\_item or\_menu\_item\_type\_action"
id="or\_menu\_item\_2"&gt;&lt;a href="/agents.html"&gt;View
Agents&lt;/a&gt;&lt;/li&gt;

  &lt;li class="or\_menu\_item or\_menu\_item\_type\_custom"
id="or\_menu\_item\_3"&gt;&lt;a href="/linkX.html"&gt;Link
X&lt;/a&gt;&lt;/li&gt;

  &lt;li class="or\_menu\_item or\_menu\_item\_type\_custom"
id="or\_menu\_item\_4"&gt;&lt;a href="/linkY.html"&gt;Link
Y&lt;/a&gt;&lt;/li&gt;

  &lt;li class="or\_menu\_item or\_menu\_item\_type\_custom"
id="or\_menu\_item\_5"&gt;&lt;a href="/linkZ.html"&gt;Link
Z&lt;/a&gt;&lt;/li&gt;

&lt;/ul&gt;

  

The first menu item above is a divider, followed by an action (View
Agents), and then 3 custom URL links. The content of the &lt;a&gt; tags
in the example have been greatly simplified so they better fit this
page, the Menu Editor will create longer, absolute links for instance.

  

All menu items will have been assigned the following class:
"or\_menu\_item" It will also be assigned one of the following classes
depending on the type of menu item that was selected when it was
created.

  

Action items will be assigned the CSS class:
or\_menu\_item\_type\_action

Page items will be assigned the CSS class: or\_menu\_item\_type\_page

Block Level items will be assigned the CSS class:
or\_menu\_item\_type\_block

Custom URL items will be assigned the CSS class:
or\_menu\_item\_type\_custom

Divider items will be assigned  the class: or\_menu\_item\_type\_divider

  

Each &lt;li&gt; menu item, with the exception of block level elements,
will also be assigned an ID of 'or\_menu\_item\_\#' where \# denotes the
individual item's numeric item ID assigned by the Menu Editor. These
ID\#s will not necessarily be consecutive (as in the example above), as
you add and delete items  from your menu this will create gaps in  the
assigned ID\#s

  

For any Menu Items that have child elements there will also be a class
of "or\_menu\_parent" assigned to the menu item.

example:

&lt;liclass="or\_menu\_item or\_menu\_parent
or\_menu\_item\_type\_divider"id="or\_menu\_item\_2"&gt;

  

All action, page, block, and custom url &lt;li&gt; elements will contain
an HTML &lt;a&gt;nchor tag , any custom CSS classes set on the menu item
via the Menu Editor will be applied to the &lt;a&gt; tag specifically.
 Divider Elements, do not contain an &lt;a&gt; tag and custom CSS
classes cannot be applied via the Menu Editor. If you wish to style the
divider items, you may target one using its specific OR menu item ID\#,
e.g. 'or\_menu\_item\_1'.

  

The HTML5 example template uses
<a href="http://users.tpg.com.au/j_birch/plugins/superfish/" class="rvts594">Superfish</a>,
a popular jQuery plugin library for converting HTML unordered lists like
the ones the OR Menu Editor can generate into interactive collapsible
parent-child menus. There are many 3rd party menu libs that will do
this, and you can also incorporate pure-CSS designs if you wish.

  

To use Superfish with a our custom menu ID\#2 example above, we will
need to add the following javascript to the &lt;HEAD&gt; of our
template's 'main.html' template file, anywhere after the {load\_js}
template tag. The following example assumes we have copied the
'superfish.js' jQuery plugin to a /lib/ subfolder of our  custom
template folder.

  

example:

&lt;scripttype="text/javascript"src="{template\_url}/lib/superfish.js"&gt;&lt;/script&gt;

  

&lt;scripttype="text/javascript"&gt;

       $(document).ready(function() {

               $('ul\#or\_menu\_2').superfish({

                       //options go here

               });

       });

&lt;/script&gt;

