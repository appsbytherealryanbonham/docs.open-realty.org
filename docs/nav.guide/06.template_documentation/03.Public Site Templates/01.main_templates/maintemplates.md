# Main Templates

/admin/template/TEMPLATE\_NAME/FILENAME

  

Tags in these templates will work in any of the public templates as well
as when placed in the WYSIWYG page editor generated pages.

  

1\. main.html- This is the template for the main site. This template
file is THE design for the majority of your website. The background,
header images, CSS styling, static content etc are all defined in this
file. It is simply a html file with the tag {content} to specify where
Open-Realty® should place it's information. When viewing your site
through Open-Realty® the {content} tag is replaced with information
defined by the sub template for the action you are trying to perform.
For example when viewing listings, the content area will be replaced
with content using the listing\_detail\_default.html file, or from other
template files or direct content from Open-Realty® depending on which
page of the site you are viewing.

  

2\. popup.html - This is the template used on popup windows such as the
Calculators and Agent Contact Forms. The template contains
&lt;head&gt;/&lt;body&gt;/&lt;html&gt; tags but is otherwise a blank
template. Use this template by setting '&popup' in the URL.

  

3\. blank.html - This is a completely blank and empty template file used
for popup windows by setting '&popup=blank'

  

4\. printer\_friendly.html - This is the template used when the user
selects the Printer Friendly Page from listing details.

  

------------------------------------------------------------------------

  

Optional

  

5\. page\#\_main.html - This is an optional main template that is based
on the page ID\# of pages created in the Page Editor. If you wish to
have a unique template design for a specific page such as having
featured listings in a section of the template on the home page (Page 1)
but not the rest of the site then you can create a page\#\_main.html
template with the differences you want.

  

Open-Realty will use the page\#\_main.html template for pages where the
alternate main template exists, if there is no specific main template
for that page then it will default to the main.html template for that
page. (\# would be the page number you want to use this main template
for)

  

You can specify a PageID=\# variable in the URL for any page on your
site in order to specify an alternate main template file for the page
you are displaying. The PageID=\# variable is specified automatically
for all page editor created pages, you can create a custom PageID for
any other page you wish to have an alternate main template layout for.

  

6\. searchresults\_main.html - This is an optional main template that
allows you to load a unique template for your search results page. This
can be used to define custom metatags for your search result pages or to
simply give your search results a different look from the main template.

  

7\. maintenance\_mode.html - This template is used and displayed to site
visitors when OR is set to be in "Maintenance Mode". (See the
Maintenance Mode option in the Site Config
[General tab](../../../05.administration/13.site_configuration/general.md))

  

8\. 500.shtml - This is not a file that is used or required by
Open-Realty. This optional file is for creating a custom 500 Internal
Server error message page that your web server will display to the site
visitor if there is a database or other internal server error instead of
displaying the SQL or other errors on the page. This is used for
security and aesthetic purposes.

