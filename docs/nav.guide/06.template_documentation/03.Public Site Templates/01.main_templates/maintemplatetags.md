# Main Template Tags

Usage: {tag\_name}.

  

The following tags are available for use within the following main
template files:

main.html

popup.html

blank.html

printer\_friendly.html

page\#\_main.html

searchresults\_main.html

maintenance\_mode.html

<table width="100%" data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>addthis_button</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places an "AddThis" button on your site where the template tag is specified. AddThis supports 150+ services such as Digg, Stumbleupon, Facebook, MySpace, Twitter and 100's of other services. This loads external javascript from the AddThis site to function.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>templated_search_form</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>This renders the templated search form onto your main template page or within a Blog or Page editor page.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="387" style="border-color: #000000; border-style: solid"><p>templated_search_form_XX</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Renders a listing search form (as above) but for for a specific property class where XX is the numeric Property class ID# (pclass_id).</p>
<p>e.g.:</p>
<p>{templated_search_form_1} &lt;- Renders a search form for property class ID#1 (Home)</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>baseurl</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Renders the base URL where Open-Realty is installed, i.e. the root of your Open-Realty site.</p>
<p>e.g.</p>
<p>http://www.mywebsite.com</p>
<p>or, if installed in a folder:</p>
<p>http://www.mywebsite.com/foldername</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="387" style="border-color: #000000; border-style: solid"><p>curley_open</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Renders an opening curly brace character {. Useful if you need to display actual curly braces and not have them interpreted by the template engine as template tag boundaries.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="387" style="border-color: #000000; border-style: solid"><p>curley_close</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Renders a closing curly brace character }. Useful if you need to display actual curly braces and not have them interpreted by the template engine as template tag boundaries.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_index</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL for the Index Page into the template.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_search</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to the Search Page into the template.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_search_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to the search page for the specficed numeric property class ID into the template.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_search_results</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to the Search Results page into the template. This will be to display ALL listings</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_searchresults_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to the search results page for property class # into the template.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_view_agents</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to view the agent's into the template.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_view_calculator</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to view the calculator into the template.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_view_favorites</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to View a member's Favorite listings. This should be used inside of the check member permission tags.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_view_saved_searches</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL to view a member's Saved Searches. This should be used inside of the check member permission tags.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_logout</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL for a member to LOGOUT of the site. This should be used inside of the check member permission tags.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_member_signup</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL for a visitor to sign up as a member to the site. This should be used inside of the check guest permission tags.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_member_login</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL for a visitor to LOGIN to the site as a member. This should be used inside of the check guest permission tags.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_agent_signup</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL for a visitor to sign up as an agent on the site. This should be used inside of the check guest permission tags.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_agent_login</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL for an agent to LOGIN to the site as an agent. This should be used inside of the check guest permission tags.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>url_blog</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the URL for the Blog Index on the site. The blog index displays a list of all the blog posts</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>page_link_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places a link to the specified page # into the template. The page number is provided when editing a page with the Page Editor.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>blog_link_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places a link to the specified blog post # into the template. The blog post # is provided when editing a blog post in the Blog Manager.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>featured_listings_vertical</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the featured listings in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>featured_listings_horizontal</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the featured listings in a horizontal layout.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>random_listings_vertical</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts random listings in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>random_listings_horizontal</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts random listings in a horizontal layout.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>latest_listings_vertical</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the latest listings in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>latest_listings_horizontal</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the latest listings in a horizontal layout.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>popular_listings_vertical</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the popular(most viewed) listings in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>popular_listings_horizontal</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the popular(most viewed) listings in a horizontal layout.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>featured_listings_vertical_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the featured listings for property class # in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>featured_listings_horizontal_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the featured listings for property class # in a horizontal layout.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>random_listings_vertical_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts random listings for property class # in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>random_listings_horizontal_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts random listings for property class # in a horizontal layout.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>latest_listings_vertical_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the latest listings for property class # in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>latest_listings_horizontal_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the latest listings for property class # in a horizontal layout.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>popular_listings_vertical_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the popular(most viewed) listings for property class # in a vertical layout.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>popular_listings_horizontal_class_#</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Inserts the popular(most viewed) listings for property class # in a horizontal layout.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="387" height="95" data-valign="middle" style="border-color: #000000; border-style: solid"><p>load_js</p></td>
<td width="691" height="95" style="border-color: #000000; border-style: solid"><p>*deprecated as of OR v3.12.10*</p>
<p>Places JavaScript into the template. This loads javascript defined using the $load_js variable and can be used throughout Open-Realty® and Add-ons. This should be placed near the top of your main template between the &lt;head&gt; and &lt;/head&gt; tags.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="387" data-valign="middle" style="border-color: #000000; border-style: solid"><p>load_ORjs</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places OR dependency and add-on JavaScript into the template. This loads javascript defined using the $load_js variable and can be used throughout Open-Realty® and properly crafted add-ons. This should be placed near the top of your main template between the &lt;head&gt; and &lt;/head&gt; tags. REPLACES {load_js} as of OR v3.2.10</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="387" data-valign="middle" style="border-color: #000000; border-style: solid"><p>load_js_last</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places JavaScript into the template. This loads javascript defined using the $load_js_last variable and can be used throughout Open-Realty and its add-ons. This should be placed at the end of your main.html template file just before the &lt;/body&gt; tag.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>load_meta_keywords</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>This places the meta keywords tag and values into your template. The Meta keywords help with search engine ranking. This tag should be placed inside the head tag of your main.html template.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>load_meta_description</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>This places the meta description tag and values into your template. The Meta description helps with search engine ranking. This tag should be placed inside the head tag of your main.html template.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>load_meta_keywords_raw</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>This places the raw meta keywords values into your template. The Meta keywords help with search engine ranking. This tag should be placed inside the head tag of your main.html template. Proper usage of this tag would be within the meta keywords tag for example:</p>
<p>&lt;meta name="keywords"content="{load_meta_keywords_raw}"/&gt;</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>load_meta_description_raw</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>This places the raw meta description values into your template. The Meta description helps with search engine ranking. This tag should be placed inside the head tag of your main.html template. Proper usage of this tag would be within the meta description tag for example:</p>
<p>&lt;meta name="description"content="{load_meta_description_raw}"/&gt;</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>template_url</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the currently selected template url into the template. Very useful for creating absolute  URLs for loading images, CSS and JS files from within your template folder.</p>
<p>e.g.: assumes Lazuli template is set:</p>
<p>&lt;img src="{template_url}/images/photo.jpg"/&gt;</p>
<p>Returns:</p>
<p>&lt;img src="http://www.mysite.com/template/lazuli/images/photo.jpg"/&gt;</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>content</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Renders the page content generated by Open-Realty into the template.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>site_title</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the site title set via Open-Realty's Site Config.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>company_name</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Displays the company name set in the Open-Realty <a href="../../../05.administration/13.site_configuration/general.html">Site Configuration</a>.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>company_location</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Renders the company location set via Site Config.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>company_logo</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Renders the URL for the company logo set via Site Config.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>lang_VARIABLENAME</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places the specified language variable from your language file(s) into the template. For example using {lang_featured_listings} would render the contents of the language variable: $lang['featured_listings'] which is in this case: "Featured Listings"</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>load_css_FILENAME</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places a link to load your CSS file(s) into your template. The CSS file must be located in your presently selected template directory alongside  'main.html' and it must have a .css extension. This tag should be placed inside your template's HTML  &lt;head&gt;&lt;/head&gt; tags.</p>
<p>Example: For loading the CSS file: 'mycss.css'  use: {load_css_mycss)</p>
<p>note: You do not include the .css extension in the tag, it is implied.</p>
<p>CSS files loaded via this method may contain {basepath} and {baseurl} template tags.which will be parsed and rendered as usual.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>charset</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Adds the charset selected in site config to your template. This tag should be placed inside your template's HTML &lt;head&gt; &lt;/head&gt; tags.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>template_select</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Displays the template selection dropdown box.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>rss_featured</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Generates a URL to the Featured Listings RSS feed. URL will be generated as SEF if SEO is enabled in Site Config</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>rss_lastmodified</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Generates a URL to the Last Modified Listings RSS feed. URL will be generated as SEF if SEO is enabled in Site Config.</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>link_calc</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Places a link to the calculators.</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>active_listing_count</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Displays the total number of active listings for all classes</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>active_listing_count_pclass_X</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Displays the total number of active listings for the single property class where X is the class ID</p>
<p>Example for 'Home':</p>
<p>(active_listing_count_pclass_1}</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>link_calc_url</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the calculators. This can be used to create your own link to the calculators using HTML.</p>
<p>example:</p>
<p>&lt;a href="{link_calc_url}"&gt;Calculator Link&lt;/a&gt;</p>
<p>(The raw html address does not contain ANY popup, size or other code found in the normal link tag)</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>listing_stat_FUNCTION_field_FIELDNAME_value</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Calculates and displays statistical information for any numeric field for all listings that contain that field. Requires 2 variables: FUNCTION and FIELDNAME. FUNCTION must be one of the following:</p>
<p>average</p>
<p>min  (lowest value)</p>
<p>max (highest value)</p>
<p>median</p>
<p>example for average price:</p>
<p>{listing_stat_average_field_price_value}</p></td>
</tr>
<tr class="odd">
<td width="387" style="border-color: #000000; border-style: solid"><p>listing_stat_FUNCTION_field_FIELDNAME_value_pclass_X</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Same as {listing_stat_FUNCTION_field_FIELDNAME_value} above, but the results are restricted to the property class defined. X =  the numeric property class ID you wish to return results.</p>
<p>example for average price for the 'Home' property class :</p>
<p>{listing_stat_average_field_price_value_pclass_1}</p></td>
</tr>
<tr class="even">
<td width="387" style="border-color: #000000; border-style: solid"><p>mobile_full_template_link</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Provides a link for switching between the mobile or regular public template. This will link to the opposite  template that the user is currently viewing, e.g: if a visitor is viewing the mobile version of the site, this will link to the full site and vice-versa.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="387" style="border-color: #000000; border-style: solid"><p><br />
</p>
<p>getvar_XXX</p></td>
<td width="691" style="border-color: #000000; border-style: solid"><p>Obtains and renders the value of a specific $_GET variable (if present) where XXX denotes the $_GET variable to query.</p>
<p><br />
</p>
<p>example for use with a search results template to obtain the value of the"city" $_GET variable used in the URL:</p>
<p>URL of search results page:</p>
<p>http://www.yoursite.com/index.php?action=searchresults&amp;city=scranton&amp;pclass[]=1</p>
<p><br />
</p>
<p>Template tag: {getvar_city} displays the text: scranton</p>
<p><br />
</p>
<p>If the $_GET variable is an array, (such as pclass[]) only the first value of the array will be returned;</p>
<p>http://www.yoursite.com/index.php?action=searchresults&amp;city=scranton&amp;pclass[]=1&amp;pclass[]=4</p>
<p><br />
</p>
<p>Template tag: {getvar_pclass} displays: 1, the first value in the pclass array</p></td>
</tr>
</tbody>
</table>

  

<table width="100%" data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="2" width="1026" style="border-color: #000000; border-style: solid"><p>Block</p></td>
</tr>
<tr class="even">
<td colspan="2" width="1026" style="border-color: #000000; border-style: solid"><p>All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE_TAG} The following template tags are blocks:</p></td>
</tr>
<tr class="odd">
<td width="259" style="border-color: #000000; border-style: solid"><p>pclass_names_ul</p></td>
<td width="743" style="border-color: #000000; border-style: solid"><p>Creates an HTML unordered list &lt;ul&gt; that contains the names of each property class.</p></td>
</tr>
<tr class="even">
<td width="259" style="border-color: #000000; border-style: solid"><p>pclass_names_searchlinks</p></td>
<td width="743" style="border-color: #000000; border-style: solid"><p>Creates an HTML unordered list &lt;ul&gt; that contains the links to to the search page and the names for each property class.</p></td>
</tr>
<tr class="odd">
<td width="259" style="border-color: #000000; border-style: solid"><p>listings_custom_search_result_block</p></td>
<td width="743" style="border-color: #000000; border-style: solid"><p>Allows you to perform a search and return listings based on your criteria to main.html and any Blog or Page Editor page.</p>
<p>The {listings_custom_search_result_block}  tag allows you to pass the following options:</p>
<p><br />
</p>
<p>$limit - The max number of listings to display. If left empty this defaults to 10. 0= no limit</p>
<p><br />
</p>
<p>$args - Search options you want to pass. This should be a string of search parameters.</p>
<p>       example: To retrieve featured listings you would use:</p>
<p>               $args="featured=yes"</p>
<p>       example2: To retrieve featured listings from only the city of Scranton you would use:</p>
<p>               $args="featured=yes&amp;city=Scranton"</p>
<p><br />
</p>
<p>$sortby - Fields (comma seperated list) to sort the search results by. If left empty this defaults</p>
<p>       to sort the results randomly.</p>
<p>       example: To sort the listings by the number of beds and then by price do:</p>
<p>               $sort="beds,price"</p>
<p><br />
</p>
<p>$sorttype - Direction to sort the listings. This should be a comma separated list of equal</p>
<p>       length as the $sortby paramater.</p>
<p>       example: $sorttype="DESC,ASC"</p>
<p><br />
</p>
<p>Example that will return 10 random listings</p>
<p>{listings_custom_search_result_block}</p>
<p>&lt;h4&gt;{listing_title}&lt;/h4&gt;</p>
<p>&lt;a href="{fulllink_to_listing}"&gt;&lt;img src="{raw_image_thumb_1}"&gt;&lt;/a&gt;</p>
<p>&lt;br/&gt;</p>
<p>  {listing_field_price_caption}: {listing_field_price_value}</p>
<p>{/listings_custom_search_result_block}</p>
<p>       </p>
<p>Example that uses options and will return 5 featured listings from the city of 'Scranton', sorted by price</p>
<p>{listings_custom_search_result_block $limit="5" $args="featuredOnly=yes&amp;city=Scranton" $sortby="price" $sorttype="DESC"}</p>
<p>&lt;h4&gt;{listing_title}&lt;/h4&gt;</p>
<p>&lt;a href="{fulllink_to_listing}"&gt;&lt;img src="{raw_image_thumb_1}"&gt;&lt;/a&gt;</p>
<p>&lt;br/&gt;</p>
<p>  {listing_field_price_caption}: {listing_field_price_value}</p>
<p>{/listings_custom_search_result_block}</p></td>
</tr>
<tr class="even">
<td width="259" style="border-color: #000000; border-style: solid"><p>custom_search_has_result</p></td>
<td width="743" style="border-color: #000000; border-style: solid"><p>Refinement for the {listings_custom_search_result_block} tag above. Only the text or tags that are within this block will be repeated for each listing, and only if there is a valid result to the search. Useful for excluding items from iteration.</p>
<p>Example:</p>
<p>{listings_custom_search_result_block $limit="5" $args="featuredOnly=yes"}</p>
<p>&lt;h1&gt;Featured Listings&lt;/h1&gt;&lt;!-- This line displayed only once --&gt;</p>
<p>  {custom_search_has_result}</p>
<p>&lt;h4&gt;{listing_title}&lt;/h4&gt;</p>
<p>&lt;ahref="{fulllink_to_listing}"&gt;&lt;img src="{raw_image_thumb_1}"&gt;&lt;/a&gt;</p>
<p>&lt;br/&gt;</p>
<p>    {listing_field_price_caption}: {listing_field_price_value}</p>
<p>  {/custom_search_has_result}</p>
<p>{/listings_custom_search_result_block}</p></td>
</tr>
<tr class="odd">
<td width="259" style="border-color: #000000; border-style: solid"><p>foreach_pclass_block</p></td>
<td width="743" style="border-color: #000000; border-style: solid"><p>Content placed within {foreach_pclass_block} {/foreach_pclass_block} will be repeated (iterated) once for each property class. Especially useful template tags for use within this block are:</p>
<p>{pclass_id}</p>
<p>{pclass_name}</p>
<p>Any tag that requires a property class ID can be used effectively.</p>
<p>This block tag can be useful for creating tabbed or other navigation content that uses unordered lists &lt;ul&gt; or nested &lt;div&gt;s.</p>
<p><br />
</p>
<p>Example for creating a tabbed navigation section using j.</p>
<p>&lt;!-- the tabs --&gt;</p>
<p>&lt;ulclass="tabs"&gt;</p>
<p>  {foreach_pclass_block}</p>
<p>&lt;li&gt;&lt;a href="#" id="tab_{pclass_id}"&gt;{pclass_name}&lt;/a&gt;&lt;/li&gt;</p>
<p>  {/foreach_pclass_block}</p>
<p>&lt;/ul&gt;</p>
<p><br />
</p>
<p>&lt;!-- tab "panes" --&gt;</p>
<p>&lt;div class="panes"&gt;</p>
<p>  {foreach_pclass_block}        </p>
<p>&lt;divid="pane_data_{pclass_id}"&gt;</p>
<p>      This content is for class: {pclass_name}</p>
<p>&lt;/div&gt;</p>
<p>  {/foreach_pclass_block}</p>
<p>&lt;/div&gt;</p>
<p>               </p>
<p>&lt;script type="text/javascript"&gt;</p>
<p>  $(document).ready(function() {</p>
<p>    $("ul.tabs").tabs("div.panes &gt; div");</p>
<p>  });</p>
<p>&lt;/script&gt;</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="259" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agents_custom_search_result_block</p></td>
<td width="743" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Content placed within {agents_custom_search_result_block} {/agents_custom_search_result_block} tags will be repeated (iterated) once for each active Agent in OR, sorted by their defined Agent Order (rank) unless otherwise specified. The Admin user (user ID#1) is a special case and will only be present in the results if "Show Admin on Agent list" is set to 'yes' in Site Config.</p>
<p><br />
</p>
<p>example for creating a select form element of all Active Agents</p>
<p>&lt;select class="myclass" name="selectname"&gt;</p>
<p> {agents_custom_search_result_block $args="userdb_is_agent=yes&amp;userdb_active=yes" $sortby="userdb_rank" $sorttype="ASC" $limit="0"}</p>
<p>   &lt;option value="{user_id}"&gt;{user_first_name} {user_last_name}&lt;/option&gt;</p>
<p> {/agents_custom_search_result_block}</p>
<p>&lt;/select&gt;</p>
<p><br />
</p>
<p>The following Agent data template tags are supported.:</p>
<p>agent_id - user ID#</p>
<p>agent_first_name - The Agent's First Name</p>
<p>agent_last_name - The Agent's Last Name</p>
<p>agent_email - The Agent's email address</p>
<p>agent_link - Text URL of the Agent's detail page</p>
<p>agent_contact_link - Clickable link to the Agent's Contact form</p>
<p>agent_qr_code_link - Text URL linking to the Agent's qrcode image</p>
<p>agent_field_FIELDNAME_caption - Renders the field Caption  of the Agent field denoted by FIELDNAME</p>
<p>agent_field_FIELDNAME_value - Renders the field value of the Agent field denoted by FIELDNAME</p>
<p>agent_field_FIELDNAME_rawvalue - Renders the raw field value of the Agent field denoted by FIELDNAME</p>
<p><br />
</p>
<p>The following Agent photo template tags are supported:</p>
<p>(# below denotes the numeric order of the photo, 1 = first, 2=second e.g. {agent_image_full_1} )</p>
<p>agent_image_full_# - Full size photo of the Agent rendered as an HTML &lt;img&gt;</p>
<p>raw_agent_image_full_# - Text URL of the full size photo.</p>
<p>agent_image_thumb_# - Thumbnail photo of the Agent rendered as an HTML &lt;img&gt;</p>
<p>raw_agent_image_thumb_# - Text URL of the thumbnail photo.<br />
<br />
</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="259" data-valign="middle" style="border-color: #000000; border-style: solid"><p>blog_custom_search_result_block</p></td>
<td width="743" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Content placed within {blog_custom_search_result_block} {/blog_custom_search_result_block} tags will be repeated (iterated) once for each Blog Article using the sorting and etc you specify.</p>
<p><br />
</p>
<p>example for creating a select form element of status: live blogs belonging to Agent ID #4, sorted by date</p>
<p>&lt;select class="myclass" name="selectname"&gt;</p>
<p> {blog_custom_search_result_block $args="blogmain_published=1&amp;userdb_id=4" $sortby="blogmain_date" $sorttype="DESC" $limit="0"}</p>
<p>   &lt;option value="{blog_id}"&gt;{blog_title}&lt;/option&gt;</p>
<p> {/blog_custom_search_result_block}</p>
<p>&lt;/select&gt;</p>
<p><br />
</p>
<p>The following Blog template tags are supported.:</p>
<p>blog_id - The unique Blog article ID#</p>
<p>blog_title - The Blog article Title</p>
<p>blog_author - The Blog's article's Author</p>
<p>blog_date_posted - The date the Article was posted</p>
<p>blog_comment_count - The number of comments on the article</p>
<p>blog_url - Clickable link to the Blog article</p>
<p>blog_summary - Renders a summary of the Blog article</p>
<p>blog_full_article - Renders the full Blog article contents.</p>
<p>blog_post_tags - Renders an unordered list of one or more links to each assigned Blog tag. The &lt;ul&gt; container has a class of: btags, each &lt;a&gt;nchor tag contains a unique id equal to: btag__+ the tag_id (e.g.: btag_42) and is additionally assigned a class of: btags_link</p>
<p><br />
</p>
<p>&lt;ul class="btags"&gt;</p>
<p> &lt;li&gt;&lt;a id="btag_42" class="btags_link"  href="#"&gt;TAGNAME&lt;/a&gt;&lt;/li&gt;</p>
<p>&lt;/ul&gt;</p>
<p><br />
</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="259" data-valign="middle" style="border-color: #000000; border-style: solid"><p>if_addon_XX</p></td>
<td width="743" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Displays content stored within this block is a specific addon (denoted by XX) is installed.</p>
<p>e.g.:</p>
<p>{if_addon_transparentmaps}</p>
<p>   Display this content if TMAPS is installed</p>
<p>{/if_addon_transparentmaps}</p></td>
</tr>
</tbody>
</table>

