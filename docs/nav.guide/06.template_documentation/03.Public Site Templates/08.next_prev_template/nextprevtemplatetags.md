# Next/Prev Template Tags

Usage:  {tag\_name}

  

The following tags are available for use within the following Next/Prev
template files:

next\_prev.html

next\_prev\_bottom

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="267" style="border-color: #000000; border-style: solid"><p>lang_VARIABLE</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the corresponding lang variable from the Open-Realty® lang file.</p></td>
</tr>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_num_rows</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>The number of search results (search result rows)</p></td>
</tr>
<tr class="even">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_page_type</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the lang variable for "next_prev_listing".</p></td>
</tr>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_meet_your_search</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the lang variable for "listings_meet_your_search".</p></td>
</tr>
<tr class="even">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_listing_num_min</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the beginning number of the range of listings that are being displayed on the search results.</p></td>
</tr>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_listing_num_max</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the ending number of the range of listings that are being displayed on the search results.</p></td>
</tr>
<tr class="even">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_guidestring_no_action</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the search string without the GET action or session id for use in a link to save searches or refine a search.</p>
<p>Example:</p>
<p>&lt;a href="index.php?action=save_search{nextprev_guidestring_no_action}" &gt;{lang_save_this_search}&lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_guidestring</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the full search string with the GET action for use in a link to additional pages of the search results.</p>
<p>Example:</p>
<p>&lt;a href="index.php?cur_page={nextprev_prevpage}{nextprev_guidestring}" class="bt_pages"&gt;  &lt;&lt;  &lt;/a&gt;</p></td>
</tr>
<tr class="even">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_prevpage</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the number of the previous page of the search results, can be used in a link to the previous page of the search results.</p>
<p>Example:</p>
<p>&lt;a href="index.php?cur_page={nextprev_prevpage}{nextprev_guidestring}" class="bt_pages"&gt;  &lt;&lt;  &lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_disp_count</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the number of the page out of the block of 10 pages that we are currently viewing.</p></td>
</tr>
<tr class="even">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_count</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the number of the page out of the block of 10 pages that we are currently viewing. This is for use in a link to the page you want to go to. The link must also include the {nextprev_guidestring}.</p>
<p>Example:</p>
<p>&lt;a href="index.php?cur_page={nextprev_count}{nextprev_guidestring}" class="bt_pages"&gt;{nextprev_disp_count}&lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_nextpage</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the number of the next page of the search results, can be used in a link to the next page of the search results.</p>
<p>Example:</p>
<p>&lt;a href="index.php?cur_page={nextprev_nextpage}{nextprev_guidestring}" class="bt_pages"&gt;  &gt;&gt;  &lt;/a&gt;</p></td>
</tr>
<tr class="even">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_next10page</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the number of the first page of the next group of 10 pages, can be used in a link to the next 10 pages (next 100 listings).</p>
<p>Example:</p>
<p>&lt;a href="index.php?cur_page={nextprev_next10page}{nextprev_guidestring}" &gt;{lang_next_100}&lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="267" style="border-color: #000000; border-style: solid"><p>nextprev_prev10page</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Displays the number of the last page of the previous group of 10 pages, can be used in a link to the previous 10 pages (next 100 listings).</p>
<p>Example:</p>
<p>&lt;a href="index.php?cur_page={nextprev_prev10page}{nextprev_guidestring}" &gt;{lang_previous_100}&lt;/a&gt;</p></td>
</tr>
</tbody>
</table>

  

  

|                                                                                                                                                                                                                                                                                                                                                                                                                                                  |                                                                                                                                                                                                                                                                                                          |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Blocks                                                                                                                                                                                                                                                                                                                                                                                                                                           |                                                                                                                                                                                                                                                                                                          |
| Template tag Blocks are template tags that will have a starting and ending tag. ALL Code inside a block will be displayed or hidden depending on if the block's conditions are met. Starting tags will be listed below. Blocks must be closed with {/TEMPLATE\_TAG\_block}. Several of the blocks have a negative switch (!) that will have the opposite condition than its non-negative version. The following template tags are block options: |                                                                                                                                                                                                                                                                                                          |
| nextprev\_num\_of\_rows\_is\_1\_block                                                                                                                                                                                                                                                                                                                                                                                                            | Block that will be displayed IF there is only one search result row being displayed.                                                                                                                                                                                                                     |
| !nextprev\_num\_of\_rows\_is\_1\_block                                                                                                                                                                                                                                                                                                                                                                                                           | Block that will be displayed IF there more than one search result row being displayed.                                                                                                                                                                                                                   |
| nextprev\_show\_save\_search\_block                                                                                                                                                                                                                                                                                                                                                                                                              | Block that will be displayed IF this is the search results page to display the save search option. This will not be displayed on other pages that use the next/prev function such as the site log.                                                                                                       |
| nextprev\_show\_refine\_search\_block                                                                                                                                                                                                                                                                                                                                                                                                            | Block that will be displayed IF this is the search results page to display the refine search option. This will not be displayed on other pages that use the next/prev function such as the site log.                                                                                                     |
| nextprev\_is\_firstpage\_block                                                                                                                                                                                                                                                                                                                                                                                                                   | Block that will be displayed IF this is the first page of the search results.                                                                                                                                                                                                                            |
| !nextprev\_is\_firstpage\_block                                                                                                                                                                                                                                                                                                                                                                                                                  | Block that will be displayed IF this is NOT the first page of the search results.                                                                                                                                                                                                                        |
| nextprev\_page\_section                                                                                                                                                                                                                                                                                                                                                                                                                          | Block for the 10 page result set selection. Items in this block will be repeated ten times to create the 10 pages of the search results. There are two blocks that are used inside of this block: nextprev\_page\_current\_block and nextprev\_page\_other\_block, see below for details of these blocks |
| nextprev\_page\_current\_block                                                                                                                                                                                                                                                                                                                                                                                                                   | Block that is displayed in the {nextprev\_page\_section} block if, the page number of the search results is the one that we are currently viewing. This is used with the normal template tag, {nextprev\_disp\_count} to maintain the count and display the count of the page we are on.                 |
| nextprev\_page\_other\_block                                                                                                                                                                                                                                                                                                                                                                                                                     | Block that is displayed in the {nextprev\_page\_section} block if, the page number of the search results is NOT the one that we are currently viewing. This is used with the normal template tag, {nextprev\_disp\_count} to maintain the count and display the count of the page we are on.             |
| nextprev\_lastpage\_block                                                                                                                                                                                                                                                                                                                                                                                                                        | Block that will be displayed IF this is the last page of the search results.                                                                                                                                                                                                                             |
| !nextprev\_lastpage\_block                                                                                                                                                                                                                                                                                                                                                                                                                       | Block that will be displayed IF this is NOT the last page of the search results.                                                                                                                                                                                                                         |
| nextprev\_prev\_100\_button\_block                                                                                                                                                                                                                                                                                                                                                                                                               | Block that will be displayed IF we are on a page past the first 10 pages.                                                                                                                                                                                                                                |
| !nextprev\_prev\_100\_button\_block                                                                                                                                                                                                                                                                                                                                                                                                              | Block that will be displayed IF we are NOT on a page past the first 10 pages.                                                                                                                                                                                                                            |
| nextprev\_next\_100\_button\_block                                                                                                                                                                                                                                                                                                                                                                                                               | Block that will be displayed IF there are more than 10 pages left to be displayed.                                                                                                                                                                                                                       |
| !nextprev\_next\_100\_button\_block                                                                                                                                                                                                                                                                                                                                                                                                              | Block that will be displayed IF there are NOT more than 10 pages left to be displayed.                                                                                                                                                                                                                   |
| nextprev\_clearlog\_block                                                                                                                                                                                                                                                                                                                                                                                                                        | Block that will be displayed IF this is the Admin View Log page and if the user is an admin.                                                                                                                                                                                                             |


