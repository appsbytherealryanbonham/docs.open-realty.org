# Next/Prev Template

/template/TEMPLATE\_NAME/FILENAME

  

This template is used to control the layout of the Next/Prev section of
search results and Listing Details page. The Next/Prev template is not
configurable, the site will display one of the two templates depending
on the template tag used to call the next/prev function:

  

1\. next\_prev.html - This is the next/prev template used for navigating
paginated content such as search results. This is rendered using the
template tag: {next\_prev}

2\. next\_prev\_bottom.html - This is the next/prev "bottom" template.
It is a reduced-functionality version of next\_prev for situations where
you may want supplemental navigation on paginated pages (perhaps at the
bottom?). This is rendered using the template tag: {next\_prev\_bottom}


