# Contact Agent Templates

/template/TEMPLATE\_NAME/FILENAME

  

The following templates are used to control the layout of the Contact
Agent form.

  

1\. contact\_agent\_free.html - Contact Agent form template for OR free
edition (default).

2\. contact\_agent\_pro.html - Contact Agent form template for OR Pro
edition.

  

Which template is used is selectable via Site Config.

  

Most of the form content within the contact forms are created
dynamically by OR, based upon fields you have created in the Lead Form
Editor. The OR free edition version of the contact form is set by
default when you install OR.

  

The template tag: {contact\_agent\_link} will generate a link to the
Contact Agent form when placed within any of the
listing\_detail\_X.htmltemplates:

  

The template tag: {agent\_contact\_link} will generate a link to the
Contact Agent form when placed within the view\_users\_default.html
 (View Agents) template file within the appropriate block tags:

  

The template tag: {user\_contact\_link} will generate a link to the
Contact Agent form when placed within the view\_user\_default.html (View
Agent) template file:

  

Note: OR's contact forms require that the user be logged in to either an
Agent or Member account before they can fill out the form.

  

Captcha image verification is only displayed and needs to be solved once
if the user is not logged in.

