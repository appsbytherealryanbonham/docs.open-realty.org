# Contact Agent Template Tags

Usage: {tag\_name}.

  

The following tags are available for use within the following  search
page template files:

contact\_agent\_free.html

contact\_agent\_pro.html

  

<table width="100%" data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd" data-valign="top">
<td width="267" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>lang_VARIABLE</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Displays the corresponding lang variable from the Open-Realty® lang file.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_id</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders or displays the current OR Listing ID when the form is called using a valid listing_id $_GET variable, usually via the listing detail page.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_id</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders or displays the current OR Agent ID when the form is called using a valid agent_id $_GET variable, usually via the View Agent or View Agents pages.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_title</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders or displays the Listing Title when the form is called using a valid listing_id $_GET variable, usually via the listing detail page.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_agent_first_name</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the recipient Agent's first name</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_agent_last_name</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the recipient Agent's last name</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_first_name</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's First name once they are logged in.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_last_name</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's Last name once they are logged in.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>feedback_formelements</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the form elements created via the Lead Form Editor. Required if you intend to collect information other than the user's name and email or the listing ID they were inquiring about.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="267" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>captcha_display</p></td>
<td width="751" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Displays the captcha image verification set in Site Config. Required.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="267" data-valign="middle" style="border-color: #000000; border-style: solid"><p>extra_url</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Required for the contact form's &lt;form&gt; action URL</p>
<p>e.g.:</p>
<p>&lt;form name="add_feedback" id="add_feedback"action="index.php?action=contact_agent{extra_url}" method="post"&gt;</p></td>
</tr>
</tbody>
</table>

  

  

<table width="100%" data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd" data-valign="top">
<td colspan="2" width="1028" style="border-color: #000000; border-style: solid"><p>Block Tags</p></td>
</tr>
<tr class="even" data-valign="top">
<td colspan="2" width="1028" style="border-color: #000000; border-style: solid"><p>Template tag Blocks are template tags that  have a starting and ending tag. ALL code inside a block will be displayed or hidden depending on if the block's conditions are met. Starting tags will be listed below. Blocks must be closed with {/TEMPLATE_TAG_block}.  The following template tags are block options:</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="303" style="border-color: #000000; border-style: solid"><p>listing_contact_block</p></td>
<td width="719" style="border-color: #000000; border-style: solid"><p>Renders the required hidden listing_id form element contents when the contact form is called via the listing detail page or if the listing_id $_GET var is present in the URL.</p>
<p>e.g.:</p>
<p>{listing_contact_block}</p>
<p>&lt;input type="hidden" name="listingID" value="{listing_id}" /&gt;</p>
<p>{/listing_contact_block}</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="303" style="border-color: #000000; border-style: solid"><p>agent_contact_block</p></td>
<td width="719" style="border-color: #000000; border-style: solid"><p>Renders the required hidden agent_id form element contents when the contact form is called via the view agent(s) pages or if the agent_id $_GET var is present in the URL.</p>
<p>e.g.:</p>
<p>{agent_contact_block}</p>
<p>&lt;input type="hidden" name="agent_id" value="{agent_id}" /&gt;</p>
<p>{/agent_contact_block}</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="303" style="border-color: #000000; border-style: solid"><p>listing_contact_block</p></td>
<td width="719" style="border-color: #000000; border-style: solid"><p>Used to display information about the listing inquired upon if the form was called via a listing detail page.</p>
<p>{listing_contact_block}In Regard to: {listing_title} {/listing_contact_block}</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="303" style="border-color: #000000; border-style: solid"><p>user_captcha_failed_block</p></td>
<td width="719" style="border-color: #000000; border-style: solid"><p>Block that will be displayed if the user fails to solve the captcha</p>
<p>e.g.:</p>
<p>{user_captcha_failed_block}</p>
<p>&lt;div class="redtext"&gt;Captcha Error - Please reenter the captcha information.&lt;/div&gt;</p>
<p>{/user_captcha_failed_block}</p></td>
</tr>
</tbody>
</table>

