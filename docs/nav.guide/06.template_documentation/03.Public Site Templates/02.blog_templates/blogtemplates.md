# Blog Templates

/template/TEMPLATE\_NAME/FILENAME

  

The following templates are used to control the layout of the Blog
pages.

  

Which Blog Template file being used is determined depending upon which
type of blog page you are viewing.

  

1\. blog\_index.html - This is the main index page that displays a
paginated list of blog posts.

  

2\. blog\_article.html - This is the page which displays the individual
blog article.

  


