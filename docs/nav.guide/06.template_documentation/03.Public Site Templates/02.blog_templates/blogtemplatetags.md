# Blog Template Tags

The following tags are available for use within the following blog
template files:

blog\_index.html

blog\_article.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>lang_VARIABLE</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the corresponding lang variable from the Open-Realty lang file.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_id</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the numeric Blog ID#</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_date_posted</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the date the blog was posted</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_title</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the Blog Title</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_summary</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the abbreviated summary of each blog on the blog_index page.</p>
<p><br />
</p>
<p>Note: To separate blog summaries from the rest of a blog's content, you must enter a &lt;hr /&gt; HTML tag where you want the break  i.e. the separation to occur.</p>
<p>e.g.:</p>
<p>(summary or initial blog content)</p>
<p>&lt;hr /&gt;</p>
<p>(remainder of blog page content)</p>
<p><br />
</p>
<p>When the &lt;hr /&gt; is present in a blog post, only the summary content of the blog post before the break will be displayed on the blog index page, and users must click "Read the rest of this article" to view the entire page.</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_author</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the Author's name</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_comment_count</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the total number of comments on the blog post</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>next_prev_bottom</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the Next Prev Bottom menu.</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_full_article</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the full blog article</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_comment_author</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the comment author</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_comment_date_posted</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the date the comment was posted</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_comment_text</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the comment</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_comments_post_url</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>       Places the comments post URL into the page. It should be used in a FORM ACTION for posting comments. See the default template for an example of proper usage.</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>listing_creation_date</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the listing's creation date in the configured Open-Realty date format.</p>
<p>Note: must be used within {blog_listing_X} {/blog_listing_X} block tags.</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>listing_last_modified_date</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>Displays the listing's last modified date in the configured Open-Realty date format.</p>
<p>Note: must be used within {blog_listing_X} {/blog_listing_X} block tags.</p></td>
</tr>
<tr class="odd">
<td colspan="2" width="1026" style="border-color: #000000; border-style: solid"><p>Blocks</p></td>
</tr>
<tr class="even">
<td colspan="2" width="1026" style="border-color: #000000; border-style: solid"><p>All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE_TAG} The following template tags are used as blocks:</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_entry_block</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>       The content of this Block will be repeated in the Blog index template for the index list for each Blog post</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>admin_edit_link_block</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>The content of this block will appear if the blog admin is logged in. It is used in the template to display the link to the admin edit page.</p></td>
</tr>
<tr class="odd">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_article_comments_block</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>This block is displayed when there are comments on a given blog post. If there are no comments then the entire contents of this block are removed</p></td>
</tr>
<tr class="even">
<td width="286" style="border-color: #000000; border-style: solid"><p>blog_article_comment_item_block</p></td>
<td width="734" style="border-color: #000000; border-style: solid"><p>This block displays each individual comment inside the {blog_article_comments_block}. The contents of this block are repeated for each comment.</p></td>
</tr>
</tbody>
</table>
