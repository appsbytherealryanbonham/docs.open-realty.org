# Custom Search Pages

It is possible to forgo using Open-Realty's somewhat static Search Page
Template system and create your own entirely custom search forms. You
will still use the initial code from the search\_page\_default.html
template for the form content but replace the 'YOUR CUSTOM FORM CONTENT'
area with your own custom form fields and their values that correspond
to fields in OR

  

&lt;form action="index.php" method="get"&gt;

       &lt;!--YOUR CUSTOM FORM CONTENT--&gt;

  

       &lt;input type="hidden" name="action" value="searchresults"/&gt;

       &lt;input type="submit"/&gt;

&lt;/form&gt;

  

If you are unfamiliar with how to build and work with HTML
&lt;FORM&gt;s, and their elements, then you should probably use
Open-Realty's existing search form template. Your custom form must
contain the hidden input element "action" with a value of
"searchresults" shown in the example above so that your form will submit
your form data to the search engine in OR. .

  

The template tag: {search\_field\_FIELDNAME\_element} can be used within
the search form to retrieve specific search fields  providing more
control over creating custom search pages, and placing search fields.
See the
[Search Page Template Tags](../10.search_page_templates/searchpagetemplatetags.md)
section for a list of all tags available for use.

  

  

  

