# Search Results Templates

/template/TEMPLATE\_NAME/FILENAME

  

This template is used to control the layout of the search result page,
this is probably the most complex template file in OR, as the search
results are dependent upon the listing field setup and configuration.
Changes to the listing fields can affect the layout and data that
appears within the search results templates.

  

The Search Results Template that will be used is determined via OR's
Site Config: template tab. Any template file named
'search\_result\_NAME.html' (where "NAME" is a unique name) will be
displayed as an extra available template for selection in Site Config
for the search results page.

  

1\. search\_result\_default.html - This is the default search result
template. It has a number of tags available to it, see the
search\_result\_template\_tags page for details.

  

2\. search\_result\_box.html - This is a tabled layout for the search
results containing more fields and information in a nicely formatted
table.

  

3\. search\_result\_nophoto.html - This is a simple single line of
listing information without any thumbnail photo displayed.

  

4\. search\_result\_tabbed.html - This is a basic implementation of the
tabbed javascript code into a search results template to show the basic
design and requirements of using the tabbed code on the search results.

  

5\. search\_result\_NAME.html - An optional custom search results
template file created by the site developer where "NAME" is a unique
name chosen to describe it.

  

  

------------------------------------------------------------------------

  

Optional

  

5\. search\_results\_class\_CLASSID.html - If there is a template file
present with this naming convention where the CLASSID is the class ID\#
of the property class being searched then this search results template
will be used, overriding the selected search results template set in
Site Config. This template will only be used if the search is queried
against a single Property Class. If the search contains references to
multiple Property Classes then the regular search results template set
in Site Config will be used.

  

For example, if you have the following classes setup:

  

Residential = 1

Land = 2

Commercial = 3

Multifamily= 4

  

If you want different search results templates for each class above your
templates would need to be created and named as follows:

  

search\_results\_class\_1.html  (Residential)

search\_results\_class\_2.html  (Land)

search\_results\_class\_3.html  (Commercial)

search\_results\_class\_4.html  (Multifamily)

  

Note:the CLASS ID\#s for your specific OR install will not necessarily
be contiguous (e.g. 1, 2, 3, 4) if you have added or removed property
classes there may be gaps in the ID\#s.


