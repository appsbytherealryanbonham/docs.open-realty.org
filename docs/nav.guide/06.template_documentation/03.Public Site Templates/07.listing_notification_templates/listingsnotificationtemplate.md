# Listings Notifications Template

/template/TEMPLATE\_NAME/notify\_listings\_default

  

This template is used to control the layout of the listings notification
email that is sent out by Open-Realty's listing notification system. The
listing notification emails are sent out to make any Members who have
saved searches aware of newly added listings that match their criteria.

  

You can trigger the sending of the notification emails manually from the
or administration area via:

  

http://www.yoursite.com/admin/index.php?action=send\_notifications

  

The Listings Notification system is intended to be automated via a CRON
job. See the Listings Notification topic in the "Automating Tasks"
section of this documentation for instructions on setting up CRON jobs
for the Listings Notifications.

  

Which Notify Listings Template to be used is determined by the site
configuration. Any template named notify\_listings\_UNIQUENAME.html will
be shown as an option for selection in Site Configuration.

  

1\. notify\_listings\_default.html - This is the default listings
notification template OR will use unless you create a custom version in
your custom template folder.

  

Note: Since the contents of this template will be sent to an email
client and not a regular HTML web browser, any CSS styling done in this
template should be performed inline. Custom designs should take into
account that some email software cannot load external CSS files, and may
have limited HTML capability overall.

  

