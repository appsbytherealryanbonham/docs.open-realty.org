# Listings Notification Template Tags

Usage:  {tag\_name}.

  

In addition to the Listings Notification Template specific tags below,
you can also utilize any of the Listing Detail Template Tags. Several of
these tags were utilized in the included RSS Template.

  

The following tags are available for use within the following blog
template files:

notify\_listings\_default.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="314" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="705" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="314" style="border-color: #000000; border-style: solid"><p>notify_url</p></td>
<td width="705" style="border-color: #000000; border-style: solid"><p>Returns the relative URL for a listing.</p></td>
</tr>
<tr class="odd">
<td width="314" style="border-color: #000000; border-style: solid"><p>notify_thumb_src</p></td>
<td width="705" style="border-color: #000000; border-style: solid"><p>Returns the URL for the listing thumbnail image</p></td>
</tr>
<tr class="even">
<td width="314" style="border-color: #000000; border-style: solid"><p>notify_thumb_height</p></td>
<td width="705" style="border-color: #000000; border-style: solid"><p>Returns the height of the listing's thumbnail image</p></td>
</tr>
<tr class="odd">
<td width="314" style="border-color: #000000; border-style: solid"><p>notify_thumb_width</p></td>
<td width="705" style="border-color: #000000; border-style: solid"><p>Returns the width of the listing's thumbnail image</p></td>
</tr>
<tr class="even">
<td colspan="2" width="1029" style="border-color: #000000; border-style: solid"><p>Blocks</p></td>
</tr>
<tr class="odd">
<td colspan="2" width="1029" style="border-color: #000000; border-style: solid"><p>Template tag Blocks are template tags that will have a starting and ending tag. Starting tags will be listed below. Blocks must be closed with {/TEMPLATE_TAG_block} The following template tags are block options:</p></td>
</tr>
<tr class="even">
<td width="314" style="border-color: #000000; border-style: solid"><p>notify_listing_block_row repeat="#"</p></td>
<td width="705" style="border-color: #000000; border-style: solid"><p>Block for the file row where rows will be used for displaying files in a horizontal layout. repeat="#" is the number of times to repeat the content inside the file_block before starting a new row.</p>
<p>When closing this block do not include repeat in the closing tag, i.e. a proper closing would be: {/file_block_row}</p></td>
</tr>
<tr class="odd">
<td width="314" height="94" style="border-color: #000000; border-style: solid"><p>notify_listing_block</p></td>
<td width="705" height="94" style="border-color: #000000; border-style: solid"><p>Block for the each listing. The code inside this block will be repeated for each listing being displayed in the notification.</p></td>
</tr>
<tr class="even">
<td width="314" style="border-color: #000000; border-style: solid"><p>notify_img_block</p></td>
<td width="705" style="border-color: #000000; border-style: solid"><p>       Block for the listing image. The code inside this block will only be displayed if there is a thumbnail image to be displayed for the listing. If the no image display is turned on then that is considered a thumbnail and this block will be displayed.</p></td>
</tr>
</tbody>
</table>


