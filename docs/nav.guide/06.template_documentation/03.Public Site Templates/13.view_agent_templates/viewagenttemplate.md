# View Agent Template

(/template/TEMPLATE\_NAME/FILENAME)

  

This template is used to control the layout of the Agent Details page.
This template is not configurable and must be named as below:

  

1\. view\_user\_default.html - This is the view agent template.

  

  


