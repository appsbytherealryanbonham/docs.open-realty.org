# View Agent Template Tags

Usage: {tag\_name}.

  

The Following tags are available for use within the following Agent
template files:

view\_user\_default.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_first_name</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the user's first name.</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_last_name</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the user's last name.</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_email</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the user's email address</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_link</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays a link to the user's view agent page.</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_images_thumbnails</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the thumbnails of the user's images</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_image_full_#</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Generates  a complete &lt;img&gt; HTML tag to the full size Agent photo specified by the #. note: first image is:  1</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>raw_user_image_full_#</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Generates the raw URL to the full size Agent photo specified by the #. note: first image is:  1</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_image_thumb_#</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the Agent's thumbnail image that is specified by the #. note: first image is:  1</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>raw_user_image_thumb_#</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the Agent's thumbnail image NAME ONLY that is specified by the #. note: first image is:  1</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_display_info  *DEPRECIATED*</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the user info. All of the fields other than the first name, last name and username.</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_contact_link</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Renders a link to contact the agent.</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_listings_list</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays links to the Agent's listings in a list format, up to a max of 50 listings.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_listings_list_#</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays links to the Agent's listings in a list format, up to the numeric value provided for #.</p>
<p>e.g:</p>
<p>{user_listings_list_80} would display links for up to 80 listings, but no more than the Agent actually has assigned to them.</p>
<p>Setting too high a value for # if an Agent really has a lot of listings can result in long page load times and increased use of server resources.</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_hit_count</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the hit count for the user.</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_vcard_link</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Adds a link to download the Agent's information as a vcard.</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_id</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays the user's id.</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_listings_link</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Displays a link to the search results page showing listings from this agent.</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_field_FIELDNAME</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Using the name of a user field defined in the agent template editor, you can call the individual field and place it on the page. For example to call the "address" field you would insert {user_field_address} into your template, which would result in both the field caption and value being placed in your template, eg. "Address: 1600 Penn Ave."</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_field_FIELDNAME_value</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Works the same as user_field_FIELDNAME, except you only get the field value. eg. "1600 Penn Ave."</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_field_FIELDNAME_caption</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Works the same as user_field_FIELDNAME, except you only get the field caption. eg. "Address"</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_field_FIELDNAME_rawvalue</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Works the same as user_field_FIELDNAME, except you only get the raw field value. For example when calling a number field instead of getting "150,000" you would simply get "150000".</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_qr_code_link</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Places a QR Code that links to this Agent's profile page.</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>user_files_select</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Places a simple drop down file selection list onto the template for users to select the file (if any) they wish to download.</p></td>
</tr>
<tr class="odd">
<td width="352" style="border-color: #000000; border-style: solid"><p>files_user_vertical</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Places the vertical templated listing files list on the template for users to view and select the files they wish to download.</p></td>
</tr>
<tr class="even">
<td width="352" style="border-color: #000000; border-style: solid"><p>files_user_horizontal</p></td>
<td width="668" style="border-color: #000000; border-style: solid"><p>Places the horizontal templated listing files list on the template for users to view and select the files they wish to download.</p></td>
</tr>
</tbody>
</table>

