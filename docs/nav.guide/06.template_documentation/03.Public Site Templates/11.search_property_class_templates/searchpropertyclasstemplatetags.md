# Search Property Class Template Tags

Usage: {tag\_name}.

  

This template is not a very complicated template and only uses a few
template tags to keep it simple. You will need to include your tags
inside of a form and field set as well as include the submit buttons to
search listings. See the default template for proper coding examples of
these fields.

  

The Following tags are available for use within the following Search
page template files:

search\_class\_default

  

|                             |                                                        |
|-----------------------------|--------------------------------------------------------|
| Tag                         | Description                                            |
| lang\_variable              | Displays the value for any defined lang variable.      |
| property\_class\_checkboxes | Displays the property class checkboxes on the template |


