# Search Page Template

/template/TEMPLATE\_NAME/FILENAME

  

This template is used to control the layout of the search page.

  

Most of the output from this template is created dynamically by OR,
based upon fields you have specified  to be searchable in the Listing
Field Editor. There is only one search page template provided by
default.

  

1\. search\_page\_default.html - Default search page template.

  

------------------------------------------------------------------------

  

Optional

  

2\. search\_page\_class\_CLASSID.html - If one or more of these template
files are present, using a naming convention where the CLASSID is the
numeric class ID\# of the property class being searched, then this
search page template will be substituted for the default search page
template above.

  

The Search Page fields must be contained inside a HTML form. Use the
following code as a basis for building a custom search form:

  

&lt;formaction="index.php" method="get"&gt;

       YOUR CUSTOM FORM CONTENT

       &lt;input type="hidden" name="action" value="searchresults"/&gt;

       &lt;input type="submit"/&gt;

&lt;/form&gt;

  

Replace the "YOUR CUSTOM FORM CONTENT" above with any formatting code
and template tags you wish to use for your template. You can examine the
source code of  the search\_page\_default.html template file as an
example to assist you. Viewing the source code of a live working search
page can also be useful when developing a custom or modified search
page.

  

  

