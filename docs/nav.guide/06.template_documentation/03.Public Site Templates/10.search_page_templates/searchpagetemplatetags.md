# Search Page Template Tags

Usage: {tag\_name}.

  

The following tags are available for use within the following  search
page template files:

search\_page\_default.html

search\_page\_class\_CLASSID.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>featured_listings_NAME</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Renders featured listings using the featured listings template specified in place of "NAME"</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>browse_all_listings</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Renders a text link to "Browse all listings" and shows the total number of listings</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>search_fields</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>This renders all of the listing fields that are set to be searchable. This tag MUST be within  the &lt;form&gt; &lt;/form&gt; tags of the search form.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>agent_searchbox</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Renders a drop down list of Agents. Usind within the search form.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>show_only_with_images</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Renders a check box option for "Show only listings with images" within the search form.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>lang_XXXX</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Renders the value for any defined language variable. Examples: {lang_show_only_with_images} {lang_show_only_with_vtours} {lang_search_listings}</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>show_only_with_vtours</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Renders a check box option for "Show only listings with virtual tours" on the search page.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>search_type</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Adds a hidden field to the search form that sets the property class for the search.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>postalcode_dist_search</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Adds a distance search option to the search page using zip codes. The distance search functions require the use of additional data not provided by Open-Realty®. See the Distance Search Database for details and instructions.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>lat_long_dist_search</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Adds a distance search option to the search page using Latitude and Longitude positions. Your listings must contain listing fields of type latitude and longitude and have their values populated to use this feature (lat/long data is provided by most MLS systems or can be obtained by other means). Open-Realty will not geocode an address automatically. The distance search functions require the use of additional data not provided by Open-Realty®. See the Distance Search Database for details and instructions.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>browse_all_listings_pclass</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Displays the browse all listing link, except it links to the listings in the selected property class and shows a count of listings in the current property class. If no property class is selected it falls back to the old display_all_listing link.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>full_text_search</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Adds a search box that will compare the users entry to all listing field values.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>city_dist_search</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Adds a distance search option to the search page using city names. The distance search functions require the use of additional data not provided by Open-Realty. See the Distance Search Database for details and instructions.</p>
<p>Note: This will not always be 100% accurate as some city names have multiple zip codes and some some are located in more than 1 state.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>search_field_FIELDNAME_element</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Provides the ability to display individual search fields by specifying the listing field name in place of FIELDNAME.</p></td>
</tr>
<tr class="even" data-valign="top">
<td colspan="2" width="1031" style="border-color: #000000; border-style: solid"><p>Blocks</p></td>
</tr>
<tr class="odd" data-valign="top">
<td colspan="2" width="1031" style="border-color: #000000; border-style: solid"><p>All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE_TAG} The following template tags are blocks:</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>templated_search_form_block</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Search &lt;form&gt; code should be contained inside this block.  </p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>no_search_results_block</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Content inside this block will be displayed when a search is performed and there are no results. If there are results, the content will be hidden. This will be in addition to the code/content outside of this block in the template  but not including any contents within {!no_search_results_block}</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="316" style="border-color: #000000; border-style: solid"><p>!no_search_results_block</p></td>
<td width="709" style="border-color: #000000; border-style: solid"><p>Content inside this block will be displayed when a search is performed and there are results. If there are no results, the content will be hidden. This will be in addition to the code outside of this block in the template but not including any contents within {no_search_results_block}</p></td>
</tr>
</tbody>
</table>

