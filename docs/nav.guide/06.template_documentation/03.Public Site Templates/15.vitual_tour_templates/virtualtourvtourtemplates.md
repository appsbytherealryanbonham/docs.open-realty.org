# Virtual Tour (vtour) Templates

/template/TEMPLATE\_NAME/FILENAME

  

This template is used to control the layout of the popup virtual tour
page. This template is rather complex due to large javascript
requirements in the &lt;head&gt; and other various details.

  

Which Template is used is determined by the administrator from the
configuration screen. Any template named vtour\_TEMPLATENAME.html will
be shown as an option in the site configuration.

  

1\. vtour\_default.html - This is the default VTour template.

  

2\. vtour\_classic.html - This is the "Classic" VTour template. The
layout was designed after the original VTour code used in Open-Realty®
prior to its template system being available.


