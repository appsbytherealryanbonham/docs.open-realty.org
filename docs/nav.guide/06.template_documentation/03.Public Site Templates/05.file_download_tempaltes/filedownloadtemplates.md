# File Download Templates

/template/TEMPLATE\_NAME/FILENAME

  

This template is used to control the layout of the file download section
when using the vertical or horizontal templated file list.

  

Which Template is used is determined by the templated file download
template tag used on your site. The templated file download template
tags are:

  

For Listing Template:

  

{files\_listing\_NAME} where NAME is the name of the featured listings
template included in your template directory. For example using the
included templates:

  

{files\_listing\_vertical} will use files\_listing\_vertical.html

  

{files\_listing\_horizontal} will use files\_listing\_horizontal.html

  

1\. files\_listing\_vertical.html - The vertical listing files template.

  

2\. files\_listing\_horizontal.html - The horizontal listing files
template.

  

For Agent Template:

  

{files\_user\_NAME} where NAME is the name of the featured users
template included in your template directory. For example using the
included templates:

  

{files\_user\_vertical} will use files\_user\_vertical.html

  

{files\_user\_horizontal} will use files\_user\_horizontal.html

  

1\. files\_user\_vertical.html - The vertical listing files template.

  

2\. files\_user\_horizontal.html - The horizontal listing files
template.

  


