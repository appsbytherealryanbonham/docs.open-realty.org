# File Download Template Tags

Each tag in the template system looks like this {tag\_name}.

  

The following tags are available for use within the following download
template files:

files\_listing\_vertical.html

files\_listing\_horizontal.html

files\_user\_vertical

files\_user\_horizontal.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="231" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="231" style="border-color: #000000; border-style: solid"><p>lang_variable</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Displays the value for any defined lang variable.</p></td>
</tr>
<tr class="odd">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_download_url</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>The download URL for the file being displayed. This URL is to a download function in Open-Realty®, this will help protect abuse of the file system such as people directly linking to files externally. Use this for creating the link to your files.</p>
<p>example:</p>
<p>&lt;a href="{file_download_url}"&gt;{file_filename}&lt;/a&gt;</p></td>
</tr>
<tr class="even">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_url</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>The direct download URL for the file being displayed. This URL is a direct link to the file on your server. If you use this download URL then you must remove the included .htaccess files in the files/listings and files/users folders. Be aware that this will allow users to link directly to your files from anywhere and may open your site for abuse by people uploading files to share with others at the expense of your bandwidth. Use this for creating the link to your files.</p>
<p>example:</p>
<p>&lt;a href="{file_url}"&gt;{file_filename}&lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_filename</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Displays the filename of the file.</p></td>
</tr>
<tr class="even">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_caption</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Displays the caption of the file.</p></td>
</tr>
<tr class="odd">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_description</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Displays the description of the file.</p></td>
</tr>
<tr class="even">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_filesize</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Displays the filesize of the file.</p></td>
</tr>
<tr class="odd">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_icon</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Displays the URL for the file icon for use in a img tag.</p>
<p>example:</p>
<p>&lt;img src="{file_icon}" height="{file_icon_height}" width="{file_icon_width}" alt="{file_caption}" /&gt;</p></td>
</tr>
<tr class="even">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_icon_height</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>       Displays the configured height for the file icons for use in a img tag.</p>
<p>example:</p>
<p>&lt;img src="{file_icon}" height="{file_icon_height}" width="{file_icon_width}" alt="{file_caption}" /&gt;</p></td>
</tr>
<tr class="odd">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_icon_width</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Displays the configured width for the file icons for use in a img tag.</p>
<p>example:</p>
<p>&lt;img src="{file_icon}" height="{file_icon_height}" width="{file_icon_width}" alt="{file_caption}" /&gt;</p></td>
</tr>
<tr class="even">
<td colspan="2" width="1031" style="border-color: #000000; border-style: solid"><p>Blocks</p></td>
</tr>
<tr class="odd">
<td colspan="2" width="1031" style="border-color: #000000; border-style: solid"><p>Template tag Blocks are template tags that will have a starting and ending tag. Starting tags will be listed below. Blocks must be closed with {/TEMPLATE_TAG_block} The following template tags are block options:</p></td>
</tr>
<tr class="even">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_block</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Block for the each file. The code inside this block will be repeated for each file being displayed. This block is required in order to display the files.</p></td>
</tr>
<tr class="odd">
<td width="231" style="border-color: #000000; border-style: solid"><p>file_block_row repeat="#"</p></td>
<td width="794" style="border-color: #000000; border-style: solid"><p>Block for the file row where rows will be used for displaying files in a horizontal layout. repeat="#" is the number of times to repeat the content inside the file_block before starting a new row. When closing this block do not include repeat in the closing tag, i.e. a proper closing would be: {/file_block_row}</p></td>
</tr>
</tbody>
</table>


