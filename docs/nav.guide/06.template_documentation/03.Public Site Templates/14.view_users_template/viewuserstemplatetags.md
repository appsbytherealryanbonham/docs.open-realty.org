# View Users Template Tags

Usage: {tag\_name}.

  

The Following tags are available for use within the following Agent
template files:

  

view\_users\_default.html

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_image_full_#</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Generates  a complete &lt;img&gt; HTML tag to the full size Agent photo specified by the #. note: first image is:  1</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>raw_agent_image_full_#</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Generates the raw URL to the full size Agent photo specified by the #. note: first image is:  1</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_image_thumb_#</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Displays the agent's thumbnail image that is specified by the #. note: first image is  1</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>raw_agent_image_thumb_#</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Displays the agent's thumbnail image NAME ONLY that is specified by the #. note: first image is 1</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_id</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Displays the Agent's OR ID# for inclusion in a link.</p>
<p>example:</p>
<p>&lt;a href="index.php?action=view_user&amp;user={agent_id}"&gt;Click Here&lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_first_name</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Displays the Agent's first name.</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_last_name</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Displays the Agent's last name.</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_contact_link</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Places a link to the agent contact form.</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_fields</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Displays the agent's fields such as phone, address etc.</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_field_FIELDNAME</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Using the name of a user field defined in the agent template editor, you can call the individual field and place it on the page. For example to call the "address" field you would insert {agentr_field_address} into your template, which would result in both the field caption and value being placed in your template, eg. "Address: 1600 Penn Ave."</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_field_FIELDNAME_value</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Works the same as user_field_FIELDNAME, except you only get the field value. eg. "1600 Penn Ave."</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_field_FIELDNAME_caption</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Works the same as user_field_FIELDNAME, except you only get the field caption. eg. "Address"</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_field_FIELDNAME_rawvalue</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Works the same as user_field_FIELDNAME, except you only get the raw field value. For example when calling a number field instead of getting "150,000" you would simply get "150000".</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_qr_code_link</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>Places a QR code that links to the Agent's OR profile page.</p></td>
</tr>
<tr class="even">
<td colspan="2" width="1029" height="28" style="border-color: #000000; border-style: solid"><p>Blocks</p></td>
</tr>
<tr class="odd">
<td colspan="2" width="1029" height="76" style="border-color: #000000; border-style: solid"><p>All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE_TAG} The following template tags are blocks:</p></td>
</tr>
<tr class="even">
<td width="307" style="border-color: #000000; border-style: solid"><p>user_block</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>The block for the display of each user. Code inside the block will be repeated for each user.</p></td>
</tr>
<tr class="odd">
<td width="307" style="border-color: #000000; border-style: solid"><p>agent_image_thumb_#_block</p></td>
<td width="712" style="border-color: #000000; border-style: solid"><p>       Block for the agent's thumbnail image specified by the #. If there isn't a thumbnail matching that number then code inside this block will not be displayed.</p></td>
</tr>
</tbody>
</table>

