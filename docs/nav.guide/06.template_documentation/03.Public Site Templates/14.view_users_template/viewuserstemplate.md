# View Users Template

/template/TEMPLATE\_NAME/FILENAME

  

This template is used to control the layout of the View Users (Agents)
page which displays a list of the Agents. This template is not
configurable and must be named as below:

  

1\. view\_users\_default.html - This is the view users template.


