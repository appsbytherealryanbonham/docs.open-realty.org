# RSS Template Tags

Usage:  {tag\_name}

  

The following tags are available for use within the following RSS
template files:

rss.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_title</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Places the RSS Title in the RSS Feed. The RSS title is set in the Site Configuration on the RSS tab for the type of RSS feed you are using.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_webroot</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Places the URL to the RSS Feed you are providing.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_description</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Places the RSS Description in the RSS Feed. The RSS Description is set in the Site Configuration on the RSS tab for the RSS feed you are using.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_blog_comments</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Creates a link to the latest blog comments RSS feed, ordered newest to oldest.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_blog_posts</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Creates a link to the latest blog post RSS feed, ordered newest to oldest.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_lastmodified</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Creates a link to the last modified listings RSS feed.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_latestlisting</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Creates a link to the latest listings RSS feed.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_item_guid</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Places a Unique identifier for the item into the RSS feed.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>rss_item_description</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Places the listing or Blog description into the RSS Feed. The listing description is obtained from the Site Configuration RSS tab in the Listing Description Field for the type of RSS Feed you are providing. HTML and Listing Template Tags can be used in that field.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_name</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Renders the listing Agent's name.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_link</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Renders a link to the listing Agent's information page</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_listings</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Renders a link to the search results to view other listings from the listing agent.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_id</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Renders the listing Agents ID#</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>item_title</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Renders the listing or blog title</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>image_thumb_#</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays a listing's thumbnail image. The number corresponds to the to the image order defined in the listing image editor.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>image_thumb_fullurl_#</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the specified thumbnail image at the configured display size. The image is linked to the image viewing path using the full URL. Where # = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>raw_image_thumb_#</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Same as image_thumb_# except it returns the image URL to be used in an image tag.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>image_full_X</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the specified main/full image where X = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>raw_image_full_X</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the specified main/full image NAME ONLY, where X = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>image_full_fullurl_X</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the specified main/full image at the configured display size. The image is linked to the image viewing path using the full URL. Where X = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Using the field name of a listing field that has been defined in the Listing Field Editor, you can retrieve the individual field info and place it on the page. For example, to retrieve and display a field named "address"  you would insert {listing_field_address} into your template, which would result in both the field caption and value being placed in your template, e.g. "Address: 1600 Penn Ave."</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_value</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Works the same listing_field_FIELDNAME, except you only receive the field value. e.g. "1600 Penn Ave."</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_caption</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Works the same as listing_field_FIELDNAME, except you only receive the field caption. e.g. "Address"</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_rawvalue</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Works the same as listing_field_FIELDNAME, except you only receive the raw field value. For example when calling the price field instead of getting "$150,000" you would simply get "150000".</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_pclass</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the name of the property class the listing is assigned to.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_last_name</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's last name.</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_first_name</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's first name.</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_thumbnail_#</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's thumbnail image that is specified in the # place. Whatever number is specified will display the thumbnail that falls in that order.</p>
<p>note: first image is 0, NOT 1</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Using the name of an agent field defined in the user template editor, you can call the individual field and place it on the page. For example to call the "phone" field you would insert {listing_agent_field_phone} into your template, which would result in both the field caption and value being placed in your template, e.g. "Phone: 555-1234"</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_value</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the field value. e.g. "555-1234"</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_rawvalue</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the raw, unformatted field value. For example when calling the price field instead of getting "$150,000" you would simply get "150000".</p></td>
</tr>
<tr class="odd">
<td width="337" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_caption</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the field caption. e.g. "Phone"</p></td>
</tr>
<tr class="even">
<td width="337" style="border-color: #000000; border-style: solid"><p>fulllink_to_listing</p></td>
<td width="685" style="border-color: #000000; border-style: solid"><p>absolute link to the listing instead of the relative link, for use with the RSS Newsfeeds.</p></td>
</tr>
</tbody>
</table>

  

  

|                                                                                                                                                                                                                                                                                                                                                                                                      |                                                                                                                                                                                                                                                 |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Blocks                                                                                                                                                                                                                                                                                                                                                                                               |                                                                                                                                                                                                                                                 |
| All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE\_TAG} The following template tags are blocks: |                                                                                                                                                                                                                                                 |
| rss\_item\_block                                                                                                                                                                                                                                                                                                                                                                                     | This block contains the data for listings that will be repeated for every listing in the RSS Feed.                                                                                                                                              |
| listing\_field\_FIELDNAME\_block                                                                                                                                                                                                                                                                                                                                                                     | To be placed around listing\_field\_FIELDNAME tags to prevent display of any code contained inside this block into the template if the field is not to be shown. You must close this block with /listing\_field\_FIELDNAME\_block               |
| listing\_agent\_field\_FIELDNAME\_block                                                                                                                                                                                                                                                                                                                                                              | To be placed around listing\_agent\_field\_FIELDNAME tags to prevent display of any code contained inside this block into the template if the field is not to be shown. You must close this block with /listing\_agent\_field\_FIELDNAME\_block |


