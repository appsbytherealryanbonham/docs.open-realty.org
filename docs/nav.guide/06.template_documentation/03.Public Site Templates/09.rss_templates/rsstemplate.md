# RSS Template

/template/TEMPLATE\_NAME/FILENAME

  

rss.html

  

This template is used to control the layout of the XML Data feed for the
RSS Newsfeeds. This template is rather complex due to having to follow
certain XML structure for proper XML output that can be read by RSS
Newreaders. There are also several configuration options on the RSS tab
of your site configuration that will affect the output of these news
feeds. You should be familiar with the RSS Specification if you are
attempting to modify the RSS Template file. Use the included template as
a guide for proper usage of the XML RSS structure. This template is not
a configuration selectable template, your RSS Newsfeeds will always use
this specific template file in your selected template directory.

  

In order to setup links to your RSS Newsfeeds you will need to add a
link to one of the following 'action' $\_GET variables.

  

rss\_featured\_listings - This will provide the user with an RSS
Newsfeed of the featured listings.

  

The template tag: {rss\_featured} can be placed within the site template
where you want a link to the RSS Featured Listings feed to appear.
Optionally, you can also place a hard coded link to the feed in your
template. A hard coded link to this featured-only Newsfeed would look
something like:

&lt;a href="index.php?action=rss\_featured\_listings"
&gt;{lang\_rss\_featured\_link\_title}&lt;/a&gt;

------------------------------------------------------------------------

  

rss\_lastmodified\_listings - This will provide the user with an RSS
Newsfeed of the latest modified listings.

  

The template tag: {rss\_lastmodified} should be placed on the site's
template where you want the link to the RSS Last Modified Listings feed
to appear. Optionally you can place a hardcoded link to the feed.

&lt;a href="index.php?action=rss\_lastmodified\_listings"
&gt;{lang\_rss\_lastmodified\_link\_title}&lt;/a&gt;

------------------------------------------------------------------------

  

rss\_latestlisting - This will provide a link to the RSS Newsfeed of the
most recently added listings.

  

The template tag: {rss\_latestlisting} should be placed on the site's
template where you want the link to the RSS Latest Listings feed to
appear. Optionally you can place a hardcoded link to the feed.  

&lt;a href="index.php?action=rss\_latestlisting" &gt;Most Recently
Added&lt;/a&gt;

------------------------------------------------------------------------

  

rss\_blog\_posts -This will provide a link to an  RSS Newsfeed of
recently added blog posts, latest first.

  

The template tag: {rss\_blog\_posts} should be placed on the site's
template where you want the link to the RSS Blog Posts feed to appear.
Optionally you can place a hardcoded link to the feed.

&lt;a href="index.php?action=rss\_blog\_posts" &gt;Recent Blog
Posts&lt;/a&gt;

------------------------------------------------------------------------

  

rss\_blog\_comments - {blog\_comments} This will provide a link to the
RSS Newsfeed of recently added blog comments.

  

The template tag: {blog\_comments} should be placed on the site's
template where you want the link to the RSS Blog Posts feed to appear.
Optionally you can place a hardcoded link to the feed.

&lt;a href="index.php?action=blog\_comments" &gt;Recent Blog
Comments&lt;/a&gt;

  


