# Featured/Random Template Tags

Usage: {tag\_name}.

  

The following template tags are available for use within the following
Featured/Random/Latest/Popular/Class listing template files:

featured\_listing\_vertical.html

featured\_listing\_horizontal.html

random\_listing\_vertical.html

random\_listing\_horizontal.html

latest\_listing\_vertical.html

latest\_listing\_horizontal.html

popular\_listing\_vertical.html

popular\_listing\_horizontal.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>lang_variable</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the value for any defined lang variable.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listingid</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing ID for the listing being displayed.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>featured_url</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>The URL for the featured listing being displayed. Use this for creating the link to your featured listing,</p>
<p>e.g.:</p>
<p>&lt;a href="{featured_url}"&gt;{listing_title}&lt;/a&gt;</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_title</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing's title.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>featured_thumb_src</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing's thumbnail image URL for use inside an img tag.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>featured_thumb_height</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Returns the thumbnail height. For use inside an img tag for the height setting.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>featured_thumb_width</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Returns the thumbnail width. For use inside an img tag for the width setting.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>money_sign</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Adds the configured money sign to the template.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>isfavorite</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Returns "yes" if listing is marked as a favorite listing by a logged in user and "no" if not marked as favorite or the user is not logged in. Can be used for CSS etc.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>featured_large_src</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing's large image URL for use inside an img tag.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>featured_large_height</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Returns the image height. For use inside an img tag for the height setting.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>featured_large_width</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Returns the image width. For use inside an img tag for the height setting.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_name</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>This places the listing agent's name.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_link</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>This places a link to the listing agent's information.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_listings</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>The places a link to the search results to view other listings from the listing agent.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_id</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>The places the listing agents id on the page.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_title</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>This places the listing's title on the page.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>image_thumb_#</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays a listing's thumbnail image. # corresponds to the to the image order defined in the media widget. 1= first, 2= second, and etc.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>image_thumb_fullurl_#</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the specified thumbnail image at the display size configured in Site Config. The image is linked to the image viewing path using the full URL. Where # equals the number of the order of the main image specified in the media widget.  1= first, 2= second, and etc.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>raw_image_thumb_#</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Same as image_thumb_# except it returns the image URL to be used in an image tag.</p>
<p>e.g.:</p>
<p>&lt;imgsrc="{raw_image_thumb_2}"&gt;</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>image_full_#</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the specified main/full image where # equals the number of the order of the main image specified in the image editor.  1= first, 2= second, and etc.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>raw_image_full_#</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>       Displays the specified main/full image NAME ONLY, where # equals the number of the order of the main image specified in the image editor.  1= first, 2= second, and etc.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>image_full_fullurl_#</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>       Displays the specified main/full image at the configured display size. The image is linked to the image viewing path using the full URL. Where # equals the number of the order of the main image specified in the image editor.  1= first, 2= second, and etc.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Using the name of a listing field defined in the listing template editor, you can call the individual field and place it on the page. For example to call the "address" field you would insert {listing_field_address} into your template, which would result in both the field caption and value being placed in your template, e.g. "Address: 1600 Penn Ave."</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_value</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Works the same as listing_field_FIELDNAME, except only the field value is returned. e.g. "1600 Penn Ave."</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_caption</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Works the same as listing_field_FIELDNAME, except only the field caption is returned. e.g. "Address"</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_rawvalue</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Works the same as listing_field_FIELDNAME, except only the raw field value is returned. For example when calling the price field instead of getting "$150,000" you would simply get "150000".</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_pclass</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the name of the property class the listing is assigned to.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_pclass_id</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the numeric OR property class ID# the listing is assigned to.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_last_name</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's last name.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_first_name</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's first name.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_thumbnail_#</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing agent's thumbnail image that is specified in the # place. Whatever number is specified will display the thumbnail that falls in that order. note: first image is: 1</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Using the name of an agent field defined in the user template editor, you can call the individual field and place it on the page. For example to call the "phone" field you would insert {listing_agent_field_phone} into your template, which would result in both the field caption and value being placed in your template, e.g. "Phone: 555-1234"</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_value</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the field value. e.g. "555-1234"</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_rawvalue</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the raw, unformatted field value. For example when calling the price field instead of getting "$150,000" you would simply get "150000".</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_caption</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the field caption. eg. "Phone"</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>fulllink_to_listing</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Absolute link to the listing instead of the relative link, for use with the RSS Newsfeeds.</p></td>
</tr>
<tr class="odd">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_creation_date</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing's creation date in the configured Open-Realty® date format.</p></td>
</tr>
<tr class="even">
<td width="351" style="border-color: #000000; border-style: solid"><p>listing_last_modified_date</p></td>
<td width="669" style="border-color: #000000; border-style: solid"><p>Displays the listing's last modified date in the configured Open-Realty® date format.</p></td>
</tr>
</tbody>
</table>

  

  

|                                                                                                                                                                                                                                                                                                                                                                                                      |                                                                                                                                                                                                                                                                                                                                                         |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Blocks                                                                                                                                                                                                                                                                                                                                                                                               |                                                                                                                                                                                                                                                                                                                                                         |
| All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE\_TAG} The following template tags are blocks: |                                                                                                                                                                                                                                                                                                                                                         |
| featured\_listing\_block                                                                                                                                                                                                                                                                                                                                                                             | Block for the each featured listing. The code inside this block will be repeated for each featured listing displayed.                                                                                                                                                                                                                                   |
| featured\_img\_block                                                                                                                                                                                                                                                                                                                                                                                 | Block for the image for the featured listing being displayed.                                                                                                                                                                                                                                                                                           |
| featured\_listing\_block\_row repeat="\#"                                                                                                                                                                                                                                                                                                                                                            | Block for the featured listing row where rows will be used for displaying featured listings. repeat="\#" is the number of times to repeat the content inside the featured\_listing\_block before starting a new row. When closing this block do not include repeat in the closing tag, i.e. a proper closing would be: {/featured\_listing\_block\_row} |
| featured\_img\_large\_block                                                                                                                                                                                                                                                                                                                                                                          | Block for the large image for the featured listing being displayed.                                                                                                                                                                                                                                                                                     |
| listing\_field\_FIELDNAME\_block                                                                                                                                                                                                                                                                                                                                                                     | To be placed around listing\_field\_FIELDNAME tags to prevent display of any code contained inside this block into the template if the field is not to be shown. You must close this block with /listing\_field\_FIELDNAME\_block                                                                                                                       |
| listing\_agent\_field\_FIELDNAME\_block                                                                                                                                                                                                                                                                                                                                                              | To be placed around listing\_agent\_field\_FIELDNAME tags to prevent display of any code contained inside this block into the template if the field is not to be shown. You must close this block with /listing\_agent\_field\_FIELDNAME\_block                                                                                                         |

