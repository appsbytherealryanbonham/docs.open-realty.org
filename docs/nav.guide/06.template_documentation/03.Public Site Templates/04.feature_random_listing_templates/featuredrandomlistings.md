# Features/Random Listing Templates

/template/TEMPLATE\_NAME/FILENAME

  

The below information applies to both the featured and random listing
options.

  

These templates are used to control the layout of the featured listings.

  

Which featured listing template file to be used is determined by the
featured,  random, latest, popular or class-specific listing template
tag used in your template. Replace "\#" in any tags below with the
Open-Realty Property Class ID\# for the specific class to use.  

Note: tag names are plural, i.e. "listings" while template file names
are not, e.g. "listing".

  

|                                             |                                               |
|---------------------------------------------|-----------------------------------------------|
| Template tag                                | Template file                                 |
| {featured\_listings\_vertical}              | featured\_listing\_vertical.html              |
| {featured\_listings\_horizontal}            | featured\_listing\_horizontal.html            |
| {random\_listings\_vertical}                | random\_listing\_vertical.html                |
| {random\_listings\_horizontal}              | random\_listing\_horizontal.html              |
| {latest\_listings\_vertical}                | latest\_listing\_vertical.html                |
| {latest\_listings\_horizontal}              | latest\_listing\_horizontal.html              |
| {popular\_listings\_vertical}               | popular\_listing\_vertical.html               |
| {popular\_listings\_horizontal}             | popular\_listing\_horizontal.html             |
| {featured\_listings\_vertical\_class\_\#}   | featured\_listing\_vertical\_class\_\#.html   |
| {featured\_listings\_horizontal\_class\_\#} | featured\_listing\_horizontal\_class\_\#.html |
| {random\_listings\_vertical\_class\_\#}     | random\_listing\_vertical\_class\_\#.html     |
| {random\_listings\_horizontal\_class\_\#}   | random\_listing\_horizontal\_class\_\#.html   |
| {latest\_listings\_vertical\_class\_\#}     | latest\_listing\_vertical\_class\_\#.html     |
| {latest\_listings\_horizontal\_class\_\#}   | latest\_listing\_horizontal\_class\_\#.html   |
| {popular\_listings\_vertical\_class\_\#}    | popular\_listing\_vertical\_class\_\#.html    |
| {popular\_listings\_horizontal\_class\_\#}  | popular\_listing\_horizontal\_class\_\#.html  |

  

