# Listing Detail Templates

/template/TEMPLATE\_NAME/FILENAME

  

These Templates are used when a site visitor views a listing. The
template file being used is determined by the site Administrator via
Site Config: template tab. Any template named listing\_detail\_NAME.html
will be displayed as an available template in Site Config.

  

The following template files have  a large number of tags available for
use, see the Listing Detail Template Tags page for available tags.

  

1\. listing\_detail\_default.html - This is the default listing detail
template.

  

2\. listing\_detail\_mainjavacolumn.html - This is the Listing detail
with Java Image with thumbnails in columns.

  

3\. listing\_detail\_slideshow.html - This is the Listing detail with
Slideshow.

  

4\. listing\_detail\_mainjavarows.html - This is the Listing detail with
Java Image with thumbnails in rows.

  

5\. listing\_detail\_tabbed.html - This is the Listing detail template
with all the elements laid-out using Tabs.

  

Optional

6\. listing\_detail\_pclass\#.html - This is an optional listing detail
template based on the property class where the \# is the property class
ID. If you create a listing detail template with this naming for a
specific property class then it will be used for listings in that
property class, overriding your listing detail setting in the site
config. When a template doesn't exist for a specific property class the
setting in the site config will be used.

  

For example, if you have the following property classes setup in OR with
the following class ID\#s:

Residential = 1

Land = 2

Commercial = 3

Multifamily= 4

  

If you want different listing details pages for each class your template
files would need to be named as follows:

  

listing\_detail\_pclass1.html  (Residential)

listing\_detail\_pclass2.html  (Land)

listing\_detail\_pclass3.html  (Commercial)

listing\_detail\_pclass4.html  (Multifamily)


