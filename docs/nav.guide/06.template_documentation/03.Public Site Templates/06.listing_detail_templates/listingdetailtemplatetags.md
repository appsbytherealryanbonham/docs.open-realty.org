# Listing Detail Template Tags

Usage: {tag\_name}.

  

The following tags are available for use within the following  search
page template files:

listing\_detail\_default.html

listing\_detail\_mainjavacolumn.html

listing\_detail\_slideshow.html

listing\_detail\_mainjavarows.html

listing\_detail\_tabbed.html

listing\_detail\_pclass\#.html

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listingid</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing ID.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>headline</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the headline area from the listing template editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>featured_listings_vertical</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Inserts the featured listings in a vertical layout.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>featured_listings_horizontal</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Inserts the featured listings in a horizontal layout.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>vtour_button</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places a link to display the listings virtual tours. Link will only display if there is a virtual tour.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images on the page.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images_nocaption</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images on the page without the image captions.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>top_left</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the top left area from the listing template editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>top_right</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the top right area from the listing template editor.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>center</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the center area from the listing template editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>feature1</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the feature1 area from the listing template editor.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>feature2</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the feature2 area from the listing template editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>bottom_left</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the bottom left area from the listing template editor.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>bottom_right</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays all listing fields set to show in the bottom right area the listing template editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>contact_agent_link</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to "Contact Agent". The link will open a popup window with the contact agent form.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>contact_agent_link_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the "Contact Agent". This can be used to create your own link to the Contact agent page using Html.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_add_favorites</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to allow members to add the listing to there favorites list.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_add_favorites_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the add to favorites. This can be used to create your own link to add to favorites using HTML. For example: &lt;a href="{link_add_favorites_url}"&gt;Add Favorites Link&lt;/a&gt; (The raw html address does not contain ANY popup, size or other code found in the normal link tag)</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_printer_friendly</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to show the listing in the printer friendly template.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_printer_friendly_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the printer friendly page. This can be used to create your own link to the printer friendly page using HTML.</p>
<p>example:</p>
<p>&lt;a href="{link_printer_friendly_url}"&gt;Printer Friendly Link&lt;/a&gt;</p>
<p>(The raw html address does not contain ANY popup, size or other code found in the normal link tag)</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_email_friend</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to allow visitors to email their friends about the listing.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_email_friend_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the email friend form. This can be used to create your own link to the email friend form using HTML. For example: &lt;a href="{link_email_friend_url}"&gt;Email Friend Link&lt;/a&gt; (The raw html address does not contain ANY popup, size or other code found in the normal link tag)</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_map</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to the map system defined in the site configuration.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_map_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the map link. This can be used to create your own link to the map using HTML.</p>
<p>example:</p>
<p>&lt;a href="{link_map_url}"&gt;View Map&lt;/a&gt;</p>
<p>(The raw html address does not contain ANY popup, size or other code found in the normal link tag)</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_yahoo_school</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to the yahoo school information for the listing area.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_yahoo_school_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the yahoo school information link. This can be used to create your own link to the yahoo school information using HTML.</p>
<p>example:</p>
<p>&lt;a href="{link_yahoo_school_url}"&gt;View School Information&lt;/a&gt;</p>
<p>(The raw html address does not contain ANY popup, size or other code found in the normal link tag)</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_yahoo_neighborhood</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to the yahoo neighorhood information for the listing area.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_yahoo_neighborhood_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address for the yahoo neighborhood information link. This can be used to create your own link to the yahoo neighborhood information using HTML.</p>
<p>example:</p>
<p>&lt;a href="{link_yahoo_neighborhood_url}"&gt;View Neighborhood Information&lt;/a&gt;</p>
<p>(The raw html address does not contain ANY popup, size or other code found in the normal link tag)</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_edit_listing</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to edit the listing on the listing detail page if the agent is logged in and is the owner of the listing, or if the logged in agent is an admin or has permissions to edit all listings.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>link_edit_listing_url</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This provides the raw html address to the edit listing page on the listing detail page if the agent is logged in and is the owner of the listing, or if the logged in agent is an Admin or has permissions to edit all listings.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_name</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This displays the listing agent's name.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_link</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link to the listing agent's profile page</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_listings</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places a link to the search results to view other listings from this listing agent.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_id</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>The displays the listing agent's OR ID# on the page.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_title</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This displays the listing's title on the page.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>pclass_link</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This places a link for the search results of the current listing classes</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>hitcount</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>This displays the number of times a the listing has been viewed.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>slideshow_images</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places Images into a slideshow.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>main_image</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places a large main listing image on the page.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>main_image_nodesc</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places a large main listing image on the page. This version of the tag does not display the image description below the main image.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>image_thumb_#</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays a listing's thumbnail image. The number corresponds to the to the image order defined in the listing image editor.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>image_thumb_fullurl_#</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the specified thumbnail image at the configured display size. The image is linked to the image viewing path using the full URL. Where # = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>raw_image_thumb_#</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Same as image_thumb_# except it returns the image url to be used in an image tag.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>main_image_java</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the main listing image on the page for the java image switch.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>main_image_java_nodesc</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the main listing image on the page for the java image switch. This version of the tag does not display the image description below the main image.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images_java</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images in a vertical column, when clicked, the image will replace the current main image on the page. Requires main_image_java also be on page.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images_java_caption</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images in a vertical column with the image caption below each image, when clicked, the image will replace the current main image on the page. Requires main_image_java also be on page.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images_java_rows</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images in a row, when clicked it the image will replace the current main image on the page. Requires main_image_java also be on page.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images_mouseover_java</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images in a vertical column, when the mouse is over the image, the image will replace the current main image on the page. Requires main_image_java also be on page.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images_mouseover_java_caption</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images in a vertical column with the image caption below each image, when the mouse is over the image, the image will replace the current main image on the page. Requires main_image_java also be on page.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_images_mouseover_java_rows</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the listing images in a row, when the mouse is over the image it the image will replace the current main image on the page. Requires main_image_java also be on page.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>image_full_X</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the specified main/full image where X = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>raw_image_full_X</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the specified main/full image NAME ONLY, where X = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>image_full_fullurl_X</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the specified main/full image at the configured display size. The image is linked to the image viewing path using the full URL. Where X = the number of the order of the main image specified in the image editor.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>by using the name of a listing field defined in the listing template editor, you can call the individual field and place it on the page. For example to call the "address" field you would insert {listing_field_address} into your template, which would result in both the field caption and value being placed in your template, e.g. "Address: 1600 Penn Ave."</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_value</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Works the same listing_field_FIELDNAME, except you only get the field value. e.g. "1600 Penn Ave."</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_caption</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Works the same listing_field_FIELDNAME, except you only get the field caption. e.g. "Address"</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_rawvalue</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Works the same listing_field_FIELDNAME, except you only get the raw field value. For example when calling the price field instead of getting "$150,000" you would simply get "150000".</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>next_prev</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the next - prev options on the Listing Detail page. To use you will need to place the tag on the listing detail template AND turn on "Show NextPrev Bar On Listing Page" in Configuration_listing_settings .</p>
<p><br />
</p>
<p>WARNING: This will cause a performance hit on sites with large databases that might yield large search results.</p></td>
</tr>
<tr class="odd">
<td width="284" height="24" style="border-color: #000000; border-style: solid"><p>next_prev_bottom</p></td>
<td width="792" height="24" style="border-color: #000000; border-style: solid"><p>Displays the next - prev options on the Listing Detail page. To use you will need to place the tag on the listing detail template AND turn on "Show NextPrev Bar On Listing Page" in Configuration_listing_settings. This uses the next_prev_bottom.html template.</p>
<p><br />
</p>
<p>WARNING: This will cause a performance hit on sites with large databases that might yield large search results.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_pclass</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the name of the property class the listing is assigned to.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_pclass_id</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the numeric OR property class ID# the listing is assigned to.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_last_name</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing Agent's last name.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_first_name</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing Agent's first name.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_thumbnail_#</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing Agent's thumbnail image that is specified in the # place. Whatever number is specified will display the thumbnail that falls in that order. note: first image is: 1</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>by using the name of an Agent field defined in the user template editor, you can call the individual field and place it on the page. For example to call the "phone" field you would insert {listing_agent_field_phone} into your template, which would result in both the field caption and value being placed in your template, e.g. "Phone: 555-1234"</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_value</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the field value. eg. "555-1234"</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_rawvalue</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the raw, unformatted field value. For example when calling the price field instead of getting "$150,000" you would simply get "150000".</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_caption</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Works the same listing_agent_field_FIELDNAME, except you only get the field caption. eg. "Phone"</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>show_vtour</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Embeds the VTour on your listing page. This will use the selected VTour template to display the VTour in your listing page. You should use the vtour_tab_block template block around this tag to hide the vtour if there is not a supported vtour uploaded for that listing.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>fulllink_to_listing</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>absolute link to the listing instead of the relative link, for use with the RSS Newsfeeds.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>get_creation_date *DEPRECATED</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing's creation date in the configured Open-Realty® date format.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>get_modified_date *DEPRECATED</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing's last modification date in the configured Open-Realty® date format.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_creation_date</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing's creation date in the configured Open-Realty® date format. (replaces {get_creation_date})</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_last_modified_date</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Displays the listing's last modified date in the configured Open-Realty® date format. (replaces {get_modified_date})</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>listing_files_select</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places a simple drop down file selection list onto the template for users to select the file they wish to download.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>files_listing_vertical</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the vertical templated listing files list on the template for users to view and select the files they wish to download.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>files_listing_horizontal</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Places the horizontal templated listing files list on the template for users to view and select the files they wish to download.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>get_featured</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Adds "featured" to listing details or search results if listing is featured otherwise does nothing. Useful for CSS templating purposes or just letting people know this is a featured listing.</p></td>
</tr>
<tr class="odd">
<td width="284" style="border-color: #000000; border-style: solid"><p>get_featured_raw</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Adds "yes" or "no" to the template depending on if the listing is featured or not. Primarily used in CSS for templating purposes.</p></td>
</tr>
<tr class="even">
<td width="284" style="border-color: #000000; border-style: solid"><p>money_sign</p></td>
<td width="792" style="border-color: #000000; border-style: solid"><p>Adds the configured money sign to the template.</p></td>
</tr>
</tbody>
</table>

  

  

<table data-border="1" data-cellpadding="1" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd" data-valign="top">
<td colspan="2" width="100%" style="border-color: #000000; border-style: solid"><p>Blocks</p></td>
</tr>
<tr class="even" data-valign="top">
<td colspan="2" width="100%" style="border-color: #000000; border-style: solid"><p>All code contained on the template inside a block will be controlled according to the block it is contained inside. This is for template designers to control the display of many fields or code by enclosing it in one simple template tag block. A STARTING block will look like a normal template tag but that block must be closed with {/TEMPLATE_TAG} The following template tags are blocks:</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>hide_printer_friendly</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Hides the content contained inside this block when the printer friendly page is being displayed. You must close this block with /hide_printer_friendly</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>show_printer_friendly</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Shows the content contained inside this block when the printer friendly page is being displayed. You must close this block with /show_printer_friendly</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>show_listed_by_admin_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Creates a block for listed by admin data. Uses the "Show Listedby Link For Admin's Listings" setting in the Configuration_listing_settings . You must close this block with /show_listed_by_admin_block</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>!show_listed_by_admin_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Creates a block to be displayed if this is an admin listing and if we have the Show Listedby Admin option disabled. Uses the "Show Listedby Link For Admin's Listings" setting in the Configuration_listing_settings . You must close this block with /!show_listed_by_admin</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>vtour_tab_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Hides the content contained inside this block if there is no supported vtour uploaded for a listing. This tag is primarily for use on the tabbed template but can be used around the show_vtour tag if embedding a vtour on your listing page. You must close this block with /vtour_tab_block</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>listing_field_FIELDNAME_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>To be placed around listing_field_FIELDNAME tags to prevent display of any code contained inside this block into the template if the field is not to be shown. You must close this block with /listing_field_FIELDNAME_block</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>!listing_field_FIELDNAME_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Prevents display of any code contained inside this block, if the field is empty or dies not exists for this listing.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>listing_agent_field_FIELDNAME_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>To be placed around listing_agent_field_FIELDNAME tags to prevent display of any code contained inside this block into the template if the field is not to be shown. You must close this block with /listing_agent_field_FIELDNAME_block</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>listing_favorite_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Show content placed within {listing_favorite_block} {/listing_favorite_block} tags for when a Member views a listing that is in their favorite listing list. When used as: {!listing_favorite_block} {/!listing_favorite_block} content is hidden.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>slideshow_thumbnail_group_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Beginning block tag for creating a photo container for slideshow or carousel javascript applications.</p>
<p>example usage:</p>
<p>{slideshow_thumbnail_group_block}</p>
<p>&lt;div&gt;</p>
<p>  {slideshow_thumbnail_block}</p>
<p>&lt;imgsrc="{slideshow_thumbnail}"width="{slideshow_width}"alt="{slideshow_caption}"title="{slideshow_title}"/&gt;</p>
<p>  {/slideshow_thumbnail_block}</p>
<p>&lt;/div&gt;</p>
<p>{/slideshow_thumbnail_group_block}</p>
<p><br />
</p>
<p>The above code blocks would render HTML output similar to below, for a listing with 4 photos:</p>
<p><br />
</p>
<p>&lt;div&gt;</p>
<p>&lt;imgsrc="http://yoursite.com/images/listing_photos/house_image1.jpg"width="100"alt="your optional caption1"title="http://yoursite.com/images/listing_photos/house_image1.jpg"&gt;</p>
<p>&lt;imgsrc="http://yoursite.com/images/listing_photos/house_image2.jpg"width="100"alt="your optional caption2"title="http://yoursite.com/images/listing_photos/house_image2.jpg"&gt;</p>
<p>&lt;imgsrc="http://yoursite.com/images/listing_photos/house_image3.jpg"width="100"alt="your optional caption3"title="http://yoursite.com/images/listing_photos/house_image3.jpg"&gt;</p>
<p>&lt;imgsrc="http://yoursite.com/images/listing_photos/house_image4.jpg"width="100"alt="your optional caption4"title="http://yoursite.com/images/listing_photos/house_image4.jpg"&gt;</p>
<p>&lt;/div&gt;</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="34%" style="border-color: #000000; border-style: solid"><p>slideshow_thumbnail_block</p></td>
<td width="100%" style="border-color: #000000; border-style: solid"><p>Intermediate block tag for iterating the listing photo used within the slideshow_thumbnail_group_block. See example above. usable tags within this block:</p>
<p>slideshow_thumbnail = renders the URL of the thumbnail photo</p>
<p>slideshow_mainimage = renders the URL of the main photo</p>
<p>slideshow_title = also renders the URL of the main photo *deprecated</p>
<p>slideshow_caption = renders the photo caption (if any)</p>
<p>slideshow_width = renders the width of the thumbnail set in Site Config -&gt; Uploads/Images -&gt; Thumbnail Width.</p>
<p>slideshow_width_mainimage = renders the width of the main listing photo as set in Site Config -&gt; Uploads/Images -&gt; Max Image Width.</p>
<p>slideshow_description = renders the listing photo description (if any).</p>
<p><br />
</p></td>
</tr>
</tbody>
</table>


