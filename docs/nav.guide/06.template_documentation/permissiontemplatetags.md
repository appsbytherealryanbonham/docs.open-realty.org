# Permission Template Tags

Each permission tag in the template system are made up of pair of tags
that surround template data you wish to restrict or display only to a
specific group.

Example:

Opening tag: {check_admin}

&lt;content to display to admin goes here&gt;

Closing tag: {/check_admin}

In addition to the permission template tags below, you can also add ! in
front of any permission tag to check for the condition of NOT having
that permission. Where the above example is restricting any enclosed
content  for admin permissions, the below block tag examples will check
if the user is NOT an admin.

Example:

Opening tag: {!check_admin}

&lt;content to display to everyone except the
admin goes here&gt;

Closing tag: {/!check_admin}

The Following tags are available for use in all of the template files.
You can add the NOT "!" logic to any of the listed permission tags:

|                             |                                                                                       |
| --------------------------- | ------------------------------------------------------------------------------------- |
| Tag                         | Description:                                                                          |
| check_agent                 | Displays content to agents.                                                           |
| check_guest                 | Displays content to guest only. Logged in members/agents will not see this info.      |
| check_member                | Displays content to members.                                                          |
| check_admin                 | Displays content to administrators.                                                   |
| check_view_logs             | Displays content to administrators or agents with view log permissions                |
| check_edit_pages            | Displays content to administrators or agents with edit page permissions               |
| check_have_vtours           | Displays content to administrators or agents with virtual tour permissions            |
| check_have_files            | Displays content to administrators or agents with file upload permissions             |
| check_edit_site_config      | Displays content to administrators or agents with edit site configuration permission. |
| check_edit_member_template  | Displays content to administrators or agents with edit member template permission.    |
| check_edit_agent_template   | Displays content to administrators or agents with edit agent template permission.     |
| check_edit_listing_template | Displays content to administrators or agents with edit listing template permission.   |
| check_edit_all_listings     | Displays content to administrators or agents with edit all listings permission.       |
| check_edit_all_users        | Displays content to administrators or agents with edit all users permission.          |
| check_edit_pages            | Displays content to administrators or agents with edit pages permission.              |
| check_edit_listing_classes  | Displays content to administrators or agents with edit listing classes permission.    |
