# Email Template Tags

Usage: {tag\_name}.

  

<table data-border="1" data-cellpadding="4" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd" data-valign="top">
<td width="268" style="border-color: #000000; border-style: solid"><p>Tag</p></td>
<td width="750" style="border-color: #000000; border-style: solid"><p>Description</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>lang_VARIABLE</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Displays the corresponding lang variable from the Open-Realty lang file.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>baseurl</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the base URL where Open-Realty is installed, i.e. the root of your Open-Realty site.</p>
<p>e.g.</p>
<p>http://www.mywebsite.com</p>
<p>or, if installed in a folder:</p>
<p>http://www.mywebsite.com/foldername</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>company_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Displays the company name set in the Open-Realty <a href="../../../05.administration/13.site_configuration/general.html" class="rvts159">Site Configuration</a>.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>lead_id</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the lead_id for the contact being processed.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>lead_url</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the URL to the lead in the contact management system.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_id</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's Open-Realty user_id used for Agent emails</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_first_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Agent's first name for registration and lead notification.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_last_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Agent's last name for registration and lead notification.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Agent's Open-Realty username</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_email</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><br />
</td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_field_FIELDNAME</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the name and contents of a user field defined in the Agent Field Editor. Used for the user_lead_notification email that is sent to the contact form user. For example, to retrieve  the "phone" field you would insert {agent_field_phone} into your template, which would result in both the field caption and value being placed in your template,</p>
<p>e.g.:</p>
<p>Phone: 555-555-1212</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>agent_image_thumb_X</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Listing agent or the contact agents Photo thumbnail, where X = the number of the order of the image specified in the media widget. Used within the user_lead_notification template.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_id</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's Open-Realty user_id. Used for Member emails</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_first_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's First name for registration and lead notification.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_last_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's Last name for registration and lead notification.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's Open-Realty username</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_email</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's email address.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_password</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Member's Open-Realty password.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>member_login_link</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the URL to the Member login page</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>fulllink_to_listing</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders a link to the listing being inquired upon via the contact form. Must be used within {listing_contact_block} block tags.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_id</p></td>
<td width="750" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Open-Realty listing id for the new_listing_notification that is sent to admin</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_title</p></td>
<td width="750" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the Listing Title for the listing being inquired upon via the contact form. Must be used within {listing_contact_block} block tags.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_pclass</p></td>
<td width="750" height="26" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the property class of the listing. Used with  new_listing_notification emails sent to admin.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_agent_first_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the listing Agent's (listing owner's) first name. Used with new_listing_notification emails sent to admin.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_agent_last_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the listing Agent's (listing owner's) last name. Used with new_listing_notification emails sent to admin.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>raw_image_full_X</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the specified main/full size image URL, where X = the number of the order of the main image specified in the media widget. Must be used within {listing_contact_block} block tags.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>display_all_lead_fields</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the form field captions and data provided by the Member who submitted the contact request.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>verification_email_link</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the URL to the email verification link. Must be used within {require_email_verification_block} block tags. Applicable when email verification is enabled in Site Config.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>notification_time</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the time the user registered for an account. Can also be used for new listing notification emails sent to admin for displaying the time a new listing was added.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>user_ip</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the IP address of the user that registered for an account.</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>lead_field_comments_value</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the comments (if any) when a lead has been reassigned.</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="268" data-valign="middle" style="border-color: #000000; border-style: solid"><p>assigned_by_name</p></td>
<td width="750" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Renders the name of the user who re-assigned a lead</p></td>
</tr>
</tbody>
</table>

  

  

<table data-border="1" data-cellpadding="4" data-cellspacing="2" style="border-color: #000000; border-style: solid;">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td colspan="2" width="1028" style="border-color: #000000; border-style: solid"><p>Block Tags</p></td>
</tr>
<tr class="even">
<td colspan="2" width="1028" style="border-color: #000000; border-style: solid"><p>Template tag Blocks are template tags that  have a starting and ending tag. ALL code inside a block will be displayed or hidden depending on if the block's conditions are met. Starting tags will be listed below. Blocks must be closed with {/TEMPLATE_TAG_block}.  The following template tags are block options:</p></td>
</tr>
<tr class="odd">
<td width="271" style="border-color: #000000; border-style: solid"><p>subject_block</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Contains the subject of the email</p>
<p>e.g:</p>
<p>{subject_block}</p>
<p>       You have a new contact request: {lead_id} from {member_first_name} {member_last_name}</p>
<p>{/subject_block}</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="271" data-valign="middle" style="border-color: #000000; border-style: solid"><p>listing_agent_block</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Contains information if the contact was generated via the view_agent or view_agents page:</p>
<p>e.g.:</p>
<p>{agent_contact_block}</p>
<p>       &lt;!--  Display if contact triggered from Agent page --&gt;</p>
<p>       Thank you for contacting me. I will get back to you as soon as possible with the information you requested.</p>
<p>       My contact information is provided here should you have any more questions in the meantime.</p>
<p>{/agent_contact_block}</p></td>
</tr>
<tr class="odd">
<td width="271" style="border-color: #000000; border-style: solid"><p>listing_contact_block</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Contains information regarding the listing being enquired upon if the contact was generated via a listing detail page:</p>
<p>e.g.:</p>
<p>{listing_contact_block}</p>
<p>       &lt;!--  Display link to listing and title if contact triggered from a listing page --&gt;</p>
<p>       &lt;h2&gt;</p>
<p>       &lt;a href="{fulllink_to_listing}"  title=""&gt;{listing_title}&lt;/a&gt;</p>
<p>       &lt;/h2&gt;</p>
<p>{/listing_contact_block}</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td width="271" style="border-color: #000000; border-style: solid"><p>moderated_block</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Contains information to show to the recipient if user moderation is active. This applies to both Member or Agent moderation depending on which is being used.</p>
<p>e.g.:</p>
<p>{moderated_block}</p>
<p>       &lt;!-- show if Agent Moderation is active  --&gt;</p>
<p>       &lt;p&gt;Thank you, your application for an account is being reviewed&lt;/p&gt;</p>
<p>{/moderated_block}</p></td>
</tr>
<tr class="odd" data-valign="middle">
<td width="271" style="border-color: #000000; border-style: solid"><p>require_email_verification_block</p></td>
<td width="751" style="border-color: #000000; border-style: solid"><p>Contains information to show to the recipient if email verification is active.</p>
<p>e.g.:</p>
<p>{require_email_verification_block}</p>
<p>       &lt;!-- show if Email verification is active  --&gt;</p>
<p>       &lt;p&gt;</p>
<p>       You must verify your email is active by clicking on the following link</p>
<p>       &lt;br /&gt;</p>
<p>       &lt;a href="{verification_email_link}"&gt;{verification_email_link}&lt;/a&gt;</p>
<p>       &lt;/p&gt;</p>
<p>{/require_email_verification_block}</p></td>
</tr>
<tr class="even" data-valign="top">
<td width="271" data-valign="middle" style="border-color: #000000; border-style: solid"><p>password_block</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Contains the user's login password from when they registered to create their account. Used for account registration/signup verification emails.</p>
<p>e.g:</p>
<p>{password_block}</p>
<p>       &lt;b&gt;Password:&lt;/b&gt; {member_password}</p>
<p>{/password_block}</p></td>
</tr>
<tr class="odd" data-valign="top">
<td width="271" data-valign="middle" style="border-color: #000000; border-style: solid"><p>!password_block</p></td>
<td width="751" data-valign="middle" style="border-color: #000000; border-style: solid"><p>Displays contents if when registration verification email template used for password recovery.</p>
<p>e.g.:</p>
<p>{!password_block}</p>
<p>       &lt;b&gt;Password:&lt;/b&gt; The password you entered when you signed up. If you do not know what this was, you can use the password reset feature on the login page</p>
<p>{/!password_block}</p></td>
</tr>
</tbody>
</table>


