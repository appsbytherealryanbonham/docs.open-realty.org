# Email Templates

The following email templates are used by Open-Realty to send out
various system-generated emails to the Admin, Agents and Members. These
templates are located in:

  

/admin/templates/default/email

  

Templates:

agent\_activation\_email.html

agent\_lead\_notification.html

agent\_signup\_email.html

agent\_signup\_notification.html

lead\_assigned.html

member\_activation\_email.html

member\_signup\_email.html

member\_signup\_notification.html

new\_listing\_notification.html

user\_lead\_notification.html

  

Note: To make changes to these templates, first copy them to an /email/
folder within your presently selected administration template folder
(referred to in these docs as TEMPLATE\_NAME) and make your changes to
those copies, otherwise your changes will be overwritten by future
upgrades.

  

example location for OR\_small template:

/admin/template/OR\_small/email/

  

example location for a custom administration template:

/admin/template/TEMPLATE\_NAME/email/
