# Template System Logic

The following illustration outlines in simplest terms how the
Open-Realty template engine determines which template folder to use to
load the necessary design template file if present.

<img src="/assets/aa8b33030982d195a155ae297eaa1e8727cc32bc.png" class="libimg" width="427" height="687" />

  

The above logic applies to all interior pages of an OR site. The main
template file which is used for the home page, containing the majority
of a site's design: 'main.html' will always be loaded from the currently
selected template folder set in Site Config. A custom site Template must
contain a 'main.html' file at a minimum.  The default template folder
otherwise contains all the template files for all interior pages in OR
except for 'main.html'.  

  

working example:

1.  OR is set via Site Config to use the 'mytemplate' Site Template, and
    the 'slideshow' Listing Template.

  

1.  A site visitor requests to view the listing detail page for a
    specific listing.

  

1.  OR first checks to see if the template folder /template/mytemplate/
    contains a slideshow listing detail template file named:
    'listing\_detail\_slideshow.html'.

  

1.  If the custom slideshow listing detail template file exists, load
    and parse the 'main.html' and 'listing\_detail\_slideshow.html'
    files from our custom  template folder and send the output to the
    site visitor.

  

1.  If the custom listing detail template file does not exist in our
    custom template folder, load and parse the 'main.html' template file
    from  /template/mytemplate/ as usual, and then fail-over to retrieve
    the missing 'listing\_detail\_slideshow.html' template file from the
     /template/default/ folder and send that to the site visitor.

  

The custom template system in OR provides a site developer with a lot of
flexibility in terms of just how much of an overall site layout needs to
be customized to suit a purpose. This method allows custom site layouts
to become functional much more quickly, and can greatly reduce the
number of files a developer or designer has to manage to create a custom
template..

