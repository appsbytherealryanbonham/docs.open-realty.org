# Introduction

Open-Realty® (often abbreviated as OR) is a web-based real estate listing management and lead generation content management system (CMS) that is designed to be very reliable and flexible as a framework for creating and managing a real estate website. Intended to be easy to set up and use, Open-Realty is developed using PHP and javascript and utilizes MySQL database technology. A large international community employs websites running Open-Realty daily. Since its release in 2003, Open-Realty remains the most popular, powerful, and most flexible choice available for building a professional real estate web site.

Open-Realty offers easy installation, highly customizable display templates, virtual tour support, advanced search capabilities, and many other necessary and desirable end-user features. Open-Realty is also very extensible, utilizing a powerful developers' add-on system for achieving almost limitless customization and integration potential with third party apps and systems.

Combine Open-Realty and its core features with well-proven add-ons such and you have all the tools necessary to build extremely competitive and professional real estate websites. Our official add-ons include:

- TransparentRETS - MLS listing import tool
- TransparentMaps advanced Google maps add-on
