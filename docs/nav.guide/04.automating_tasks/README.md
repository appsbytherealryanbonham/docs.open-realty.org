# Overview

CRON jobs

CRON jobs are a way to trigger a URL or run a program at a preset time automatically. CRON is a time-based job or task scheduler. CRON is commonly used to automate system maintenance or administration, but its general-purpose nature means that it can be used for lots of other purposes. The commands that you want your CRON job to execute will be different for each task you want to run. A specific example of a CRON command to use will be noted for each task in this section of the documentation. The way CRON is setup and how jobs are formatted are dependant on a host's particular setup, so the examples provided herein may or may nor work as-is, and may need to be adjusted for your specific host's server setup. 

Make sure to replace ORADMINUSER with your Open-Realty® Admin account user name, ORADMINPASSWORD with your OR Admin account password, and replace www.yourdomain.com with the real domain name your site is using and any additional path elements if OR is installed in a folder. 

If you are unsure of how to create a CRON job with your hosting account, you should contact your host for details as this varies a lot between servers and control panels.
