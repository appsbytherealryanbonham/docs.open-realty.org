# Update BrowserCap Cache

To use the tracking option of Open-Realty to see stats on who is visiting your website, we use Browser Capabilities PHP Project to determine the users browser version, etc. In order for this to work, you have to setup a cron to update the browsercap cache. Tracking will not function correctly if this cache is not available.

Setup a cronjob on your hosting to run the following every 24hrs.

```
cd /YOURPATH; ./vendor/bin/browscap-php browscap:update -c ./files/browsercap_cache/
```

::: tip

Replace "/YOURPATH" above with the path to your open-realty site often something like "/home/youraccount/public_html/"

:::
