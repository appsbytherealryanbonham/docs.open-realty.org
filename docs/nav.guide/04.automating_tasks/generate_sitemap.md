# Generate XML Sitemap

Open-Realty has the ability to generate a [sitemaps.org](http://www.sitemaps.org/protocol.html) compliant XML sitemap. The XML sitemap generation task should be setup to be run at least once per day assuming new listings and/or content are added to your site at least once per day. Generated sitemap files are placed in the root of your Open-Realty install

Triggering the sitemap generation task

It is recommended that you run any CRON task during off-peak hours to reduce load on your site while the task is being run. If you import listings using an import add-on such as IDXManager or TransparentRETS then it is recommended that you run this task after the daily import of any listings is completed.

Example XML sitemap generation CRON command:

```bash
php YOURPATH/admin/index.php "user_name=ORADMINUSER&user_pass=ORADMINPASSWORD"  "action=generate_sitemap"
```

Example XML sitemap generation Manual Trigger:

If you prefer to trigger the creation of your sitemap manually, you can do so with the following URL:

    http://www.yoursite.com/admin/index.php?action=generate_sitemap

Alternatively you could also add a link to the sitemap generator in your OR administration template using the following code:

```
{check_admin}<a href="http://www.yoursite.com/admin/index.php?action=generate_sitemap">Create XML Sitemap</a>{/check_admin}
```

When triggering the sitemap generation manually, the process may take several minutes or longer to complete depending on how many listings, page editor pages, and blog pages are being processed. Avoid being impatient and verify the task has completed before triggering the task again.
