# Listings Notification

The Listings Notification task should be setup to be run at least once per day. The Listings Notification task will process all Members' saved searches and send out any matching new listings added since the last time the task was processed.

Triggering the notification task:

It is recommended that you run any CRON task during off-peak hours to reduce load on your site while the task is being run. If you import listings using an import add-on such as IDXManager or TransparentRETS then it is recommended that you run this task after the daily import of any listings is completed.

Example Listings Notification CRON command:

```bash
php YOURPATH/admin/index.php "user_name=ORADMINUSER&user_pass=ORADMINPASSWORD"  "action=send_notifications"
```

Note:The above example assumes CURL is available. You also may need to include the full path to CURL for your host's setup, e.g. /user/bin/curl -d".....

Example Listings Notification Manual Trigger:

If you prefer to process listing notifications manually you can do so with the following URL:

    http://www.yoursite.com/admin/index.php?action=send_notifications

Alternatively you could also add a link to the Listing Notification system in your administration template using the following code:

    {check_admin}Process Listings Notifications{/check_admin}

When triggering the listing notifications manually the process may take several minutes or longer to complete depending on how much many saved searches are being processed and the complexity of those searches. Avoid being impatient and verify the task has completed before triggering the task again.
