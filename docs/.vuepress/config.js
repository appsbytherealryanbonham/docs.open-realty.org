const { description } = require('../../package')
const path = require('path');
const getConfig = require("vuepress-bar");
const dirPath = path.join(__dirname, '/..');
const navoptions = {
  addReadMeToFirstGroup: false,
  maxLevel: 4,
  mixDirectoriesAndFilesAlphabetically: true
}
const { nav, sidebar } = getConfig(dirPath, navoptions);
console.dir(sidebar);
sidebar['/nav.addons/'].find((item) => item.title === "Transparentrets").title = "TransparentRETS";
sidebar['/nav.addons/'].find((item) => item.title === "Transparentmaps").title = "TransparentMAPS";
module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Open-Realty Documentation',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],
  dest: 'public',
  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: 'https://gitlab.com/appsbytherealryanbonham/docs.open-realty.org',
    editLinks: true,
    docsDir: 'docs',
    docsBranch: 'main',
    editLinkText: 'Help us improve this page!',
    lastUpdated: true,
    logo: '/assets/img/logo.jpg',
    nav: [
      {
        text: 'User Guide',
        link: '/nav.guide/',
      },
      {
        text: 'Official Addons',
        link: '/nav.addons/',
      },
      {
        text: 'Open-Realty',
        link: 'https://www.open-realty.org'
      }
    ],
    sidebar,
    // sidebar: {
    //   '/guide/': [
    //     {
    //       title: 'Guide',
    //       collapsable: false,
    //       children: [
    //         '',
    //         {
    //           title: 'Installation',
    //           path: '/guide/installation/',

    //           collapsable: true,
    //           children: [

    //             '/guide/installation/server_requirement',
    //             '/guide/installation/new_install',
    //             '/guide/installation/default_login',
    //             '/guide/installation/seo_urls',

    //           ]
    //         },
    //         {
    //           title: 'Upgrading',
    //           path: '/guide/upgrading/',

    //           collapsable: true,
    //           children: [
    //             '/guide/upgrading/upgrade_from_2x.md',
    //             '/guide/upgrading/upgrade_from_3x.md',
    //           ]
    //         },
    //         {
    //           title: 'Moving Hosting Providers',
    //           path: '/guide/move_to_new_host/',

    //           collapsable: true
    //         },
    //         {
    //           title: 'Automating Tasks',
    //           path: '/guide/automating_tasks/',

    //           collapsable: true,
    //           children: [
    //             '/guide/automating_tasks/generate_sitemap.md',
    //             '/guide/automating_tasks/listing_notification.md',
    //           ]
    //         },
    //       ]
    //     }
    //   ],

    // }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    ['@vuepress/plugin-google-analytics', { ga: 'UA-190089802-1' }]
  ]
}
