# Import Commands

The import command is triggered either using a  web browser or using a
CRON job on your server. The import is run by triggering the
Open-Realty® administration action: addon_transparentRETS_loadRETS.

For example, after logging into your OR site as the Admin, you can enter
the following into your web browser address bar to trigger the loader
manually:

http://www.mysite.com/admin/index.php?action=addon\_transparentRETS\_loadRETS

With TransparentRETS 2.0 and higher you can call the loader through the
Open-Realty ajax page, so you do not get  Open-Realty admin template
HTML in the output.

http://www.mysite.com/admin/ajax.php?action=addon\_transparentRETS\_loadRETS

The loader will then run, updating the OR activity log as it downloads
and imports listing data and photos.

---

CRON jobs

Note: You should not attempt to setup your CRON jobs before you have
completed one or more fully working, manually triggered imports.

Setting-up
a <a href="http://en.wikipedia.org/wiki/Cron" class="rvts105">CRON</a> job
is a way for your hosting account to trigger the import loader at a
recurring preset time without having to login to your site and trigger
the import manually. An example CRON job follows. Make sure to replace
ORADMINUSER with your Open-Realty® admin user name, ORADMINPASSWORD with
your password, and replace yourdomain.com with your real domain name and
any additional path elements if you installed OR in a folder and not
root.

If you are unsure of how to create or troubleshoot a CRON job with your
hosting account, you should contact your host for details as this
process varies between different servers and hosting control panels.


```php YOURPATH/admin/index.php "user_name=ORADMINUSER&user_pass=ORADMINPASSWORD"  "action=addon_transparentRETS_loadRETS"```



You must allow enough time between re-running your CRON job so that the
import can complete before another one starts. Overlapping imports can
corrupt your OR listing database. Running an import once per 24 hours is
recommended, more often is possible if you are 100% certain your jobs
will not overlap. .

---

Optional Modifiers

ONLY add modifiers when needed. Please note that when adding modifiers
to the command in a CRON job you must often escape the ampersand
(&) in the URL with a \\.

example:

```php YOURPATH/admin/index.php "user_name=ORADMINUSER&user_pass=ORADMINPASSWORD"  "action=addon_transparentRETS_loadRETS&listingupdate_only=1"```

Please note that your last modified time will still be updated whenever
you use a modifier so use these with caution. These modifiers should
normally only be used when directed by Open-Realty developers.

Skip Data Import - To skip importing update data this run add the
following to your command.

&photoupdate_only=1

Skip Image Import - To skip importing photos for this run, add the
following to your command.

&listingupdate_only=1

Skip Cleanup - This will skip the cleanup routine, leaving any old
listings in your database. This will also prevent us for double checking
for missing listings.

&skipcleanup=1

Change Last Update Time for Photos - This can be used to import missing
photos, if you accidentally delete photos off your site. Using a time
stamp of 0 will result in all images being downloaded. Other then 0 your
time stamp should be a UNIX time stamp in GMT for the date/time you wish
to import all modified photos from.

&photo_last_update=TIMESTAMP

Change Last Update Time for Listings - This can be used to import
missing listings or used if you modify your import criteria. Using a
time stamp of 0 will result in all listing data being downloaded. Other
then 0 your time stamp should be a UNIX time stamp in GMT for the
date/time you wish to import all modified listings from. This Flag is
not needed in normal operations, we automatically send the time of the
last successfully import. IF your MLS does not have a Listing Photo
Timestamp field in the data to search for listings with modified photos,
this will also effect the photo import.

&listing_last_update=TIMESTAMP

Change TimeZone - The RETS specification states that all date/time
queries MUST be send in GMT, however some bad RETS servers expect
date/times to be sent using the local timezone of the server. You should
complain to your MLS if your RETS server does not follow the RETS
specification, as the entire point of having a "RETS" server is that
they are all should operate the same, lowering cost for operating the
server and allowing you to use any RETS client to connect and pull data.
You can change the timezone TransparentRETS sends, by setting the
following flag. This is not recommended however as it violates the RETS
specification. If your server requires datetime's to be sent using GMT-5
(EST) for example you would set the following parameter.

&rets_timezone_offset=5

---

Utility modifiers

These modifiers are used to prepare a new installation of
TransparentRETS, or to start over from scratch. These modifiers delete
information from your OR and TransparentRETS database tables, make very
certain you want to use these as their actions cannot be undone. Running
both of these prior to configuring TransparentRETS is recommended as it
will start you off with a clean listing database and field list.

Remove All Existing Open-Realty Listing and RETS metadata - This command
will DELETE ALL existing listings from your OR database. It will also
remove all cached metadata, and class configuration information about
your RETS server leaving only your basic login information intact in
TransparentRETS.

&addon_transparentRETS_cleanTables

Remove All Existing Open-Realty Fields - This command will DELETE ALL
existing Open-Realty fields from your database. This is not reversible
and should be used with extreme caution. It should be used in
combination with the cleanTables command above.

&action=addon_transparentRETS_cleanAppFields
