# The Final Import

Now that everything is setup and configured, you can trigger a normal
TransparentRETS import, which will import the rest of the listings for
the other classes and their photos. From now on you can use the normal
import trigger:

  

http://yourwebsite.com/admin/index.php?action=addon\_transparentRETS\_loadRETS

(replace yourwebsite.com with your actual web site address)

  

This process could take quite a while and may appear to not be doing
much of anything unless you are looking at and refreshing the OR logs.
However, if the import does not blow-up in the first minute or so, you
can take a break and relax if you like, but definitely be patient. Large
imports of 40K+ listings that have 15+ photos per listing can require an
hour or two the first time. 

  

You should now have a full import working of all active listings and all
corresponding photos from your RETS repository. You can now setup your
CRON jobs for automating the normal import trigger, you can start
installing Agents into Open-Realty, you can start fine-tuning your Field
Maps to remove any unwanted fields from the import, or you can go back
and setup more elaborate  Listing Titles for each class if you didn't
put much thinking into that part of the Update Criteria tab step
already.

  

Your work is definitely not over yet, but the most time consuming part
now should be. 


