# Server Requirements

Unix/Apache (POSIX) based web hosting platform.

Fresh install of Open-Realty v3 or greater.

<a href="http://www.php.net/" class="rvts105">PHP</a> 8.0+ (with the
following modules)

- PHP SimpleXML
- PHP CURL
- PHP OpenSSL

Ability to create CRON jobs that can use CURL (optional, for automating
all processes)

Ability to change your PHP memory_limit

Ability to change your APACHE Timeout Value

Ability to make outbound connections from your hosting account to HTTP,
HTTPS, and RETS servers. (ports 80 and 443, RETS may use others)
