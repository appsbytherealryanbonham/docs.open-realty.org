# Installing the add-on

1\. Upload the transparentRETS folder from the .ZIP file into your
Open-Realty /addons folder. When finished you should have:
/addons/transparentRETS

a. Do NOT rename the folder, it must be named 'transparentRETS'
exactly.

b. Upload the files using FTP BINARY mode and not ascii or "auto"
otherwise you may receive errors,and the add-on may not function.

2\. Login to Open-Realty as the Admin

3\. Click on the "RETS Servers"
icon ![](/assets/transparentRETS/023ca654c1b65415ebd59728ea7cd02897e180a2.jpg "lib:\Btn_rets_servers.jpg").

4\. Click Continue.
