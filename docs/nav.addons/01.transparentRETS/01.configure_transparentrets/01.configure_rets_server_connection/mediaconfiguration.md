# Media Configuration


This screen is where we gather information regarding Media (Photos) from
your MLS's RETS server..

  

![](/assets/transparentRETS/678ac342ca3d20011f1a0295fe1998595fdf459f.jpg "lib:\\Media_resources.jpg")

  

If your MLS is RETS compliant we should have already auto detected this
information in the previous step when we gathered Resource Information.
If not you will be prompted to Detect Media Information, simply press
the button and a list of Media Resources will be retrieved. You should
select the "Photo" Resource. 

  

There is nothing else you need to do on this page. You should now return
the the main Open-Realty™ administration screen and continue with the
directions for configuring the RETS Import Settings starting with Class
Settings. 

  

  

You are now ready to move on to configuring your RETS property classes
to match up with Open-Realty.

