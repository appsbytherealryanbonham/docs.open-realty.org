# Class Configuration

This screen is where we gather information regarding Property classes
from the MLS RETS server..

If the MLS is RETS compliant TransparentRETS should have already
auto-detected this information in the previous step when we gathered
Resource Information. If not, you will be prompted to Detect Class
Information, simply press the button and a list of Property classes
should be displayed.

![](/assets/transparentRETS/504ada0abb3c112847c4c96b7b6a2c7a65b29472.jpg "lib:\Class_configuration.jpg")

You should now click the RETS Media Configuration tab.

New in Version 1.2

There are now 3 icons that will appear next to any RETS class that you
have mapped to an Open-Realty class, see the RESI class in the
screenshot above..

A. The first icon will update the cached metadata from the server. This
is done during your nightly imports and should not have to be done unless
you are directed to by Open-Realty Developers.

B. The second icon will fetch an update from the RETS server for this
specific property class. This is the same as the nightly cron job you
setup except is is limited to one property class.

C. The third icon will fetch an full import from the RETS server for
this specific property class. This pulls a fresh import off all
available listings and photos from the RETS server. This is not a update
but a complete fresh import from scratch and will be very large.

You may now proceed to the
RETS <a href="mediaconfiguration.html" class="rvts105">Media Configuration</a> tab
