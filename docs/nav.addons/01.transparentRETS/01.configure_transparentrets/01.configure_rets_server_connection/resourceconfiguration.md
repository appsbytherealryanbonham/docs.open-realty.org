# Resource Configuration


This screen is where we gather information regarding resources and other
information from the MLS's RETS server.

  

The first time you visit this screen you will need to click on the
"Detect RETS Server Resource" button as shown here 

  

![](/assets/transparentRETS/58b5e06413efc162496c47fd87d3c2ebb0ae5583.jpg "lib:\\Resource_config_detect.jpg")

  

Once you have done this, you should be provided with a list of Resource
Names on your MLS's RETS Server. The "Property" resource should be
selected for you automatically. If not you will need to select it or the
Resource that holds listing data, if your MLS is non compliant and has
named the resource something else. 

  

![](/assets/transparentRETS/f2f34bd0c1dd4b005111c1b90b21d8509ac8e4f3.jpg "lib:\\Resource_config_select.jpg")

  

Once you have done this you should click on
the <a href="classconfiguration.html" class="rvts105">Class Configuration</a> tab. 

