# Class Settings

The next step in setting up TransparentRETS™ is to configure which
property classes you wish to import from your RETS server.

  

1\. Start by selecting a property class that you wish to import from the
RETS server. 

  

![](/assets/transparentRETS/b7efe78818a657810ecc8a993bf4485ff51cd82b.jpg "lib:\\Select_class.jpg")

  

  

2\. Mark the radio button "Yes" on the Import Class option.

  

3\. Select the Open-Realty™ property class you wish to import the data
into. You can only import one RETS property class per Open-Realty™
class. Mapping multiple RETS property classes into the same Open-Realty
property™ class will result in listings not being correctly imported.

  

4\. Click the "Save Settings" button.

  

9\. Great you are now ready to proceed with Field Mapping for this
class. Click the "Field Map" tab on the screen and follow the directions
for creating your Field Map. 

