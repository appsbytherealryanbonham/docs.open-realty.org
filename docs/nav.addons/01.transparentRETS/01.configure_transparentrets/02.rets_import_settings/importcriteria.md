# Import Criteria

This step is where we map define what listings you wish to import from
the RETS server. For most IDX/VOW type sites you will wish to import all
active listings, however you can define any criteria you like. You
should check your local MLS rules to see if there are limitation on how
you limit your data imports. TransparentRETS will automatically change
your search to include a command to pull only listings modified since
the last import. You can override the "Last Import" time when you run
the import process if you need.

You should now see a screen like this.

![](/assets/transparentRETS/0be22cc00864b08d8b70b35a2140b15aec59c8a6.jpg "lib:\Config_server_search.jpg")

For each filed you wish to filter your import on you need to define a
search criteria.

A. Many field will have combo boxes next to them where you can select
one or more items to search on.

B. For fields that have just a text box for you to insert search
criteria you will need to enter a valid RETS search string. See the RETS
Search Examples Below for a quick quide or the RETS specification for
full details.

For more details on a fields meaning or what type of search you can do
on it hold your mouse over the RETS field name.

RETS SEARCH EXAMPLES

Numeric Search - This is how you can search number fields

Enter 10 to find listings where the field contains 10

Enter 10+ to find listings where the field contains a number greater
then 10.

Enter 10- to find listings where the fields contains a number less then 10.

Enter 10-20 to find a listing where the field contains a number between
10 and 20

Text Search - This is how you can search text fields.

Enter TEXT to find listings where the field contains the exact string
TEXT.

Enter C\* to find listings where the field contains text starting with
the letter(s) C

Enter \*TEXT\* to find listings where the field contains a text string
with the phrase TEXT in it.

![](/assets/transparentRETS/4642ae3ba05ad5fde900d06b55adc19ac055ec18.jpg "lib:\Config_server_advsearch.jpg")

Advanced RETS Search New in TransparentRETS 1.2 - You can now right your
own RETS queries to send the the RETS server. You need to follows the
RETS specification when building the queries, entering anything into
this box will override all other search selections you may have made.
This should only be used by advanced users or at the direction of
Open-Realty Developers.

3\. Once you have defined all your import criteria you can proceed with
setting up your update criteria. Click the "Update Criteria" tab on the
screen and follow the directions for defining your Update Criteria.
