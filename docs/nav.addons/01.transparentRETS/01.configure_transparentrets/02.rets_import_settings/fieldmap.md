# Field Map

This step is where we map the fields available for download on the RETS
server to fields in Open-Realty™. This is the most time consuming step
in the setup.

  

You should now see a screen like this. 

  

  

![](/assets/transparentRETS/a28e89db85f4cdfd891222b1a74e8d1713c082ef.jpg "lib:\\Field_map.jpg")

  

1\. Find the first field that you wish to import into Open-Realty™. If
the RETS field names do not reflect a clear meaning of what data the
field holds, hold your mouse over the field name and a pop up will
appear showing you a alternative long and short name from the RETS
server that will help describe the field. 

  

![](/assets/transparentRETS/89ec2abc0dfa5a885cffb9a56da6349f08fc08e4.png "lib:\\Field_map_hover.png")

  

2\. Now that you know what field you want to import you can either map
it to an existing Open-Realty field or create a new field.

  

A. Map to an existing Open-Realty™ Field.

   1. Select the Open-Realty™ Field in the dropdown next the the
corresponding RETS field.

   2. Click the Save Map button next to the field

  

B. Creating a new Open-Realty™ Field (AutoMap).

   1. Next to the RETS field you wish to import into Open-Realty™ find
and click the Auto Create Map button. This will create the necessary
field in Open-Realty and create the field mapping for you. You can later
edit the field's caption and display setting in the Open-Realty™
 Listing Template Editor.

   2. New in TransparentRETS 1.2 You can now create and map to multiple
fields in Open-Realty at one time. You do this by clicking the checkbox
to the left of each field. Once you select all of the fields you with to
automap, you will find a button at the bottom of the field list that
says "AutoMap Selected Fields".

  

 C. The third option is to AutoMap fields from the RETS server to
corresponding fields in Open-Realty. This mapping is based on Field Name
and is useful particularly for doing quick field mapping's on additional
RETS property classes where you have already created Open-Realty fields
from previous classes.

  

3\. Once you have mapped all your fields you can proceed with setting up
your import criteria. Click the "Import Criteria" tab on the screen and
follow the directions for defining your Import Criteria. 

