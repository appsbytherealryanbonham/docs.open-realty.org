# Translating for other Languages


By default TransparentRETS supports the English language, and a "lang"
file is included that contains English phrases which can be edited to
suit your needs.

  

Language file location:

  

/addons/transparentmaps/lang.php

  

The file: lang.php contains a large PHP $lang array, you can edit the
values as you wish, however make sure not to change the array keys. If
you do make changes, make sure to keep  a separate copy before upgrading
TMAPS, otherwise your lang.php file will be overwritten.

  

  

Note:  When altering the array values you must be careful when using the
apostrophe ' character. Any apostrophies that appear in your text must
be escaped with a slash \\ otherwise you will receive an error and TMAPS
will not operate until you fix or undo your changes.

  

e.g. text: Don't overwrite your files.

escaped version: Don\\'t overwrite your files.

  

