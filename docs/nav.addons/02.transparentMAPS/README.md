# What is TransparentMAPS

TransparentMAPS™ (TMAPS) is an add-on for Open-Realty® (OR) that uses the Google Maps API to embed
maps into its various pages to display the locations of listings. The
maps are positioned and embedded using OR template tags specific to
TMAPS which are fully configurable via:
[TransparentMAPS Configuration](02.Configuration/01.APIGeocoderConfiguration.md)

Google's mapping service requires accurate Longitude and Latitude
coordinates to display accurate maps, these coordinates can either be
entered for your listings manually when you create or edit them, or you
can optionally use a 3rd party geocoding service, and TransparentMaps
will automatically obtain and insert the coordinates for you.

![](/assets/transparentMAPS/cce4b51578b6bba347f9216b05b35ea064001b97.jpg)

Features summary:

- Search Results Map - Embed a map to display marker icons (pins)
  identifying the location of each listing returned on the search
  results page.
- Listing Show Map - Embed a map on the listing detail page to
  identify the location of that specific listing.
- Listings in the area - Embed a map on the listing detail page to
  identify the location of other listings within a pre-configured
  distance from the listing being viewed.
- Agent Listings Map - Embed a map on the Agent detail page to
  identify the location of that Agent's listings.
- Fully Configurable - Configuring TMAPS is similar to using
  Open-Realty's Site Configuration. It provides for changes to the
  layout and fields displayed in the Map's info window, the size of
  embedded maps, as well as if a map should be linked to a popup
  window or be an integrated part of the page being displayed.
- Colored marker icons (map pins) defined by property class:
  ![](/assets/transparentMAPS/114c6b6d52caebd4893ad771bb510da1b3b2e724.png)
  ![](/assets/transparentMAPS/3922059c18a1db594e41d9dccb293b87e3bada58.png)
  ![](/assets/transparentMAPS/dd30effa17396a41f94c5ea0aef785d09a8281c2.png)
  ![](/assets/transparentMAPS/138d2d7174cb8b24e5492d1a5016917652488289.png)
  ![](/assets/transparentMAPS/30f2ab40a61c9492410b4ed5b2d346afe15b82cc.png)  Marker
  icon colors are displayed corrosponding to the property class ID\#
  the listing belongs to. Marker icons for up to 25 property classes
  are included.
- Geocoding of listings\* - When listings are to be displayed on the
  map they are checked for lat/long data, if the listing does not have
  lat/long data available, TMAPS will attempt to obtain Lat/Long
  coordinates using the listing's street address and any returned
  coordinates will be stored in the listing database to ensure the
  accurate display of maps that involve that listing in the future.
- Geocode all listings\* - A function that can be triggered using a
  server CRON job to geocode any listings that do not contain lat/long
  coordinates in the listing database. It can also be manually
  triggered from your Open-Realty administration area. Only retrieves
  geocoding coordinates for listings that are missing Lat/Long
  information, so you can manually correct any invalid coordinates
  without your changes being overwritten.
- Street View Support\*\* -
   ![](/assets/transparentMAPS/e3297124da69fa8f86419a80715f813e2eb917fa.jpg) Optionally
  display Google Street Views within your map.

![](/assets/transparentMAPS/7365fef0a1f86227b65e128943d249ffa39dbcb6.jpg)

- Direction Finding Support - Provide site visitors the ability to
  find directions to or from a property.

\*Geocoding features assume you have access to a 3rd party geocoder
(free or paid) such as the
<a href="https://developers.google.com/maps/documentation/geocoding/get-api-key" class="rvts16">Google Maps Geocoding API</a>.

Geocoding from street addresses cannot always be performed successfully,
incomplete street addresses, new construction addresses or rural areas
cannot always be located via geocoding services. Manual entry of
Lat/Long coordinates may be necessary for some listings.

\*\* Google has not yet captured images from each and every location
accessible by street... yet.
