# Server Requirements

The TransparentMaps add-on for Open-Realty requires the following:

Working install of Open-Realty v3.4.2+

- PHP 7.4.3+
  - PHP SimpleXML
  - PHP CURL
  - PHP OpenSSL
