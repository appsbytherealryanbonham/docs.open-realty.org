# Troubleshooting

Q: When I try to run the geocoder to obtain coordinate information for
my listings, it is reporting that it failed for all of them.

This could be caused by several things.

1.  Missing Type: Longititude and/or Latitude fields from Open-Realty's
    Listing Field Editor.
2.  Missing or invalid Google Maps Geocoder API key in
    [TransparentMaps config](../02.Configuration/01.APIGeocoderConfiguration.md).
3.  Invalid or unset Address Fields in Open-Realty's Site Config -&gt;
    Listings Tab See:
    [Automating Geocoding](../../04.geocoders/AutomatingGeocoding.md).

If it is just not geocoding some listings, make sure the street address
information stored for those listings is complete including city, state
and zip code.

Q: Maps I have set to display in a popup window, correctly opens the new
window but it does not display a map, but instead the text: "unable to
display map"

Make sure that you have an Open-Realty template file named:
printer_friendly.html in your custom template folder and that it
contains the {addon_transparentmaps_loadgoogleapis}
{addon_transparentmaps_preloadicons} template tags for TMAPS within
the &lt;head&gt; &lt;/head&gt;.

Q: My Search Results Map and Listing Map shows pins for listings in the
wrong place or not at all.

Check the listings on that page and make sure all have Latitude and
Longitude coordinates. Correct any that are missing and re-run the
geocode_all action if you are not entering your coordinates manually.
If you are manually entering coordinate information, double check you
have used correct coordinates.

For any listings that are in the wrong place on the map, examine the
default Lat and Long coordinates set via your TMAPS Configuration on the
General Map tab. If these match a listing that is displaying in the the
wrong place and you are using the geocoder, that means Google could not
return coordinates based on the full street address that was sent
because it either does not have them, or the street address information
we sent is incomplete or invalid. Assuming the latter:

First, verify that the listing has a valid and complete street address
and correct any missing street address items, including city, state and
zip code and if you make any corrections, delete the stored coordinates
from the listing and re-run: action=addon_transparentmaps_geocode_all
and check the results on your map.

Otherwise, if the full street address information looks valid, copy it
into the address field on the google Maps website and see if the address
you provide returns an accurate location. <https://www.google.com/maps>
 If google does not return the right location, it can't work it out from
the address, and you will need to obtain correct coordinates via another
means. If it returns the correct location, do the following:

1.  Right-click the pin on the map that is displayed.
2.  Select: What's here?
3.  A card will appear at the bottom of the browser window with more
    info, and contains the Latitude and Longitude coordinates.
4.  Copy these coordinates and compare to what is stored in OR for this
    listing.

If the coordinates do not match reasonably, and are not the same as the
default values set in TMAPS, double-check your Address fields setup in
OR's SIte Config -&gt; Listing tab -&gt; Map Settings, and make sure you
have set all relevant fields to assemble a complete street address all
the way through zip code. If you make any corrections re-run the
geocoder using: action=addon_transparentmaps_geocode_all_refresh
