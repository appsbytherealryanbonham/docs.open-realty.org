# Quick Start guide

This guide will help you get TransparentMAPS up and working on your
listing detail page in only a few minutes. Once you have it working
there, you will be able to easily add maps to other pages if desired. It
is assumed you have read the relevant documentation pages above this
one, and have
already [installed the add-on](../01.Installation/02.Installingtheaddon.md),
but if not, go ahead and do that first and then you can begin.

1\) Obtain a free Javascript API key for google's Mapping and geocoding
services

<a href="https://console.developers.google.com/flows/enableapi?apiid=maps_backend,geocoding_backend,directions_backend,distance_matrix_backend,elevation_backend,places_backend&amp;keyType=CLIENT_SIDE&amp;reusekey=true" class="rvts13">Get a Key for Google Maps JavaScript API</a>

- Go to the Google Developer Console
- From the projects list, select a project or create a new one
- If the API Manager page isn’t already open, open the menu and select
  API Manager
- On the left, choose Credentials
- Click ‘Create credentials’ and then select ‘API key’
- Choose ‘Browser key’
- Give the key a meaningful name.
- You can leave the HTTP Referrers field blank.  If you are concerned
  about security, enter a referrer: for most sites, use
  yoursite.com/\* (Google’s example applies if you have subdomains).
  You can alternatively restrict access  by IP address.
- Copy the API key and click ‘OK’
- Login to your Open-Realty administration area as the admin user and
  enter your API key using the TransparentMAPS config area via the
  [API/Geocoder tab](01.APIGeocoderConfiguration.md)
  making sure that Google Geocoder Priority: is set to "First" and
  save.

If you need to geocode (obtain Latitude/Longitude coordinates
for) your listings, also do the following:

- Make sure you have configured valid Map Settings (Address Fields,
  City, State and Zip) in Open-Realty's Site Config under the Listings
  tab, and verify that 2 fields exist in your Listing Field Editor
  that are type: Latitude and Longitude respectively.

* Trigger the geocoder by clicking on the Batch Geocode Listings icon
  in your Open-Realty administration area. After a while you should
  see the results reporting how many listings were successfully
  geocoded and how many failed (if any).

If the geocoding reports a failure for all or most of your listings,
recheck to make sure you have the correct API key saved and its Priority
set to "First", and recheck your OR Map Settings to make sure you have
valid Address Field(s), City, State, and Zip configured. Lastly, make
sure any failed listings have valid address information stored for each
address-related field.

2\) Access the Template tab in OR's Site Config and note which Listing
Template you have set presently. For the purposes of this guide we will
assume you have the "default" Listing Template set, if not you will
simply need to edit the listing_detail_XXX.html template file you have
set for step \#4.

3\) Access your custom template folder and edit your main.html template
file. Insert the following two template tags within the &lt;head&gt;
&lt;/head&gt; tags, somewhere after the OR {load_js} tag but before the
closing &lt;/head&gt; and save.

Tags:

{addon_transparentmaps_loadgoogleapis}

{addon_transparentmaps_preloadicons}

When you have finished, you should have something that resembles the
following.

        {load_css_style_default}
        {load_css_style}

        {load_js}

        {addon_transparentmaps_loadgoogleapis}
        {addon_transparentmaps_preloadicons}
    </head>

4\)  Access your custom template folder and edit your
listing_detail_default.html template file. Insert the following
template tag somewhere appropriate in your layout where you want the map
to appear and save your template file. If you are using a different
listing detail template, edit that file instead. If your custom template
folder is missing the listing_detail_XXX.html template file you have
set in Site Config, copy it from the /template/default/ and edit that
copy.

Tag: {addon_transparentmaps_showmap}

For purposes of this guide we have placed the tag at the bottom of the
template file.

    <div class="hitcount">{lang_this_listing_has_been_viewed}
         <strong>{hitcount}</strong> {lang_times}.
    </div>
     {addon_transparentmaps_showmap}

5\) Navigate to the listing detail page for one of your listings, you
should see a google map at the bottom and a marker icon (pin)
representing that listing's location on the map. If you want to change
the size and/or options for your listing detail maps, you can adjust
settings via the
[Listing Map tab](05.ListingMapConfiguration.md)
in the TMAPS configuration area.

You can now add any additional map tags to your search results, Agent
details page and etc, by placing the appropriate
[MAPS template tag](../03.Templating/README.md) in the
correct template files. If you are using google's geocoding service, you
will need to periodically trigger the geocoder to provide lat/long
coordinates for any newly added listings. This process can be automated,
simply refer to the
[Automating Geocoding](geocod)
section of this documentation.
