# General Map configuration

[TransparentMAPS Configuration](TransparentMAPSConfiguration.html) ››

  

This is the general map configuration tab of TransparentMaps. Settings
in this tab are global throughout all of the maps available with this
add-on. These configuration options provide general content and
formatting settings..

  

![](/assets/transparentMAPS/86bb07df999f4421eee8912435374a09469a045c.jpg)

  

  

  

-   Prepend Custom URL: - If Prepend Custom URL is set to yes, this adds
    extra variables to the URL via the Custom URL field that will appear
    before the normal Open-Realty link. This is disabled (No), by
    default.

  

-   Custom URL: - Add any custom variables, switches etc to your URL's
    by adding them to this field. Default is for a Joomla CMS
    integration installation. Must turn on the Prepend Custom URL
    setting to use this feature.

  

-   Default Latitude: - The Default Latitude coordinate for the initial
    center point of the map while it loads your dynamic map content and
    points. Only displayed briefly on most systems until the map is
    fully loaded. The default value is approximately centered on the
    United States. Change this to your general location.

  

-   Default Longitude: - The Default Longitude coordinate for the
    initial center point of the map while it loads your dynamic map
    content and points. Only displayed briefly on most systems until the
    map is fully loaded. The default value is approximately centered on
    the United States. Change this to your general location.

  

-   Default Zoom: - The Default Zoom Level for the initial center and
    zoom of the map while it loads your dynamic map content and points.
    This will be the zoom level of maps which display a single listing,
    such as the listing map. Maps that load multiple listings, such as
    the area map, will automatically adjust the zoom level to fit all
    the listings on the map. Lower number = Zoom out higher number up to
    13 = Zoom in.

  

-   Scroll Wheel Zooms: - If set to yes, rotating the mouse scroll wheel
    will zoom the map in and out.

  

-   Enable Dragging: - Control if the user will be able to drag and
    recenter the map using their mouse.

  

-   Icon Width: - Width of the icons/markers/pins on the Google map in
    pixels.

  

-   Icon Height: - Height of the icons/markers/pins on the Google map in
    pixels.

