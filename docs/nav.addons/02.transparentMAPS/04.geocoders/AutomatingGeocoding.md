# Automating Geocoding

GeoCoding listings that do not contain lat/long coordinates

It is critical that you have already setup valid Map settings In
Open-Realty's Site Config under the Listings tab before you attempt to
run the geocoder. The Map Settings in Open-Realty are how TMAPS
works-out what listing fields to use to assemble a complete street
address to send to the geocoding service. The more accurate the full
street address that can be sent, the more accurate the longitude and
latitude coordinates returned will be.

The following geocoding command will attempt to add latitude and
longitude coordinates to any listings that do not already have it. This
is the command that you would want to setup via CRON to run once a day
(or more often) to make sure all listings have geocoding coordinates.

The geocoding command can be triggered either via your web browser or
via a CRON job on your server. The geocoding is run by issuing the
Open-Realty action: addon_transparentmaps_geocode_all. For example:
After logging into your Open-Realty administration area you can enter
the following URL into your web browser's address bar to trigger the
geocoder:

http://www.yourdomain.com/admin/index.php?action=addon_transparentmaps_geocode_all

The geocoder will then run, when completed it will report the results to
your web browser, and your OR Site Log. You can also click on the Batch
Geocode Listings menu item in your Open-Realty administration area which
will perform the same task:

![](/assets/transparentMAPS/dd5f184b2f5f0314af8ec99c2aa73b83215dc934.gif)

Batch Geocode

Listings

Using a CRON job to automate geocoding

CRON jobs are a method to trigger the loader on a defined schedule
without having to manually log in to your Open-Realty site. Especially
useful if you add listings regularly, or use an automated import add-on
such as IDXManager or TransparentRETS. The shell command that you want
your CRON job to execute would be similar to the example below that uses
CURL. Make sure to replace ORADMINUSER with your Open-Realty admin user
name, and ORADMINPASSWORD with its password, and replace yourdomain.com
with your site's domain name including any additional path elements if
you have OR installed in a subfolder. If you are importing listings as
mentioned above, you will want to trigger geocoding at a time well after
your last import of listings has completed.


```php YOURPATH/admin/index.php "user_name=ORADMINUSER&user_pass=ORADMINPASSWORD"  "action=addon_transparentmaps_geocode_all"```

Note: The above CRON example uses ajax.php instead of index.php in the
URL to limit the returned output to display only the results, instead of
your OR administration template plus the results from TMAPS. This
ability was added in v3.1.1

If you are unsure of how to create a CRON job with your hosting account
control panel, you should contact your host for specific details and
syntax as setting up CRON jobs can differ between different servers and
control panels.

Refreshing the geocoding for ALL Listings

Re-geocoding all of your listings is performed by triggering the
Open-Realty action: addon_transparentmaps_geocode_all_refresh. For
example: after logging into your Open-Realty administration area you
would enter the following into your browser:

http://www.yourdomain.com/admin/index.php?action=addon_transparentmaps_geocode_all_refresh

This geocoding command will add latitude and longitude data to all
listings replacing any pre-existing longitude and latuitude coordinates.
This command is useful if you update the address information for a large
number of listings, or are experimenting with a listing import oe
configuring your address fields and need to refresh all your geocode
coordinate information to leverage your changes. It's a good idea to
trigger this manually and not use it in a CRON job unless you have a
very specific need to do that.
