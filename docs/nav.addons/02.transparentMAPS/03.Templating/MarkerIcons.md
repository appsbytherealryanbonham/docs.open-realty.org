# Marker Icons



TransparentMaps uses different colored marker icons (pins) to mark the
locations of properties on the map.
![](/assets/transparentMAPS/30f2ab40a61c9492410b4ed5b2d346afe15b82cc.png) ![](/assets/transparentMAPS/138d2d7174cb8b24e5492d1a5016917652488289.png) ![](/assets/transparentMAPS/dd30effa17396a41f94c5ea0aef785d09a8281c2.png) ![](/assets/transparentMAPS/3922059c18a1db594e41d9dccb293b87e3bada58.png)

  

The color of icon that is used is determined by numeric Open-Realty
property class ID for each given listing. Located in the
/transparentmaps/icons folder are 25 colored .png icon images. The
numeric name of the image (1.png, 2.png, 3.png, etc..) corresponds to
the property class ID the listing belongs to. You can change the names
of the icons if you wish so you can have a specific color of icon used
to denote a specific property class.

If you prefer to use your own icons you may replace the default icons
with ones of your choice. Custom icons must use the same numeric naming
convention and be PNG image files. Your icon sizes are configurable via
the TMAPS General Map tab, however all of your icons must be the same
size but they can be larger or smaller than the default icon sizes.

NOTE: If you do not have an image named with a number for each property
class id then no marker will be displayed for any property classes do
not have a corresponding image.

Special Marker Images

center.png  ![](/assets/transparentMAPS/114c6b6d52caebd4893ad771bb510da1b3b2e724.png)  Is
a special marker used on the Area maps to distinguish the current
listing being viewed from the listings found in the specified area.

  

