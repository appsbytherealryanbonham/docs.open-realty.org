---
home: true
heroImage: /assets/img/logo.jpg
tagline: "Open-Realty®  is a web-based real estate listing management and lead generation content management system (CMS) that is designed to be very reliable and flexible as a framework for creating and managing a real estate website."
actionText: Quick Start →
actionLink: /nav.guide/
features:
- title: Easy Setup
  details: Open-Realty is very easy to setup set upanage, and is developed using PHP and javascript, utilizing MySQL database technology.
- title: Customizable Template
  details: Open-Realty provides a highly customizable HTML display template system, virtual tour support, advanced search capabilities, and many other professional features. 
- title: Powerful Addon System
  details: Extensive developer's add-on system, hooks, and  API available for the creation of virtually unlimited custom features and functionality.
---
