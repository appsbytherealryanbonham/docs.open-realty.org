# Contributing to docs.open-realty.org

When contributing to the development of docs.open-realty.org, please first discuss the change
you wish to make via issue with the maintainers before
making a change.

Please note we have a [Code of Conduct](#code-of-conduct), please follow it in all
your interactions with the project.

# Table of contents
1. [Creating an issue](#issues-issues-and-more-issues)
  1. [Template](#issue-template)
2. [Pull requests](#pull-request-process)
3. [Code of Conduct](#code-of-conduct)

# Issues, issues and more issues!

There are many ways you can contribute to docs.open-realty.org, and all of them involve creating issues
in [docs.open-realty.org issue tracker](https://gitlab.com/appsbytherealryanbonham/docs.open-realty.org/-/issues). This is the
entry point for your contribution.

To create an effective and high quality ticket, try to put the following information on your
ticket:

 A detailed description of the documentation error or improvment
  - For errors, please add links to the documentation that is incorrect.
  - For improvements, please add a detailed description of your proposal.


## Issue template
```
[Title of the issue or feature request]

Detailed description of the issue. Put as much information as you can, potentially
with images showing the issue or mockups of the proposed feature.

If it's an issue, add the steps to reproduce like this:

Steps to reproduce:

1. Open docs.open-realty.org
2. Go to "Site Configuration -> Listing Editor"

3. The text in paragraph 2 says "Open-Relty" instead of "Open-Realty"
```


This is an example of a good and informative issue:



# Pull Request Process

1. Ensure the documentation compiles. Run `yarn build` before creating the pull request.
3. The commit message is formatted as follows:

```
  <summary>

   A paragraph explaining the problem and its context.

   Another one explaining how you solved that.

   <link to the bug ticket>
```

4. Once your MR is reviewed and approved by the maintainers, it will be merged and automatically deployed..

---

# Code of Conduct

## Summary

The Open-Realty community creates software for a better world. We achieve this by behaving well towards
each other.

Therefore this document suggests what we consider ideal behaviour, so you know what
to expect when getting involved in the Open-Realty community . This is who we are and what we want to be.
There is no official enforcement of these principles, and this should not be interpreted
like a legal document.

## Advice

 * **Be respectful and considerate**: Disagreement is no excuse for poor behaviour or personal
     attacks. Remember that a community where people feel uncomfortable is not a productive one.

 * **Be patient and generous**: If someone asks for help it is because they need it. Do politely
     suggest specific documentation or more appropriate venues where appropriate, but avoid
     aggressive or vague responses such as "RTFM".

 * **Assume people mean well**: Remember that decisions are often a difficult choice between
     competing priorities. If you disagree, please do so politely. If something seems outrageous,
     check that you did not misinterpret it. Ask for clarification, but do not assume the worst.

 * **Try to be concise**: Avoid repeating what has been said already. Making a conversation larger
     makes it difficult to follow, and people often feel personally attacked if they receive multiple
     messages telling them the same thing.


In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

---

# Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/
